/*
* ThemeController.java
* Created on  2015-4-9 上午11:49
* 版本       修改时间          作者      修改内容
* V1.0.1    2015-4-9       weixuda    初始版本
*
*/
package com.ccut.bits.theme;

import com.ccut.bits.theme.service.ThemeService;
import com.ccut.bits.userFile.service.UserFileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
import java.util.Map;

/**
 * 类的描述信息
 *
 * @author weixuda
 * @version 1.0.1
 */
@Controller
@RequestMapping("/theme")
public class ThemeController {
    @Autowired
    private ThemeService themeService;
    @Autowired
    private UserFileService userFileService;
    @RequestMapping("/getThemeList")
    public String getThemeList(Model model) {
        model.addAttribute("parentTheme",themeService.getThemeTree());
        model.addAttribute("TopTheme",themeService.getTopTheme());
       model.addAttribute("NewKnowledge",userFileService.getNewKnowledge());
        model.addAttribute("cacheThemes",themeService.getCacheThemeNames());
        return "theme/getThemeList";
    }
    @RequestMapping("/ThemeMetadata")
    public String ThemeMetadata(Model model) {
        return "theme/ThemeMetadata";
    }
    @RequestMapping("/getChartData")
    @ResponseBody
    public Map<String,Integer> getChartData(){
        return themeService.getChartData();
    }

    @RequestMapping("/getDetailThemeChart")
    @ResponseBody
    public Map<String,Integer> getDetailThemeChart(@RequestParam("themeId")Integer themeId){
        return themeService.getDetailThemeChartData(themeId);
    }

    @RequestMapping(value = "/getThemeNames",method = RequestMethod.POST)
    @ResponseBody
    public List<String> getThemeNames(){
        return themeService.getThemeNames();
    }
}
