package com.ccut.bits.security;


import com.ccut.bits.files.entity.FileVO;
import com.ccut.bits.files.service.FileService;
import com.ccut.bits.security.dao.UserDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created by Administrator on 2016/4/10.
 */


@Controller
public class FilesController {
    @Autowired
    private FileService fileService;
    @Autowired
    private UserDao userDao;


    @RequestMapping(value = "/admin/file/listAllPictureDo",method = {RequestMethod.GET})
    public String getAllFileDo(FileVO fileVO, Model model, HttpServletRequest request) {

        System.out.println("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
        fileVO.setUserName2("图片");
        System.out.println(fileVO.getUserName2());
        List<FileVO> fileList = fileService.getFileInfoList(fileVO);


        model.addAttribute("fileList", fileList);
        model.addAttribute("fileVO", fileVO);
        return "security/file/listAllPictureDo";
    }






    @RequestMapping(value = "/admin/file/listAllVideoDo",method = {RequestMethod.GET})
    public String getAllVideoDo(FileVO fileVO, Model model, HttpServletRequest request) {


        System.out.println("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
        fileVO.setUserName2("视频");
        System.out.println(fileVO.getUserName2());

        List<FileVO> fileList = fileService.getFileInfoList(fileVO);

        model.addAttribute("fileList", fileList);
        model.addAttribute("fileVO", fileVO);
        return "security/file/listAllVideoDo";
    }


    @RequestMapping(value = "/admin/file/listAllMusicDo",method = {RequestMethod.GET})
    public String getAllMusicDo(FileVO fileVO, Model model, HttpServletRequest request) {


        System.out.println("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
        fileVO.setUserName2("音频");
        System.out.println(fileVO.getUserName2());

        List<FileVO> fileList = fileService.getFileInfoList(fileVO);

        model.addAttribute("fileList", fileList);
        model.addAttribute("fileVO", fileVO);
        return "security/file/listAllMusicDo";
    }



    @RequestMapping("/admin/file/deleteManyFileInfo")
    @ResponseBody
    public String deleteManyFileInfo(@RequestParam("fileIds[]") String[] fileIds) {

        fileService.deleteManyFileInfo(fileIds);

      return "";
    }

 @RequestMapping(value = "/admin/file/editFileDo", method = {RequestMethod.GET})
    public String editFileDo(HttpServletRequest request,@RequestParam("id") int id) {
    //  User user = userService.findUserById(id);
     //   request.setAttribute("user",user);
  FileVO fileVO=fileService.findFileById(id);
    request.setAttribute("fileVO",fileVO);
     System.out.println("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
     System.out.println(id);
      return "security/file/editPictureDo";
   }


    @RequestMapping(value = "/admin/file/editFile", method = {RequestMethod.POST})
    @ResponseBody
    public void editFileVO(FileVO fileVO) {

        fileService.editFileVO(fileVO);
        System.out.println("11111111111111111111888888888$$$$$$$");
    }

    //跳转到文件管理界面
    @RequestMapping(value = "admin/file/listAllFileDo",method={RequestMethod.GET})
    public String toListAllFileDo(FileVO fileVO, Model model){

        fileVO.setUserName2("文件");
        List<FileVO> fileList = fileService.getFileInfoList(fileVO);

        model.addAttribute("fileList", fileList);
        model.addAttribute("fileVO", fileVO);
        return "security/file/listAllFileDo";
    }

}
