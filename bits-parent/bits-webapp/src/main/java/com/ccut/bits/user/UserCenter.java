/*
* UserCenter.java
* Created on  2015-4-20 上午10:08
* 版本       修改时间          作者      修改内容
* V1.0.1    2015-4-20       weixuda    初始版本
*
*/
package com.ccut.bits.user;

import com.ccut.bits.expert.service.ExpertService;
import com.ccut.bits.files.entity.FileVO;
import com.ccut.bits.files.service.FileService;
import com.ccut.bits.model.User;
import com.ccut.bits.page.PageResponse;
import com.ccut.bits.relation.ThemeRelation;
import com.ccut.bits.theme.service.ThemeService;
import com.ccut.bits.userCenter.model.History;
import com.ccut.bits.userCenter.service.UserCenterService;
import com.ccut.bits.userCenter.service.UserCenterServiceImpl;
import com.ccut.bits.userFile.model.UserFileVO;
import com.ccut.bits.userFile.service.UserFileService;
import org.apache.commons.fileupload.FileUploadException;
import org.omg.CORBA.Request;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 类的描述信息
 *
 * @author weixuda
 * @version 1.0.1
 */

@Controller
@RequestMapping("/userCenter")
public class UserCenter {
    @Autowired
    private UserFileService userFileService;
    @Autowired
    private ThemeService themeService;
    @Autowired
    private UserCenterService userCenterService;
    @Autowired
    private ThemeRelation themeRelation;
    @Autowired
    private ExpertService expertService;
    private int currentUserId = 0;

    private boolean maybeRedirect(HttpSession session) {
        return session.getAttribute("User") == null;
    }

    private User getCurrentUser(HttpSession session) {
        return (User) session.getAttribute("User");
    }

    private boolean isChangeUser(HttpSession session) {
        User user = (User) session.getAttribute("User");
        return currentUserId != user.getUserId();
    }




    @RequestMapping(value = "/uploadAttachment", method = {RequestMethod.POST})
    public String uploadAttachment(@RequestParam() MultipartFile[] files, HttpSession session, HttpServletRequest request, HttpServletResponse response) {

        String returnMassage = "success";
        FileVO uploadVO = new FileVO();

        if (files != null && files.length > 0) {
            //循环获取file数组中得文件
            for (int i = 0; i < files.length; i++) {
                MultipartFile file = files[i];


                try {
                    userFileService.uploadFile(file, request,session,response, i);
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (FileUploadException e) {
                    e.printStackTrace();
                }


            }

        }

        return "user/handArrangement";
    }










    @RequestMapping(value = "/updatePassword", method = RequestMethod.POST)
    @ResponseBody
    public void updatePassword(User user) {
        userCenterService.updatePassword(user);
    }

    @RequestMapping(value = "/updateUserInfo", method = RequestMethod.POST)
    @ResponseBody
    public void updateUserInfo(User user) {
        userCenterService.updateUserInfo(user);
    }

    @RequestMapping(value = "getUserHistory", method = RequestMethod.POST)
    @ResponseBody
    public PageResponse<History> getUserHistoryListPage(History history) {
        return new PageResponse<>(history.getPage(), userCenterService.getUserHistoryListPage(history));
    }

    @RequestMapping(value = "getUserHistoryDown", method = RequestMethod.POST)
    @ResponseBody
    public PageResponse<History> getUserHistoryListPageDown(History history) {
        return new PageResponse<>(history.getPage(), userCenterService.getUserHistoryListPageDown(history));
    }

    @RequestMapping(value = "getUserHistoryUp", method = RequestMethod.POST)
    @ResponseBody
    public PageResponse<History> getUserHistoryListPageUp(History history) {
        return new PageResponse<>(history.getPage(), userCenterService.getUserHistoryListPageUp(history));
    }

    @RequestMapping(value = "getArrangement", method = RequestMethod.POST)
    @ResponseBody
    public PageResponse<UserFileVO> getUserArrangementListPage(UserFileVO userFileVO) {
        return new PageResponse<>(userFileVO.getPage(), userFileService.getUserArrangementListPage(userFileVO));
    }

    @RequestMapping(value = "getArrangementImg", method = RequestMethod.POST)
    @ResponseBody
    public PageResponse<UserFileVO> getUserArrangementImgListPage(UserFileVO userFileVO) {
        return new PageResponse<>(userFileVO.getPage(), userFileService.getUserArrangementImgListPage(userFileVO));
    }

    @RequestMapping(value = "getArrangementVideo", method = RequestMethod.POST)
    @ResponseBody
    public PageResponse<UserFileVO> getUserArrangementVideoListPage(UserFileVO userFileVO) {
        return new PageResponse<>(userFileVO.getPage(), userFileService.getUserArrangementVideoListPage(userFileVO));
    }

    @RequestMapping(value = "getArrangementMusic", method = RequestMethod.POST)
    @ResponseBody
    public PageResponse<UserFileVO> getUserArrangementMusicListPage(UserFileVO userFileVO) {
        return new PageResponse<>(userFileVO.getPage(), userFileService.getUserArrangementMusicListPage(userFileVO));
    }

    @RequestMapping(value = "getArrangementOther", method = RequestMethod.POST)
    @ResponseBody
    public PageResponse<UserFileVO> getUserArrangementOtherListPage(UserFileVO userFileVO) {
        return new PageResponse<>(userFileVO.getPage(), userFileService.getUserArrangementOtherListPage(userFileVO));
    }

    @RequestMapping(value = "getCommon", method = RequestMethod.POST)
    @ResponseBody
    public PageResponse<UserFileVO> getCommon(UserFileVO userFileVO) {
        return new PageResponse<>(userFileVO.getPage(), userFileService.getUserCommonListPage(userFileVO));
    }

    @RequestMapping(value = "editInfo", method = {RequestMethod.GET})
    public String editInfo(HttpServletRequest request,@RequestParam("id") int id) {
        UserFileVO userFileVO=userFileService.findById(id);
        request.setAttribute("userFileVO",userFileVO);
        return "user/editInfoDo";
    }

    @RequestMapping(value = "editFileName", method = {RequestMethod.POST})
    @ResponseBody
    public void editFileName(UserFileVO userFileVO) {
        userFileService.editFileName(userFileVO);
    }

    @RequestMapping(value = "addCommon", method = {RequestMethod.POST})
    @ResponseBody
    public void addCommon(UserFileVO userFileVO) {
        userFileService.addCommon(userFileVO);
    }

    @RequestMapping(value = "delUserHistory", method = {RequestMethod.DELETE})
    @ResponseBody
    public void delUserHistory(@RequestParam("id") int id) {
        userCenterService.delUserHistory(id);
    }

    @RequestMapping(value = "delUserHistoryDown", method = {RequestMethod.DELETE})
    @ResponseBody
    public void delUserHistoryDown(@RequestParam("id") int id) {
        userCenterService.delUserHistoryDown(id);
    }

    @RequestMapping(value = "delUserHistoryUp", method = {RequestMethod.DELETE})
    @ResponseBody
    public void delUserHistoryUp(@RequestParam("id") int id) {
        userCenterService.delUserHistoryUp(id);
    }

    @RequestMapping(value = "delArrangement", method = {RequestMethod.DELETE})
    @ResponseBody
    public void delArrangement(@RequestParam("id") int id) {
        userFileService.delArrangement(id);
    }

    @RequestMapping(value = "delArrangementImg", method = {RequestMethod.DELETE})
    @ResponseBody
    public void delArrangementImg(@RequestParam("id") int id) {
        userFileService.delArrangementImg(id);
    }

    @RequestMapping(value = "delArrangementMusic", method = {RequestMethod.DELETE})
    @ResponseBody
    public void delArrangementMusic(@RequestParam("id") int id) {
        userFileService.delArrangementMusic(id);
    }

    @RequestMapping(value = "delArrangementVideo", method = {RequestMethod.DELETE})
    @ResponseBody
    public void delArrangementVideo(@RequestParam("id") int id) {
        userFileService.delArrangementVideo(id);
    }

    @RequestMapping(value = "delArrangementOther", method = {RequestMethod.DELETE})
    @ResponseBody
    public void delArrangementOther(@RequestParam("id") int id) {
        userFileService.delArrangementOther(id);
    }

    @RequestMapping(value = "getDelArrangement", method = RequestMethod.POST)
    @ResponseBody
    public PageResponse<UserFileVO> getDelUserArrangementListPage(UserFileVO userFileVO) {
        return new PageResponse<>(userFileVO.getPage(), userFileService.getDelUserArrangementListPage(userFileVO));
    }

    @RequestMapping(value = "delArrangementDus", method = {RequestMethod.DELETE})
    @ResponseBody
    public void delArrangementDus(@RequestParam("id") int id) {
        userFileService.delArrangementDus(id);
    }

    @RequestMapping(value = "addArrangement", method = {RequestMethod.POST})
    @ResponseBody
    public void addArrangement(UserFileVO userFileVO) {
        userFileService.addArrangement(userFileVO);
    }

    //用户关注部分
    @RequestMapping(value = "addUserFocusThemes", method = RequestMethod.POST)
    @ResponseBody
    public void addUserFocusThemes(HttpServletRequest request, HttpSession session) {
        String[] expertIds = request.getParameterValues("themeIds[]");
        User user = (User) session.getAttribute("User");
        if (user == null)
            return;
        userCenterService.updateUserFocus(user, expertIds, UserCenterServiceImpl.OPERATION.ADD, UserCenterServiceImpl.PART.FOCUS_THEME);
    }

    @RequestMapping(value = "addUserFocusExperts", method = RequestMethod.POST)
    @ResponseBody
    public void addUserFocusExperts(HttpServletRequest request, HttpSession session) {
        String[] expertIds = request.getParameterValues("expertIds[]");
        User user = (User) session.getAttribute("User");
        if (user == null)
            return;
        userCenterService.updateUserFocus(user, expertIds, UserCenterServiceImpl.OPERATION.ADD, UserCenterServiceImpl.PART.FOCUS_EXPERT);
    }

    @RequestMapping(value = "delUserFocusThemes", method = RequestMethod.POST)
    @ResponseBody
    public void delUserFocusThemes(HttpServletRequest request, HttpSession session) {
        String[] expertIds = request.getParameterValues("themeIds[]");
        User user = (User) session.getAttribute("User");
        if (user == null)
            return;
        userCenterService.updateUserFocus(user, expertIds, UserCenterServiceImpl.OPERATION.DEL, UserCenterServiceImpl.PART.FOCUS_THEME);
    }

    @RequestMapping(value = "delUserFocusExperts", method = RequestMethod.POST)
    @ResponseBody
    public void delUserFocusExperts(HttpServletRequest request, HttpSession session) {
        String[] expertIds = request.getParameterValues("expertIds[]");
        User user = (User) session.getAttribute("User");
        if (user == null)
            return;
        userCenterService.updateUserFocus(user, expertIds, UserCenterServiceImpl.OPERATION.DEL, UserCenterServiceImpl.PART.FOCUS_EXPERT);
    }

    @RequestMapping(value = "updateUserFocus", method = RequestMethod.POST)
    @ResponseBody
    public void updateUserFocus(HttpServletRequest request, HttpSession session) {
        User user = this.getCurrentUser(session);
        if (user == null)
            return;
        String[] themeIds = request.getParameterValues("themeIds[]");
        String[] expertIds = request.getParameterValues("expertIds[]");
        if (themeIds != null) {
            Integer[] t1 = new Integer[themeIds.length];
            for (int i = 0; i < themeIds.length; i++) {
                t1[i] = Integer.valueOf(themeIds[i]);
            }
            user.getFocusThemes().clear();
            user.getFocusThemes().addAll(Arrays.asList(t1));
        }else{
            user.getFocusThemes().clear();
        }
        if (expertIds != null) {
            Integer[] t2 = new Integer[expertIds.length];

            for (int i = 0; i < expertIds.length; i++) {
                t2[i] = Integer.valueOf(expertIds[i]);
            }
            user.getFocusExperts().clear();
            user.getFocusExperts().addAll(Arrays.asList(t2));
        }else{
            user.getFocusExperts().clear();
        }
        userCenterService.updateUserFocusToDB(user);
    }

    @RequestMapping(value = "getUserFocusThemes", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> getUserFocus(HttpSession session) {
        User user = this.getCurrentUser(session);
        Map<String, Object> map = new HashMap<>();
        map.put("focusThemes", themeRelation.getThemes(user.getFocusThemes()));
        return map;
    }

    //以下为页面跳转
    @RequestMapping("/handleUserInfo")
    public String getUserCenterPage(Model model, HttpSession session) {
        if (maybeRedirect(session))
            return "redirect:/login";
        User user = this.getCurrentUser(session);
        model.addAttribute("focusThemes", themeRelation.getThemes(user.getFocusThemes()));
        model.addAttribute("focusExpert", expertService.getExpertsByIds(user.getFocusExperts()));
        return "user/handleUserInfo";
    }

    @RequestMapping("/handlePassword")
    public String changePassword(HttpSession session) {
        if (maybeRedirect(session))
            return "redirect:/login";
        return "user/handlePassword";
    }

    @RequestMapping("/handleHistory")
    public String searchHistory(HttpSession session) {
        if (maybeRedirect(session))
            return "redirect:/login";
        return "user/handleHistory";
    }

    @RequestMapping("/handleHistoryUp")
    public String searchHistoryUp(HttpSession session) {
        if (maybeRedirect(session))
            return "redirect:/login";
        return "user/handleHistoryUp";
    }

    @RequestMapping("/handleHistoryDown")
    public String searchHistoryDown(HttpSession session) {
        if (maybeRedirect(session))
            return "redirect:/login";
        return "user/handleHistoryDown";
    }

    @RequestMapping("/handArrangementMusic")
    public String searchArrangementMusic(HttpSession session) {
        if (maybeRedirect(session))
            return "redirect:/login";
        return "user/handArrangementMusic";
    }

    @RequestMapping("/handArrangementVideo")
    public String searchArrangementVideo(HttpSession session) {
        if (maybeRedirect(session))
            return "redirect:/login";
        return "user/handArrangementVideo";
    }

    @RequestMapping("/handArrangementImg")
    public String searchArrangementImg(HttpSession session) {
        if (maybeRedirect(session))
            return "redirect:/login";
        return "user/handArrangementImg";
    }

    @RequestMapping("/handArrangementOther")
    public String searchArrangementOther(HttpSession session) {
        if (maybeRedirect(session))
            return "redirect:/login";
        return "user/handArrangementOther";
    }

    @RequestMapping("/handFileUpload")
    public String showFileUpload(HttpSession session) {
        if (maybeRedirect(session))
            return "redirect:/login";
        return "user/handFileUpload";
    }

    //显示所有关联文档通过文件id
    @RequestMapping(value = "/showAllFileRelationById")
    @ResponseBody
    public void showAllFileRelationById( HttpServletRequest request,@RequestParam("id") int id,HttpSession session){
        //获取session用户数据
        User user = (User) session.getAttribute("User");
        //显示所有和该文件关联的文件
        List<UserFileVO> file3=userFileService.showFileRelation(id,user.getUserId());
        request.setAttribute("File",file3);
    }

    @RequestMapping("/handTreasure")
    public String showTreasure(HttpSession session, Model model, HttpServletRequest request,@RequestParam(value = "id",required = false) Integer id) {
        if (maybeRedirect(session))
            return "redirect:/login";
        model.addAttribute("myFiles",userFileService.getFileNameById(getCurrentUser(session).getUserId()));
        model.addAttribute("cacheThemes",themeService.getAllThemeNames());


        //获取session用户数据
        User user = (User) session.getAttribute("User");
        List<UserFileVO> file=userFileService.getAllFileList(user.getUserId());
        request.setAttribute("file",file);
        if(id!=null){

        //显示所有和该文件关联的文件
        List<UserFileVO> file3=userFileService.showFileRelation(id,user.getUserId());
        request.setAttribute("File",file3);}
        return "user/handTreasure";
    }

    @RequestMapping(value = "addShare", method = {RequestMethod.POST})
    @ResponseBody
    public void addShare(UserFileVO userFileVO) {
        userFileService.addShare(userFileVO);
    }

    @RequestMapping(value = "addCollect", method = {RequestMethod.POST})
    @ResponseBody
    public void addCollect(UserFileVO userFileVO) {
        userFileService.addCollect(userFileVO);
    }

    @RequestMapping(value = "addDownfile", method = {RequestMethod.POST})
    @ResponseBody
    public void addDownfile(History history) {
        userCenterService.addDownfile(history);
    }

    @RequestMapping(value = "addCheckHistory", method = {RequestMethod.POST})
    @ResponseBody
    public void addCheckHistory(History history) {
        userCenterService.addCheckHistory(history);
    }

    @RequestMapping("/handArrangement")
    public String showArrangement(HttpSession session) {
        if (maybeRedirect(session))
            return "redirect:/login";
        return "user/handArrangement";
    }

    @RequestMapping("/handCommon")
    public String showCommon(HttpSession session) {
        if (maybeRedirect(session))
            return "redirect:/login";
        return "user/handCommon";
    }

    @RequestMapping("/handDustbin")
    public String showDustbin(HttpSession session) {
        if (maybeRedirect(session))
            return "redirect:/login";
        return "user/handDustbin";
    }

    @RequestMapping("/handleFocus")
    public String focusThemeList(HttpSession session, Model model) {
        if (maybeRedirect(session))
            return "redirect:/login";
        model.addAttribute("cacheThemes", themeRelation.getCacheThemes());
        model.addAttribute("focusExpert", expertService.getExpertsByIds(getCurrentUser(session).getFocusExperts()));
        if (isChangeUser(session)) {
            session.setAttribute("recommendInfo", userCenterService.getUserRecommend(this.getCurrentUser(session)));
            this.currentUserId = getCurrentUser(session).getUserId();
        }else{
            if(session.getAttribute("recommendInfo") == null)
                session.setAttribute("recommendInfo", userCenterService.getUserRecommend(this.getCurrentUser(session)));
        }
        return "user/handleFocus";
    }

    @RequestMapping("/interestRecommend")
    public String interestRecommend() {
        return "user/interestRecommend";
    }

    // TO添加文件页
    @RequestMapping("/toFileRelationPage")
    public String toAddRelationPage(HttpServletRequest request,@RequestParam("id") int id,HttpSession session,History history){
        //获取session用户数据
        User user = (User) session.getAttribute("User");
        //显示想要关联文件的信息
        UserFileVO file=userFileService.findFileById(id);
        request.setAttribute("file",file);

        //显示所有和该文件关联的文件
        List<UserFileVO> file1=userFileService.showFileRelation(id,user.getUserId());
        request.setAttribute("file1",file1);
        //显示所有未和该文件关联的信息
//       List<FileVO> file3=fileService.getfileListById(user.getUserId(),id);
//        request.setAttribute("file3",file3);

        String[] fileId1= userFileService.getFileRelation(user.getUserId(),id);
        //test
        List<UserFileVO> file3=userFileService.showFileRelationTest(user.getUserId(),id,fileId1);
        request.setAttribute("file3",file3);
        return "user/addFileRelationPage";
    }
    //添加文件关联
    @RequestMapping(value="/addFileRelation")
    @ResponseBody
    public String addFileRelation(HttpServletRequest request ,HttpSession session){
        String[] fileId1= request.getParameterValues("fileId1");
        String[] fileId2= request.getParameterValues("fileId2[]");
        User user = (User) session.getAttribute("User");
        userFileService.addFileRelation(fileId1,fileId2,user.getUserId());
        return "";
    }
    //用文件编号添加文件关联
    @RequestMapping(value="/addFileRelationTest")
    @ResponseBody
    public String addFileRelationTest(HttpServletRequest request ,HttpSession session){
        String[] fileId1= request.getParameterValues("fileId1");
        String[] fileId2= request.getParameterValues("fileId2");

        User user = (User) session.getAttribute("User");
        userFileService.addFileRelation(fileId1,fileId2,user.getUserId());
        return "";
    }

    //删除文件关联
    @RequestMapping(value = "deleteFileRelation")
    @ResponseBody
    public String deleteFileRelation(HttpServletRequest request ,HttpSession session){
        String[] fileId1= request.getParameterValues("fileId1");
        String[] fileId2= request.getParameterValues("fileId2");
        User user = (User) session.getAttribute("User");
        userFileService.deleteFileRelation(fileId1,fileId2,user.getUserId());
        return "";
    }
}
