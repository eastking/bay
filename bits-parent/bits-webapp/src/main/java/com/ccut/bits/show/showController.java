package com.ccut.bits.show;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by mafuyu on 2016/6/5.
 */
@Controller
@RequestMapping("/show")
public class showController {
    @RequestMapping(value = "/showVideo",method ={RequestMethod.GET})
        public String showVideo(){
            return "show/showVideo";
        }
}
