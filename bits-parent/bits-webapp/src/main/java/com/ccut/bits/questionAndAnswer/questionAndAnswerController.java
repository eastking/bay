package com.ccut.bits.questionAndAnswer;

import com.ccut.bits.expert.service.ExpertService;
import com.ccut.bits.literature.model.Literature;
import com.ccut.bits.literature.service.LiteratureService;
import com.ccut.bits.page.Page;
import com.ccut.bits.page.Pagination;
import com.ccut.bits.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created by mafuyu on 2016/5/19.
 */
@Controller
@RequestMapping("/questionAndAnswer")
public class questionAndAnswerController {

    @Autowired
    private LiteratureService literatureService;
    @Autowired
    private ExpertService expertService;

    @RequestMapping("/getTheExperts")
    public String getLiteratureList(HttpServletRequest request, Model model) {

        Page page = new Page(request);
        List<Literature> result = literatureService.getAllLiterature(page);
        Pagination pagination = new Pagination(page, literatureService.getTotalRecord(),null);
        model.addAttribute("result",result);
        model.addAttribute("Pagination",pagination.getPagination());
        return "questionAndAnswer/getTheExperts";
    }
    @RequestMapping("/getQuestionDetail")
    public String getLiteratureDetail(@RequestParam("id")Integer id, Model model) {
        if (id == null) return "redirect:/literature/getLiteratureList";
        Literature literature = literatureService.getLiteratureById(id);
        if (literature == null) return "redirect:/literature/getLiteratureList";
        String[] names = literature.getAuthorCn().split(";");
        names = StringUtil.trimStringArray(names);
        model.addAttribute("Literature",literature);
        model.addAttribute("Experts",expertService.getExpertByNames(names));
        return "questionAndAnswer/getQuestionDetail";
    }

}
