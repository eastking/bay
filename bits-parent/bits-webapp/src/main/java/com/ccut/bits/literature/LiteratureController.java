/*
* literature.java
* Created on  2015-4-5 上午10:51
* 版本       修改时间          作者      修改内容
* V1.0.1    2015-4-5       weixuda    初始版本
*
*/
package com.ccut.bits.literature;

import com.ccut.bits.autocomplete.service.AutoCompleteService;
import com.ccut.bits.expert.service.ExpertService;
import com.ccut.bits.literature.model.Literature;
import com.ccut.bits.literature.service.LiteratureService;
import com.ccut.bits.page.Page;
import com.ccut.bits.page.PageResponse;
import com.ccut.bits.page.Pagination;
import com.ccut.bits.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * 类的描述信息
 *
 * @author weixuda
 * @version 1.0.1
 */
@Controller
@RequestMapping("/literature")
public class LiteratureController {
    @Autowired
    private LiteratureService literatureService;
    @Autowired
    private ExpertService expertService;
    @Autowired
    private AutoCompleteService autoCompleteService;

    @RequestMapping("/getLiteratureList")
    public String getLiteratureList(HttpServletRequest request,Model model) {
        Page page = new Page(request);
        List<Literature> result = literatureService.getAllLiterature(page);
        Pagination pagination = new Pagination(page, literatureService.getTotalRecord(),null);
        model.addAttribute("result",result);
        model.addAttribute("Pagination",pagination.getPagination());
        return "literature/getLiteratureList";
    }

    @RequestMapping("/getLiteratureDetail")
    public String getLiteratureDetail(@RequestParam("id")Integer id,Model model) {
        if (id == null) return "redirect:/literature/getLiteratureList";
        Literature literature = literatureService.getLiteratureById(id);
        if (literature == null) return "redirect:/literature/getLiteratureList";
        String[] names = literature.getAuthorCn().split(";");
        names = StringUtil.trimStringArray(names);
        model.addAttribute("Literature",literature);
        model.addAttribute("Experts",expertService.getExpertByNames(names));
        return "literature/getLiteratureDetail";
    }

    @RequestMapping(value = "/getLiteratureByThemeId")
    public String getLiteratureByThemeId(HttpServletRequest request,Model model){
        Page page = new Page(request);
        int themeId = Integer.parseInt(request.getParameter("themeId"));
        List<Literature> result = literatureService.getLiteratureByThemeId(themeId,page);
        if (result == null || result.isEmpty()) return "redirect:/noRecord";
        int totalRecord = literatureService.getTotalRecordByThemeId(themeId);
        model.addAttribute("result",result);
        model.addAttribute("Pagination",new Pagination(page,totalRecord,"themeId="+themeId).getPagination());
        return "literature/getLiteratureList";
    }

    @RequestMapping("/getLiteratureListDo")
    @ResponseBody
    public PageResponse<Literature> getLiteratureListDo(Literature literature){
        return new PageResponse<>(literature.getPage(), literatureService.getLiteratureListPage(literature));
    }

    @RequestMapping(value = "addLiteratureDo", method = {RequestMethod.GET})
    public String addUserDo() {
        autoCompleteService.initAutoComplete();
        return "security/literature/addLiterature";
    }

    @RequestMapping(value = "editLiteratureDo", method = {RequestMethod.GET})
    public String editUserDo(HttpServletRequest request,@RequestParam("id") int id) {
        Literature literature = literatureService.getLiteratureById(id);
        request.setAttribute("literature",literature);
        return "security/literature/editLiterature";
    }

    @RequestMapping(value = "addLiterature", method = RequestMethod.POST)
    @ResponseBody
    public void addLiterature(Literature literature){
        literatureService.addLiterature(literature);
    }

    @RequestMapping(value = "editLiterature",method = RequestMethod.POST)
    @ResponseBody
    public void editLiterature(Literature literature){
        literatureService.editLiterature(literature);
    }

    @RequestMapping(value = "delLiteratureDo",method = {RequestMethod.DELETE})
    @ResponseBody
    public void delLiteratureDo(@RequestParam("id") int id) {
        literatureService.delLiterature(id);
    }
}
