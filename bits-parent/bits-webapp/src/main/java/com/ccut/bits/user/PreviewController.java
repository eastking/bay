package com.ccut.bits.user;


import com.ccut.bits.files.entity.FileVO;
import com.ccut.bits.files.service.FileService;


import com.ccut.bits.model.User;

import com.ccut.bits.userFile.model.UserFileVO;
import com.ccut.bits.userFile.service.UserFileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;



import java.util.List;

/**
 * Created by Administrator on 2016/6/5.
 */
@Controller

public class PreviewController {

    @Autowired
    private FileService fileservice;
   @Autowired
   private UserFileService userFileService;

    @RequestMapping(value = "/fileSearch", method = {RequestMethod.POST})
    public String search(@RequestParam("fsearch") String fsearch, Model model, HttpSession session) {

        FileVO fileVO=new FileVO();
        User user = (User)session.getAttribute("User");
        List<FileVO> fs = fileservice.search(fsearch);
        model.addAttribute("f",fs);
        return "search/getsearch";
}
    @RequestMapping(value = "/userfileSearch", method = {RequestMethod.POST})
    public String usersearch(@RequestParam("userfsearch") String fsearch, Model model, HttpSession session) {

        UserFileVO userfileVO=new UserFileVO();
        User user = (User)session.getAttribute("User");
        userfileVO.setUserId(user.getUserId());
      List<UserFileVO > userFileVO=userFileService.search(fsearch);
       model.addAttribute("userFileVO",userFileVO);
        return "user/handArrangement";


    }


    @RequestMapping(value = "/abcSearch", method = {RequestMethod.POST})
    public String abcsearch(HttpServletRequest request, Model model) {
     String fname=request.getParameter("fname");
     String fkeyword=request.getParameter("fkeyword");
        String ftime=request.getParameter("ftime");
        String ftype=request.getParameter("ftype");
        String AA=request.getParameter("A");
        String BB=request.getParameter("B");


        FileVO fileVO=new FileVO();
        fileVO.setFileName(fname);
        fileVO.setFileKeyword(fkeyword);
        fileVO.setFileTime(ftime);






        if(ftype.equals("所有"))
        {
              if((AA.equals("1"))&&(BB.equals("1"))){
                    int a=111;

                  List<FileVO> f = fileservice.search111(fileVO);
                  model.addAttribute("f",f);
              }

              else if((AA.equals("1"))&&(BB.equals("0"))){
                  int a=110;
                  List<FileVO> f = fileservice.search110(fileVO);
                  model.addAttribute("f",f);
              }

              else if((AA.equals("0"))&&(BB.equals("1"))){
                  int a=101;
                  List<FileVO> f = fileservice.search101(fileVO);
                  model.addAttribute("f",f);
              }

              else {
                  int a=100;
                  List<FileVO> f = fileservice.search100(fileVO);
                  model.addAttribute("f",f);
              }
        }
        else
        {

              fileVO.setFileType(ftype);
            if((AA.equals("1"))&&(BB.equals("1"))){
                int a=011;

                List<FileVO> f = fileservice.search011(fileVO);
                model.addAttribute("f",f);
            }

            else if((AA.equals("1"))&&(BB.equals("0"))){
                int a=010;
                List<FileVO> f = fileservice.search010(fileVO);
                model.addAttribute("f",f);
            }

            else if((AA.equals("0"))&&(BB.equals("1"))){
                int a=001;
                List<FileVO> f = fileservice.search001(fileVO);
                model.addAttribute("f",f);
            }

            else {
                int a=000;
                List<FileVO> f = fileservice.search000(fileVO);
                model.addAttribute("f",f);
            }

        }



        return "search/getsearch";
    }


}