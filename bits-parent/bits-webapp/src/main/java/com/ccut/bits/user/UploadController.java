
package com.ccut.bits.user;



import com.ccut.bits.files.dao.FileDao;
import com.ccut.bits.files.entity.FileVO;
import com.ccut.bits.files.service.FileService;
import com.ccut.bits.userFile.model.UserFileVO;
import com.ccut.bits.userFile.service.UserFileService;
import org.apache.commons.fileupload.FileUploadException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@Controller
@Transactional

public class UploadController {
    private Logger logger = LoggerFactory.getLogger(UploadController.class);
    @Autowired
    private UserFileService uploadService;
    @Autowired
    private FileService downService;
    @Autowired
    private FileDao fileDao;


    @RequestMapping(value = "/downFile")
    public void downFile(@RequestParam("filePath") String filePath, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // String path = request.getParameter("path");
        downService.downFile(filePath, request, response);
    }

    @RequestMapping(value = "/previewFile")
    public String previewFile(@RequestParam("filePath") String filePath, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // String path = request.getParameter("path");
        String url = downService.previewFile(filePath, request, response);
        return "redirect:" + url;


    }

    // 判断选中的文件类型
    @RequestMapping(value = "/getInfo")
    public String getInfo(HttpServletRequest request, @RequestParam("id") int id) {
        FileVO file = downService.findFileById(id);
        request.setAttribute("file", file);
        if (file.getFileType().equals("视频")) {
            return "redirect:/expert/showVideo?id=" + id;

        } else if (file.getFileType().equals("音频")) {
            return "redirect:/expert/showMusic?id=" + id;
        } else {
            return "redirect:/expert/getInformation?id=" + id;

        }

    }

    @RequestMapping(value = "/getInfomation")
    public String getInfomation(HttpServletRequest request, @RequestParam("id") int id) {
        UserFileVO ufile = uploadService.findFileById(id);
        request.setAttribute("file", ufile);
        if (ufile.getUfileType().equals("视频")) {
            return "redirect:/expert/showuVideo?id=" + id;

        } else if (ufile.getUfileType().equals("音频")) {
            return "redirect:/expert/showuMusic?id=" + id;
        } else {
            return "redirect:/expert/getuInformation?id=" + id;

        }

    }
}



//
//  @RequestMapping(value = "/togetInfo")
//   public String gettoInfo(HttpServletRequest request,@RequestParam("id") int id) {
//       UserFileVO userFileVO=downService.findUfileById(id);
//        request.setAttribute("file",userFileVO);
//        if (userFileVO.getFileType().equals("视频")) {
//           return "redirect:/expert/showVideo?id="+ id;
//
//        }
//        else if (userFileVO.getFileType().equals("音频")){
//            return "redirect:/expert/showMusic?id="+ id;
//        }
//        else{
//            return "redirect:/expert/getInformation?id="+ id;
//
//       }
//
//   }
//
//}
