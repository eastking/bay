/**
 * Created by qiutiandong on 2016/6/4 0004.
 */
var CommonDoc = function () {
    var reloadTable = function (pageFlag) {
        $("#datatable_ajax").DataTable().draw(pageFlag)
    };
    var onSortColumn = function (sortColumn, sortDirection) {
        switch (sortColumn) {
            case "ufileName":
                sortColumn = "ufile_name";
                break;
        }
        return onSortColumnDefault(sortColumn, sortDirection);
    };
    var Common = function () {
        var grid = new DataTable();
        var $table = $("#datatable_ajax");
        grid.addAjaxParam("userId", $("#userId").val());
        grid.init({
            src: $table,
            url: basePath + "userCenter/getCommon",
            onSortColumn: onSortColumn,
            onQuery: function (data) {
                data.ufileName = $("#ufileName").val();
            },
            dataTable: {
                "columns": [
                    {data: 'ufileName', orderable: true},
                    {data: 'ufileTime', orderable: true},
                    {
                        data: 'operate', orderable: false,
                        render: function (data, type, full) {
                            var returnValue = '<a class="btn btn-xs grey-cascade" onclick="CommonDoc.getInfo(\'' + full.ufileId +'\')"  id="add' + full.ufileId + '"><i class="fa fa-link"></i>详细</a>'
                            return returnValue
                        }
                    }
                ]
            }
        });


        //重新载入按钮事件
        $("#reloadTable").click(function () {
            reloadTable(false);
        });
    }

    return {
        init: function () {
            Common();
        },
        search:function(){
            reloadTable(false);
        },
        getInfo:function (id) {
        $("#dialogTitle").text("详细信息");
        $("#userEditIframe").attr("src", basePath + "getInfomation?id=" + id);
        $("#modalDialog").modal("show");
        }
    }
}();
