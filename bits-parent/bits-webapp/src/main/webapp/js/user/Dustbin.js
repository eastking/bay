/**
 * Created by qiutiandong on 2016/6/4 0004.
 */
var Dustbin = function () {
    var reloadTable = function (pageFlag) {
        $("#datatable_ajax").DataTable().draw(pageFlag)
    };
    var onSortColumn = function (sortColumn, sortDirection) {
        switch (sortColumn) {
            case "ufileName":
                sortColumn = "ufile_name";
                break;
        }
        return onSortColumnDefault(sortColumn, sortDirection);
    };
    var handleRecords = function () {
        var grid = new DataTable();
        var $table = $("#datatable_ajax");
        grid.addAjaxParam("userId", $("#userId").val());
        grid.init({
            src: $table,
            url: basePath + "userCenter/getDelArrangement",
            onSortColumn: onSortColumn,
            onQuery: function (data) {
                data.ufileName = $("#ufileName").val();
            },
            dataTable: {
                "columns": [
                    {data: 'ufileName', orderable: true},
                    {data: 'ufileUrl', orderable: true},
                    {data: 'delTime', orderable: true},
                    {
                        data: 'operate', orderable: false,
                        render: function (data, type, full) {
                            var returnValue = '<a class="add btn green" data-target="#addRowConfirm" data-toggle="modal"><i class="fa fa-plus"></i>恢复</a>'
                            returnValue += '<a class="delete btn red btn-xs black faa-flash animated-hover " data-target="#deleteRowConfirm" data-toggle="modal"><i class="fa fa-trash-o"></i>清除</a>'
                            return returnValue
                        }
                    }
                ]
            }
        });


        //重新载入按钮事件
        $("#reloadTable").click(function () {
            reloadTable(false);
        });

        $table.find('a.delete').live('click', function () {
            var $this = $(this);
            //confirm中确认按钮事件，此处需要unbind，否则点击取消时下次再点击删除按钮会重复绑定click。
            $("#deleteRow").unbind("click.deleteRow").bind("click.deleteRow", function () {
                var $row = $table.DataTable().row($this.parents('tr')[0]);
                $.ajax({
                    url:basePath + "userCenter/delArrangementDus?id=" + $row.data().id,
                    dataType: "json",
                    type: "DELETE",
                    success: function () {
                        $table.DataTable().draw();
                        $('#deleteRowConfirm').modal('hide');
                        toast.success_del();
                    },
                    error: doError
                })
            })
        });

        $table.find('a.add').live('click',function () {
            var $this=$(this);
            $('#addRow').unbind("click.addRow").bind("click.addRow",function () {
                var $row = $table.DataTable().row($this.parents('tr')[0]);
                $.ajax({
                    url: basePath + "userCenter/addArrangement",
                    data:{
                        delId:$("#checkId").val(),
                        id:$row.data().id
                    },
                    dataType: "json",
                    type: "POST",
                    success:function () {
                        $table.DataTable().draw();
                        $('#addRowConfirm').modal('hide');
                        toast.success_addArrangement();
                    },
                    error:doError
                })
            })
        });

    }

    return {
        init: function () {
            handleRecords();
        },
        search:function(){
            reloadTable(false);
        }
    }
}();
