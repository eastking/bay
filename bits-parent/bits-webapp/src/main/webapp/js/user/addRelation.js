
var addRelation = function () {
    function delValidates(checkboxName, text) {
        text = text == undefined ? "关联" : text;
        var $checkbox = $("input[name=" + checkboxName + "]:checkbox");
        if ($checkbox.size() <= 0) {
            confirm("没有可操作的记录！");
            return false;
        }
        if ($checkbox.filter(":checked").size() < 1) {
            confirm.warn("请至少选择一个选项！");
            return false
        }
        return confirm("您确定要将这" + $checkbox.filter(":checked").size() + "项" + text + "吗？");
    }
    function getCheckedId(checkboxName){
        var checkedIds = [];
        $("input[name=" + checkboxName + "]:checked").each(function () {
            checkedIds.push(this.value);
        });
        return checkedIds;
    }

    return {
        addFileRelationDo:function(fileId1) {
            if (!delValidates("fileId2"))return;
            $.ajax({
                url: basePath + "userCenter/addFileRelation",
                data: {
                    fileId1: fileId1,
                    fileId2: getCheckedId("fileId2")
                },
                dataType: "json",
                type:  requestMethod.POST,
                success:function () {
                   // toast.success_addRRelation();
                    window.location.href = basePath + "userCenter/toFileRelationPage?id=" + fileId1
                },
                error: doError
            });
        },
        addFileRelationDoTest:function(fileId1,fileId2) {
            $.ajax({
                url: basePath + "userCenter/addFileRelationTest",
                data: {
                    fileId1: fileId1,
                    fileId2: fileId2
                },
                dataType: "json",
                type:  requestMethod.POST,
                success:function () {
                    //toast.success_addRRelation();
                    window.location.href = basePath + "userCenter/toFileRelationPage?id=" + fileId1
                },
                error: doError
            });
        },
        deleteRelation:function(fileId1,fileId2){
            $.ajax({
                url: basePath+ "userCenter/deleteFileRelation",
                data:{
                    fileId1: fileId1,
                    fileId2: fileId2,
                },

                dataType: "json",
                type:  requestMethod.POST,
                success:function () {
                    //toast.success_DeleteRelation();
                    window.location.href = basePath + "userCenter/toFileRelationPage?id=" + fileId1
                },
                error: doError
            })
        },
        initSelect2:function(){
            $("#fastSearch").select2({
                placeholder:"选择一个文件来关联"
            })
            $("#fastSearch").change(function (){
                if ($(this).val() == "")
                    return false;
                // window.toThemeExperts($(this).val());
                addRelation.addFileRelationDoTest($("#fileId").val(),$(this).val())
            })
        }
    }
}();
