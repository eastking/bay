function uploadAttachment() {
    var file = $("#attachmentFile").val()

    if (file == "") {
        toast.warning("请先选择上传文件，再进行操作")
        return;
    }
    formSub(basePath + "userCenter/uploadAttachment");

}
function addDownfile(fileId) {
    $.ajax({
        url: basePath + "userCenter/addDownfile",
        data: {
            userId: $("#userId").val(),
            fileId: fileId
        },
        dataType: "json",
        type: "POST"
    })
}

function addCheckHistory(fileId) {
    $.ajax({
        url: basePath + "userCenter/addCheckHistory",
        data: {
            userId: $("#userId").val(),
            fileId: fileId
        },
        dataType: "json",
        type: "POST"
    })
}

function fileDownload(filePath,fileId){
   if($("#userId").val()){
    addDownfile(fileId);
    location.href = basePath + "downFile?filePath=" + filePath;
   }
    else
       toast.error("请登录后再下载!");
}

function filePreview(fileUrl,fileId){
    addCheckHistory(fileId);
    window.open(basePath + "previewFile?filePath=" + fileUrl);
}

function getInfo(id) {
    $("#userEditIframe").attr("src", basePath + "getInfo?id=" + id);
    $("#modalDialog").modal("show");
}

function getSearchInfo(id) {
    $("#dialogTitle").text("详细信息");
    $("#userEditIframe").attr("src", basePath + "getInfo?id=" + id);
    $("#modalDialog").modal("show");

    alert("测试");
}

function fileSearch(){
    formSub(basePath + "fileSearch");
}

function abcSearch(){
    formSub(basePath + "abcSearch");
}

