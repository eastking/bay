/**
 * Created by qiutiandong on 2016/6/4 0004.
 */
var Arrangement = function () {
    var reloadTable = function (pageFlag) {
        $("#datatable_ajax").DataTable().draw(pageFlag)
    };
    var onSortColumn = function (sortColumn, sortDirection) {
        switch (sortColumn) {
            case "ufileName":
                sortColumn = "ufile_name";
                break;
        }
        return onSortColumnDefault(sortColumn, sortDirection);
    };
    var handleRecords = function () {
        var grid = new DataTable();
        var $table = $("#datatable_ajax");
        grid.addAjaxParam("userId", $("#userId").val());
        grid.init({
            src: $table,
            url: basePath + "userCenter/getArrangementVideo",
            onSortColumn: onSortColumn,
            onQuery: function (data) {
                data.ufileName = $("#ufileName").val();
            },
            dataTable: {
                "columns": [
                    {data: 'ufileName', orderable: true},
                    {data: 'ufileTime', orderable: true},
                    {
                        data: 'operate', orderable: false,
                        render: function (data, type, full) {
                            var returnValue = '<a class="add btn green" onclick="Arrangement.getInfomation(\'' + full.ufileId +'\')"  id="getIfo' + full.ufileId + '"><i class="fa fa-file-o "></i>查看详细</a>'
                            return returnValue
                        }
                    },
                    {
                        data: 'operate', orderable: false,
                        render: function (data, type, full) {
                            var returnValue = '<a class="edit btn default btn-xs purple" onclick="Arrangement.editInfo(\'' + full.id + '\')" id="edit' + full.id + '"><i class="fa fa-edit faa-shake animated-hover"></i>编辑</a>'
                            returnValue += '<a class="delete btn red btn-xs black faa-flash animated-hover " data-target="#deleteRowConfirm" data-toggle="modal"><i class="fa fa-trash-o"></i>删除</a>'
                            //returnValue += '<a class="add btn green" data-target="#addRowConfirm" data-toggle="modal"><i class="fa fa-mail-forward "></i>发送</a>'
                            //returnValue += '<a class="btn btn-xs grey-cascade" onclick="Arrangement.addFileRelation(\'' + full.ufileId +'\')"  id="add' + full.ufileId + '"><i class="fa fa-link"></i>关联</a>'
                            return returnValue
                        }
                    },
                    {
                        data: 'operate', orderable: false,
                        render: function (data, type, full) {
                            var returnValue = '<a class="add btn green" data-target="#addRowConfirm" data-toggle="modal"><i class="fa fa-mail-forward "></i>发送</a>'
                            return returnValue
                        }
                    },
                    {
                        data: 'operate', orderable: false,
                        render: function (data, type, full) {
                            var returnValue = '<a class="btn btn-xs grey-cascade" onclick="Arrangement.addFileRelation(\'' + full.ufileId +'\')"  id="add' + full.ufileId + '"><i class="fa fa-link"></i>关联</a>'
                            return returnValue
                        }
                    }
                ]
            }
        });


        //重新载入按钮事件
        $("#reloadTable").click(function () {
            reloadTable(false);
        });

        $table.find('a.delete').live('click', function () {
            var $this = $(this);
            //confirm中确认按钮事件，此处需要unbind，否则点击取消时下次再点击删除按钮会重复绑定click。
            $("#deleteRow").unbind("click.deleteRow").bind("click.deleteRow", function () {
                var $row = $table.DataTable().row($this.parents('tr')[0]);
                $.ajax({
                    url:basePath + "userCenter/delArrangementVideo?id=" + $row.data().id,
                    dataType: "json",
                    type: "DELETE",
                    success: function () {
                        $table.DataTable().draw();
                        $('#deleteRowConfirm').modal('hide');
                        toast.success();
                    },
                    error: doError
                })
            })
        });

        $table.find('a.add').live('click',function () {
            var $this=$(this);
            $('#addRow').unbind("click.addRow").bind("click.addRow",function () {
                var $row = $table.DataTable().row($this.parents('tr')[0]);
                $.ajax({
                    url: basePath + "userCenter/addCommon",
                    data:{
                        ucommonId:$("#checkId").val(),
                        id:$row.data().id
                    },
                    dataType: "json",
                    type: "POST",
                    success:function () {
                        $('#addRowConfirm').modal('hide');
                        toast.success_addCommon();
                    },
                    error:doError
                })
            })
        });

    }

    return {
        init: function () {
            handleRecords();
        },
        editInfo: function (id) {
            $("#dialogTitle").text("编辑");
            $("#userEditIframe").attr("src", basePath + "userCenter/editInfo?id=" + id);
            $("#modalDialog").modal("show");
        },
        search:function(){
            reloadTable(false);
        },
        quit: function () {
            location.href = "about:blank";
            parent.parent.$("#modalDialog").modal("hide");
        },
        editSave: function () {
            $('#editForm').validate({
                onsubmit: true,
                errorElement: 'span', //default input error message container
                errorClass: 'help-block', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                rules: {
                    file_name: {
                        required: true
                    },
                    filekeyword:{
                        required: true
                    },
                    file_abstract:{
                        required: true
                    }
                },
                messages: {
                    file_name: {
                        required: "知识名称不能为空！"
                    },
                    filekeyword:{
                        required: "关键词不能为空!"
                    },
                    file_abstract:{
                        required: "描述内容不能为空!"
                    }
                },
                highlight: function (element) { // hightlight error inputs
                    $(element)
                        .closest('.form-group').addClass('has-error'); // set error class to the control group
                },

                success: function (label) {
                    label.closest('.form-group').removeClass('has-error');
                    label.remove();
                },

                errorPlacement: function (error, element) {
                    error.insertAfter(element.closest('.form-control'));
                },

                submitHandler: function (form) {
                    $("#saves").button('loading');
                    $.ajax({
                        url: basePath + "userCenter/editFileName",
                        data: {
                            id:$("#checkId").val(),
                            ufileName:$("#file_name").val(),
                            ufileAbstract:$("#file_abstract").val(),
                            ufileKeyword:$("#filekeyword").val()
                        },
                        dataType: "json",
                        type: "POST",
                        success: function () {
                            // location.href = "about:blank";
                            // parent.parent.$("#modalDialog").modal("hide");
                            // var $table = parent.$("#datatable_ajax");
                            // $table.DataTable().draw();
                            parent.toast.success_arragement();
                        },
                        error: doError
                    })
                }
            });
        },
        addFileRelation:function(id){
            $("#dialogTitle").text("添加关联");
            $("#userEditIframe").attr("src", basePath + "userCenter/toFileRelationPage?id=" + id);
            $("#modalDialog").modal("show");
        },
        getInfomation:function (id) {

            $("#userEditIframe").attr("src", basePath + "getInfomation?id=" + id);
            $("#modalDialog").modal("show");
        },
        reloadTable: reloadTable
    }
}();
