/**
 * Created by qiutiandong on 2016/6/5 0005.
 */
var Treasure = function(){

    return {
        initSelect2:function(){
            $("#fastSearch").select2({
                placeholder:"选择一个文件"
            })
            $("#fastSearch").change(function (){
                if ($(this).val() == "")
                    return false;
            })
            $("#fastSearch3").select2({
                placeholder:"选择一个领域"
            })
            $("#fastSearch3").change(function (){
                if ($(this).val() == "")
                    return false;
            })

            $("#fastSearch4").select2({
                placeholder:"选择一个文件编号"
            })
            $("#fastSearch4").change(function (){
                if ($(this).val() == "")
                    return false;
                Treasure.showFileRelationDoTest($(this).val())
            })
        },
        showFileRelationDoTest:function(fileId1) {
            $.ajax({
                url: basePath + "userCenter/showAllFileRelationById",
                data: {
                    id: fileId1,
                },
                dataType: "json",
                type:  requestMethod.POST,
                success:function () {
                //    toast.success_addRRelation();
                window.location.href = basePath + "userCenter/handTreasure?id="+fileId1;
                },
                error: doError
            });
        },
        addShare: function () {
            $.ajax({
                url: basePath + "userCenter/addShare",
                data: {
                    userId: $("#userId").val(),
                    ufileName:$("#fastSearch").find("option:selected").text(),
                    ufileId: $("#fastSearch").val(),
                    ufileKeyword: $("#keyword").val(),
                    themeType: $("#fastSearch3").val(),
                    ufileAbstract: $("#desc").val()
                },
                dataType: "json",
                type: "POST",
                success: function () {
                    setTimeout(function (){toast.success("共享成功!")},500);
                    setTimeout(function (){window.location.reload()},2000);
                },
                error: doError
            })

            // $('#share').validate({
            //     onsubmit: true,
            //     errorElement: 'span', //default input error message container
            //     errorClass: 'help-block', // default input error message class
            //     focusInvalid: false, // do not focus the last invalid input
            //     rules: {
            //         fastSearch: {
            //             required: true
            //         },
            //         fastSearch3: {
            //             required: true
            //         },
            //         keyword : {
            //             required: true
            //         },
            //         desc: {
            //             required: true
            //         }
            //     },
            //     messages: {
            //         fastSearch: {
            //             required: "文件不能为空！"
            //         },
            //         fastSearch3: {
            //             required: "领域不能为空！"
            //         },
            //         keyword: {
            //             required: "关键词不能为空!"
            //         },
            //         desc: {
            //             required: "描述不能为空!"
            //         }
            //     },
            //     highlight: function (element) { // hightlight error inputs
            //         $(element)
            //             .closest('.form-group').addClass('has-error'); // set error class to the control group
            //     },
            //
            //     success: function (label) {
            //         label.closest('.form-group').removeClass('has-error');
            //         label.remove();
            //     },
            //
            //     errorPlacement: function (error, element) {
            //         error.insertAfter(element.closest('.form-control'));
            //     },
            //
            //     submitHandler: function (form) {
            //         $("#addshare").button('loading');
            //         $.ajax({
            //             url: basePath + "userCenter/addShare",
            //             data: {
            //                 userId: $("#userId").val(),
            //                 ufileName: $("#fastSearch").val(),
            //                 ufileKeyword: $("#keyword").val(),
            //                 themeType: $("#fastSearch3").val(),
            //                 ufileAbstract: $("#desc").val()
            //             },
            //             dataType: "json",
            //             type: "POST",
            //             success: function () {
            //                 window.location.reload();
            //                 parent.toast.success_share();
            //             },
            //             error: doError
            //         })
            //     }
            // });
        },
        getInfo:function (id) {
            $("#userEditIframe").attr("src", basePath + "getInfo?id=" + id);
            $("#modalDialog").modal("show");
        },
        getInfomation:function (id) {
            $("#userEditIframe").attr("src", basePath + "getInfomation?id=" + id);
            $("#modalDialog").modal("show");
        },
        

}
}();