var HistoryPage = function () {
    var reloadTable = function (pageFlag) {
        $("#historyInfo").DataTable().draw(pageFlag)
    };

    var handleRecords = function () {
        var grid = new DataTable();
        var $table = $("#historyInfo");
        grid.addAjaxParam("userId", $("#userId").val());
        grid.init({
            src: $table,
            url: basePath + "userCenter/getUserHistoryUp",
            dataTable: {
                "columns": [
                    {data: 'ufileName', orderable: true},
                    {data: 'ufileUrl', orderable: true},
                    {data: 'uploadtime', orderable: true},
                    
                    {
                        data: 'operate', orderable: false,
                        render: function (data, type, full) {
                            var returnValue = "";
                            returnValue += '<a class="delete btn red btn-xs black faa-flash animated-hover " data-target="#deleteRowConfirm" data-toggle="modal"><i class="fa fa-trash-o"></i>删除</a>'
                            return returnValue
                        }
                    }
                ]
            }
        });


        //重新载入按钮事件
        $("#reloadTable").click(function () {
            reloadTable(false);
        });

        $table.find('a.delete').live('click', function () {
            var $this = $(this);
            //confirm中确认按钮事件，此处需要unbind，否则点击取消时下次再点击删除按钮会重复绑定click。
            $("#deleteRow").unbind("click.deleteRow").bind("click.deleteRow", function () {
                var $row = $table.DataTable().row($this.parents('tr')[0]);
                $.ajax({
                    url:basePath + "userCenter/delUserHistoryUp?id=" + $row.data().id,
                    dataType: "json",
                    type: "DELETE",
                    success: function () {
                        $table.DataTable().draw();
                        $('#deleteRowConfirm').modal('hide');
                        toast.success();
                    },
                    error: doError
                })
            })
        });
    }

    return {
        init: function () {
            handleRecords();
        }
    }
}();