/**
 * Created by qiutiandong on 2016/6/9 0009.
 */
var Collection = function(){
    return {
        addCollect: function (fileId) {
            $.ajax({
                url: basePath + "userCenter/addCollect",
                data: {
                    userId: $("#userId").val(),
                    ufileId: fileId
                },
                dataType: "json",
                type: "POST",
                success: function () {
                    setTimeout(function () {toast.success("收藏成功!")}, 500)
                },
                error: doError
            })
        }
    }
}();