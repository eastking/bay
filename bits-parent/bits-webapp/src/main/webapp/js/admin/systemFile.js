
function refreshPicture(){
    window.location.href = basePath + "admin/file/listAllPictureDo";
}

function refreshFile(){
    window.location.href = basePath + "admin/file/listAllFileDo";
}

function refreshVideo(){
    window.location.href = basePath + "admin/file/listAllVideoDo";
}

function refreshMusic(){
    window.location.href = basePath + "admin/file/listAllMusicDo";
}



function deleteManyFileInfo() {
    if (!delValidate("fileIds"))return;
    coverToggle();
    $.ajax({
        url: basePath + "admin/file/deleteManyFileInfo",
        data: {
            fileIds: getCheckedIds("fileIds")
        },
        type: requestMethod.POST,
       // success: function () {
      //     window.location.href = basePath + "admin/file/listAllPictureDo";
      // }
    });
}
function getCheckedIds(checkboxName){
    var checkedIds = [];
    $("input[name=" + checkboxName + "]:checked").each(function () {
        checkedIds.push(this.value);
    });
    return checkedIds;
}



function coverToggle(content) {
    content = content==undefined?"内容加载中，请稍等...":content;
    if ($("#carpoCover").size() == 0) {
        var html = '<div id="carpoCover" class="coverDiv"></div>' +
            '<span id="carpoSpan" class="coverSpan"></span>';
        $("body").append(html);
    } else if ($("#carpoCover")[0].style.display != "none") {
        $("#carpoCover,#carpoSpan").hide();
        return;
    }
    $("#carpoSpan").html(content);
    if(content==""){
        $("#carpoSpan").removeClass("coverSpanBack")
    }else{
        $("#carpoSpan").addClass("coverSpanBack")
    }
    var rel = document.compatMode == 'CSS1Compat' ? document.documentElement : document.body;
    var width = Math.max(rel.scrollWidth, rel.clientWidth || 0) - 1;
    var height = Math.max(rel.scrollHeight, rel.clientHeight || 0) - 1;
    $("#carpoCover").css({
        width: width + 'px',
        height: height + 'px'
    }).show();
    $("#carpoSpan").css({
        top:(height - 20) / 2,
        left:(width - 160) / 2
    }).show();
}



function delValidate(checkboxName, text) {
    text = text == undefined ? "删除" : text;
    var $checkbox = $("input[name=" + checkboxName + "]:checkbox");
    if ($checkbox.size() <= 0) {
        alertMsg.warn("没有可操作的记录！");
        return false;
    }
    if ($checkbox.filter(":checked").size() < 1) {
        alertMsg.warn("请至少选择一个选项！");
        return false
    }
    return confirm("您确定要将这" + $checkbox.filter(":checked").size() + "项" + text + "吗？");
}




function  editInfo(id) {

            $("#dialogTitle").text("文件编辑")
            $("#userEditIframe").attr("src", basePath + "admin/file/editFileDo?id=" + id);
            $("#modalDialog").modal("show");

        }


function  quitFile() {
            location.href = "about:blank";
            parent.parent.$("#modalDialog").modal("hide");
        }

function  editFileSave() {

            $("#saves").button('loading');
            $.ajax({
                url: basePath + "admin/file/editFile",
                data: {
                    fileId: $("#fileId").val(),
                    fileName: $("#fileName").val(),
                    fileType: $("#fileType").val(),
                    fileSize: $("#fileSize").val(),
                },
                dataType: "json",
                type: "POST",
                success: function () {
                    location.href = "about:blank";
                    parent.parent.$("#modalDialog").modal("hide");
                    var $table = parent.$("#datatable_ajax");
                    $table.DataTable().draw();
                    parent.toast.success();
                },
                error: doError
            })

        }
