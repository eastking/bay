/**
 * Created by Demon on 2015/5/18.
 */
var AutoAnswerPage = function () {
    var color = [
        "color1",
        "color2",
        "color3",
        "color4",
        "color5",
        "color6",
        "color7",
        "color8",
        "color9",
        "color10"
    ];

    var cont = $('#chats');
    var list = $('.chats', cont);
    var form = $('.chat-form', cont);
    var input = $('input', form);
    var btn = $('.btn', form);
    var container = $("#answerExperts");

    var count = 0;

    function getAnswerContent(answer) {
        var tpl = "";
        var time = new Date();
        var time_str = time.toLocaleDateString();
        tpl += '<li class="in">';
        tpl += '<img class="avatar" alt="" src="' + basePath + '/assets/admin/layout/img/avatar2.jpg"/>';
        tpl += '<div class="message">';
        tpl += '<span class="arrow"></span>';
        tpl += '<a href="#list'+ count +'" class="name">知识库小助手</a>&nbsp;';
        tpl += '<span class="datetime">—— ' + time_str + '</span>';
        tpl += '<span class="body">';
        tpl += answer;
        tpl += '</span>';
        tpl += '</div>';
        tpl += '</li>';
        var newLi = $(tpl);
        $(".chats li.in .message .name",newLi).fancybox({
            'centerOnScroll': true
        });
        list.append(newLi);
        $('.scroller', cont).slimScroll({
            scrollTo: list.height()
        });
    }

    function getAnswerExpertDiv(experts) {
        var wrap = $('<div class="col-md-12 expertsWrap" id="list'+ count +'">');
        var str = "";
        if (experts == null || experts.length == 0) {
            str += '<div class="note note-danger text-center">';
            str += '<h4 class="block">该问题暂无相关专家</h4></div>';
            wrap.append(str);
            wrap.appendTo(container);
            count++;
            return;
        }
        for (var i = 0, l = experts.length; i < l; i++) {
            str += '<div class="note note-success col-md-12">';
            str += '<div class="col-md-2">';
            str += '<img src="'+ basePath + experts[i].img +'" alt="专家头像"></div>';
            str += '<div class="col-md-10">';
            str += '<a href="javascript:;" class="expertName" onclick="toExpertDetail('+ experts[i]["id"] +')">'+ experts[i]["expertName"] +' <sub>—'+ experts[i]["expertOrg"] +'</sub></a>';
            str += '<a href="#email" class="btn btn-sm green contact">联系他</a>'
            str += '<p style="margin: 10px 0px">';
            str += '领域 :' + getTheme(experts[i]["themeList"]) + '</p></div></div>';
        }
        wrap.append(str);
        $("a.contact",wrap).each(function (){
           $(this).fancybox({
               'centerOnScroll': true,
               'onCleanup':function (){
                   $("#email input,textarea").val("");
               }
           });
        });
        wrap.appendTo(container);
        count++;
    }

    function getTheme(themeList){
        if (themeList.length == 0) return "暂无";
        var str = "";
        for (var i = 0,j = themeList.length; i < j; i++){
            str += '<a class="theme color'+ (i%color.length + 1) +'" href="javascript:;" onclick="toThemeExperts('+ themeList[i].id +')">' + themeList[i]["themeName"] + '</a>';
        }
        return str;
    }

    function isValidQuestion(val) {
        return val == null || "" == val.trim();
    }

    function getAnswer() {
        $.ajax({
            url: basePath + "autoAnswer/getAnswer",
            type: "post",
            data: {question: input.val()},
            dataType: "json",
            success:
                function (result) {
                    var answer=result.text;
                getAnswerContent(answer);
                getAnswerExpertDiv(answer);
            }
        })
    }

    return {
        init: function () {
            $("#clear").on("click",function(){
                $("#chats .chats > li:gt(0)").remove();
                $("#answerExperts").empty();
                toast.success("清除成功!");
            });
        },
        getAnswer:function (){
            getAnswer();
        }
    }
}();
