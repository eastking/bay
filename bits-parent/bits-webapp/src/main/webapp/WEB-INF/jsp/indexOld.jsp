<%--
    Created by panzhuowen on 2015/2/6.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<head>
    <meta charset="UTF-8">
    <title>比特能专家</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

    <meta content="Metronic Shop UI description" name="description">
    <meta content="Metronic Shop UI keywords" name="keywords">
    <meta content="keenthemes" name="author">

    <meta property="og:site_name" content="-CUSTOMER VALUE-">
    <meta property="og:title" content="-CUSTOMER VALUE-">
    <meta property="og:description" content="-CUSTOMER VALUE-">
    <meta property="og:type" content="website">
    <meta property="og:image" content="-CUSTOMER VALUE-">
    <!-- link to image for socio -->
    <meta property="og:url" content="-CUSTOMER VALUE-">

    <link rel="shortcut icon" href="favicon.ico">
    <link href="favicon.ico" rel="SHORTCUT ICON" type="image/ico">
    <style>
        .topIcon {
            position:absolute;
            top:0px;
            right:0px;
        }
        .header{
            background-color: rgba(255,255,255,0.4) !important;
        }
        body.page-header-fixed.custom{
            padding-top: 0px !important;
        }
    </style>
    <jsp:include page="includes/style.jsp"/>
</head>
<body class="corporate page-header-fixed custom">
<%--<jsp:include page="includes/frontend-top.jsp"/>--%>
<jsp:include page="includes/header.jsp"/>
<br>
<div class="" style="position:absolute;top: 0;left: 0; width: 100%; height: 100%">
<jsp:include page="includes/pageSlider.jsp"/>
</div>
<!-- Header END -->
<div class="main" style="position: absolute;top: 100%; left:0;width: 100%;height: auto">
    <div class="container">
        <!-- BEGIN SERVICE BOX -->
        <div class="row service-box margin-bottom-40">
            <div class="col-md-4 col-sm-4" style="background-color:#FFFAC5;height:150px">
                <div class="service-box-heading">
                    <em><i class="fa fa-location-arrow blue"></i></em>
                    <span><a style="color:black" href="expertWeb/getExpertWeb">主题专家网</a></span>
                </div>
                <p>输入您感兴趣的主题，我们将为您呈现与该主题相关的专家网络，为您的提供更多相关主题领域的专家。</p>
            </div>
            <div class="col-md-4 col-sm-4" style="background-color:#D6F4F7;height:150px">
                <div class="service-box-heading">
                    <em><i class="fa fa-check red"></i></em>
                    <span><a style="color:black" href="expertRecommend/getExpertRecommend">专家推荐</a></span>
                </div>
                <p>查找某主题的内容，我们将为您推荐相关领域的核心专家。
                    能够输入一段描述所要研究的课题内容，系统自动推荐出与该研究方向可能相关的专家列表或图谱，
                    推荐的专家应该给出推荐的依据。</p>
            </div>
            <div class="col-md-4 col-sm-4" style="background-color:#E2CFFF;height:150px">
                <div class="service-box-heading">
                    <em><i class="fa fa-compress green"></i></em>
                    <span><a style="color:black" href="autoAnswer/getPage">自动问答</a></span>
                </div>
                <p>为您解决相关领域的问题,对于用户问题可以同时由多个专家机器人合作解答，形成面向复杂问题的“会诊”应用。</p>
            </div>
        </div>
        <!-- BEGIN BLOCKQUOTE BLOCK -->
        <div class="row quote-v1 margin-bottom-30">
            <div class="col-md-9">
                <i class="fa fa-suitcase" style=color:white></i>
                <span>行业知识服务</span>
            </div>
        </div>
        <!-- END BLOCKQUOTE BLOCK -->

        <!-- BEGIN RECENT WORKS -->
        <div class="row recent-work margin-bottom-40">
            <div class="col-md-3">
                <h2><a>模块功能简介</a></h2>

                <p></p>

                <p>为您整理出本机器人系统中热门行业，并且提供专门的行业内部内容提供，为用户提供更专业的行业知识。</p>
            </div>
            <div class="col-md-9">
                <div class="owl-carousel owl-carousel3">
                    <c:forEach items="${TopTheme}" var="theme">
                        <div class="recent-work-item">
                            <em>
                                <img src="<c:url value="${theme.img}"/>"
                                     alt="${theme.themeName}"
                                     class="img-responsive">
                                <a href="javascript:void(0)" onclick="toThemeExperts('${theme.id}')"><i
                                        class="fa fa-search"></i></a>
                                <img class="topIcon" src=""/>
                            </em>
                            <a class="recent-work-description"
                               href="javascript:void(0)" onclick="toThemeExperts('${theme.id}')">
                                <strong>${theme.themeName}</strong>
                            </a>
                        </div>
                    </c:forEach>
                </div>
            </div>
        </div>
        <!-- END RECENT WORKS -->
        <!-- BEGIN TABS AND TESTIMONIALS -->
        <shiro:hasPermission name="user">
        <div class="row mix-block margin-bottom-40">
            <!-- TABS -->
            <div>
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#tab-1" data-toggle="tab">用户推荐</a></li>
                    <li><a href="#tab-2" data-toggle="tab">近期检索足迹</a></li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane row fade in active" id="tab-1">
                        <div class="col-md-12">
                            <c:if test="${recommendExpert == null || recommendExpert.isEmpty()}">
                                <h2 style="color:black;"><c:out value="暂无"/></h2>
                            </c:if>
                            <c:if test="${!recommendExpert.isEmpty()}">
                                <div class="scroller" style="height:280px" data-always-visible="1"
                                     data-rail-visible="0">
                                    <c:forEach items="${recommendExpert}" var="rExpert">
                                        <div class="row">
                                            <div class="note col-md-12"
                                                 style="padding-left: 0px;padding-right: 0px;">
                                                <div class="col-md-1">
                                                    <img src="<c:url value="${rExpert.img}"/>"
                                                         alt="" style="width: 60px;height: 60px;">
                                                </div>
                                                <div class="col-md-11">
                                                    <h4 class="media-heading"><a href="javascript:void(0)"
                                                                                 onclick="toExpertDetail('${rExpert.id}')">${rExpert.expertName}</a>
                                                    </h4>

                                                    <p>
                                                        <c:forEach items="${rExpert.themeList}" var="theme">
                                                            <a href="javascript:void(0)" class="btn default"
                                                               onclick="toThemeExperts('${theme.id}')">${theme.themeName}</a>
                                                        </c:forEach>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </c:forEach>
                                </div>
                            </c:if>
                        </div>
                    </div>
                    <div class="tab-pane row fade" id="tab-2">
                        <div class="col-md-12">
                            <c:if test="${historyExpert == null || historyExpert.isEmpty()}">
                                <h2 style="color:black;"><c:out value="暂无"></c:out></h2>
                            </c:if>
                            <c:if test="${!historyExpert.isEmpty()}">
                                <div class="scroller" style="height:280px" data-always-visible="1"
                                     data-rail-visible="0">
                                    <c:forEach items="${historyExpert}" var="hExpert">
                                        <div class="row">
                                            <div class="note col-md-12" style="padding-left: 0px;padding-right: 0px;">
                                                <div class="col-md-1">
                                                    <img src="<c:url value="${hExpert.img}"/>"
                                                         alt="" style="width: 60px;height: 60px;">
                                                </div>

                                                <div class="col-md-11">
                                                    <h4 class="media-heading"><a href="javascript:void(0)"
                                                                                 onclick="toExpertDetail('${hExpert.id}')">${hExpert.expertName}</a>
                                                    </h4>

                                                    <p>
                                                        <c:forEach items="${hExpert.themeList}" var="theme">
                                                            <a href="javascript:void(0)" class="btn default"
                                                               onclick="toThemeExperts('${theme.id}')">${theme.themeName}</a>
                                                        </c:forEach>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </c:forEach>
                                </div>
                            </c:if>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END TABS -->
        </div>
        </shiro:hasPermission>
    </div>
</div>
<!-- END SERVICE BOX -->
<!-- BEGIN FOOTER -->
<jsp:include page="includes/footer.jsp"/>
<jsp:include page="includes/script.jsp"/>
<!-- END FOOTER -->
<script src="<c:url value="/js/frontend/IndexPage.js"/>" type="text/javascript"></script>
<script>
    $(document).ready(function () {
        IndexPage.init();
    })
</script>
</body>
</html>