<%--
  Created by IntelliJ IDEA.
  User: qiutiandong
  Date: 2016/5/27 0027
  Time: 16:26
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
    <title>${User.username}&nbsp;的文件上传</title>
    <%@include file="../includes/style.jsp"%>

    <script type="text/javascript" src="../js/jquery-1.10.2.js"></script>
    <script type="text/javascript" src="../js/user/upload.js"></script>
    <script type="text/javascript">
        i = 1;
        j = 1;
        //$(document).ready(function(){
        function addNew(){
            var up = $("#up");
            $(".form-body").append("<div id='div"+i+"'>"+up.html()+"<input type='button' class='delete btn red btn-xs black faa-flash animated-hover ' value='删除'  onclick='del_1("+i+")'/></div></div>");
            /*document.getElementById("form-body").innerHTML+='<div id="div_'+i+'"><label  class="col-md-2 control-label">名称： </label><input type="text" id="fileName" name="fileName"  class="form-control input-inline input-sm input-medium"/><input type="button" value="删除"  onclick="del_1('+i+')"/></div>';*/

            i = i + 1;
        }
        // $("#btn_add2").click(function(){

        // });


        //});

        function del_1(o){
            $("#div"+o).remove();
        }



    </script>

</head>
<body>
<%@include file="../includes/header.jsp"%>
<div class="main">
    <div class="container">
        <ul class="breadcrumb">
            <li><a href="javascript:;" onclick="toHome()">首页</a></li>
            <li><a href="javascript:;">用户中心</a></li>
            <li class="active">文件上传</li>
        </ul>
        <!-- BEGIN SIDEBAR & CONTENT -->
        <div class="row margin-bottom-40">
            <!-- BEGIN SIDEBAR -->
            <%@include file="../includes/user-menu.jsp"%>
            <!-- END SIDEBAR -->
            <!-- BEGIN CONTENT -->
            <div class="col-md-9 col-sm-9">
                <h1>文件上传</h1>
                <div class="content-form-page">
                    <div class="row">



                        <form action="#" id="form_sample_1" class="form-horizontal" method="post" enctype="multipart/form-data" >
                            <div class="form-body">

                                <div id="up">
                                    <div class="form-group">
                                        <div id="file_name" class="form-group">
                                            <label class="col-md-2 control-label">名称：</label>
                                            <input id="fileName" name="fileName" class="form-control input-inline input-sm input-medium" type="text">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="file_keyword"  class="form-group">
                                            <label class="col-md-2 control-label">关键字：</label>
                                            <input id="fileKeyword" name="fileKeyword" class="form-control input-inline   input-small " type="text">

                                        </div>
                                    </div>



                                    <div class="form-group">
                                        <label class="col-md-2 control-label">说明：
                                        </label>
                               <textarea id="explain" name="explain" cols="40" rows="4">
                               </textarea>
                                    </div>


                                    <div class="form-group">
                                        <label for="attachmentFile" class="col-md-2 control-label">文件：</label>
                                        <input id="attachmentFile" name="files" style="width: 240px;" type="file">
                                        <%--<br>--%>
                                        <%--<p>请采用rar，zip，pdf，doc，xls，ppt格式文件--%>
                                            <%--文件小于10M--%>
                                        <%--</p>--%>
                                    </div>
                                    <div class="form-group">
                                        <div class="file_keyword"  class="form-group">
                                            <label class="col-md-7 control-label">请采用rar，zip，pdf，doc，xls，ppt格式文件，文件小于10M</label>
                                        </div>
                                    </div>



                                </div>

                                <div class="form-actions fluid">
                                    <div class="col-md-offset-4 col-md-4">
                                        <input class="btn green btn-sm" id="btn_add2" value="增加一行" type="button" onclick="addNew()">
                                        <a onclick="uploadAttachment()" class="btn green btn-sm">上传</a>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="attachmentFile" class="col-md-2 control-label"></label>
                                </div>

                            </div>
                        </form>
                        <!-- BEGIN FORM-->
                    </div>
                </div>
            </div>
            <!-- END CONTENT -->
        </div>
        <!-- END SIDEBAR & CONTENT -->
    </div>
</div>
<%@include file="../includes/script.jsp"%>
<script src="<c:url value="/js/user/ChangePasswordPage.js"/>" type="text/javascript"></script>
<script src="<c:url value="/assets/global/plugins/jquery-validation/js/jquery.validate.min.js"/>"
        type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function (){
        ChangePasswordPage.init();
    })
</script>
</body>
</html>