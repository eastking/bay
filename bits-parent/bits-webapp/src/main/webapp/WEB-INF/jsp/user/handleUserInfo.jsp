<%--
  Created by IntelliJ IDEA.
  User: Shy
  Date: 2015/8/10
  Time: 9:23
  To change this template use File | Settings | File Templates.
--%>
<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
  <title>${User.username}&nbsp;的个人信息</title>
  <%@include file="../includes/style.jsp"%>
  <style>
    .color1{
      background-color: rgb(242,229,186);
    }
    .color2{
      background-color: rgb(245,222,196);
    }
    .color3{
      background-color: rgb(235,227,225);
    }
    .color4{
      background-color: rgb(222,235,225);
    }
    .color5{
      background-color: rgb(203,214,205);
    }
    .color6{
      background-color: rgb(226,211,182);
    }
    .color7{
      background-color: rgb(247,196,182);
    }
    .color8{
      background-color: rgb(203,214,205);
    }
    .color9{
      background-color: rgb(157,185,206);
    }
    .color10{
      background-color: rgb(210,173,181);
    }
    table a{
      text-decoration: none !important;
      padding: 3px 3px;
      cursor: pointer;
    }
  </style>
</head>
<body>
<%@include file="../includes/header.jsp"%>

<div class="main">
  <div class="container">
    <ul class="breadcrumb">
      <li><a href="javascript:;" onclick="toHome()">首页</a></li>
      <li><a href="javascript:;">用户中心</a></li>
      <li class="active">个人资料</li>
    </ul>
    <!-- BEGIN SIDEBAR & CONTENT -->
    <div class="row margin-bottom-40">
      <!-- BEGIN SIDEBAR -->
      <%@include file="../includes/user-menu.jsp"%>
      <!-- END SIDEBAR -->
      <div class="col-md-9 col-sm-9">
        <ul class="nav nav-tabs">
          <li  class="active">
            <a href="#tab_2_1" data-toggle="tab">个人资料</a>
          </li>
        </ul>
        <div class="tab-content">
          <div class="tab-pane fade active in" id="tab_2_1">
            <div class="row">
              <div class="portlet-body">
                <div class="col-md-12 col-sm-13">
                  <div class="content-form-page">
                    <!-- BEGIN CONTENT -->
                      <div class="content-form-page">
                        <div class="row">
                          <table class="table table-bordered">
                            <tbody>
                            <tr>
                              <td align="center" width="15%">登陆账号</td>
                              <td align="center">${User.loginName}</td>
                            </tr>
                            <%--<tr>--%>
                            <%--<td align="center" width="15%">部门</td>--%>
                            <%--<td align="center">${User.username}</td>--%>
                            <%--</tr>--%>
                            <%--<tr>--%>
                            <%--<td align="center" width="15%">邮箱</td>--%>
                            <%--<td align="center">${User.email}</td>--%>
                            <%--</tr>--%>
                            <tr>
                              <td align="center" width="15%">关注的知识</td>
                              <td id="focusTheme">
                                <c:if test="${focusThemes.size() == 0}">
                                  <c:out value="暂无"></c:out>
                                </c:if>
                                <c:if test="${focusThemes.size() != 0}">
                                  <c:forEach items="${focusThemes}" var="theme">
                                    <a class="" onclick="toThemeExperts('${theme.id}')">${theme.themeName}</a>
                                  </c:forEach>
                                </c:if>
                              </td>
                            </tr>
                            <%--<tr>--%>
                            <%--<td align="center" width="15%">关注的主题</td>--%>
                            <%--<td id="focusExpert">--%>
                            <%--<c:if test="${focusExpert.size() == 0}">--%>
                            <%--<c:out value="暂无"></c:out>--%>
                            <%--</c:if>--%>
                            <%--<c:if test="${focusExpert.size() != 0}">--%>
                            <%--<c:forEach items="${focusExpert}" var="expert">--%>
                            <%--<a class="" onclick="toExpertDetail('${expert.id}')">${expert.expertName}</a>--%>
                            <%--</c:forEach>--%>
                            <%--</c:if>--%>
                            <%--</td>--%>
                            <%--</tr>--%>
                            </tbody>
                          </table>
                        </div>
                      </div>
                    <!-- END CONTENT -->
                  </div>
                </div>
              </div>
            </div>
          </div>

        </div>
      </div>
    </div>
  </div>
    <!-- END SIDEBAR & CONTENT -->
  </div>
</div>
<%@include file="../includes/script.jsp"%>
<script>
  $(document).ready(function (){
    var color = [
      "color1",
      "color2",
      "color3",
      "color4",
      "color5",
      "color6",
      "color7",
      "color8",
      "color9",
      "color10"
    ];
    var l = color.length;
    $("a","#focusTheme").each(function(index){
      $(this).addClass(color[index % l]);
    });
    $("a","#focusExpert").each(function(index){
      $(this).addClass(color[index % l]);
    });
  })
</script>
</body>
</html>
