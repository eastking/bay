<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>查看关联</title>
    <jsp:include page="../includes/style.jsp"/>
    <link href="<c:url value="/assets/frontend/pages/css/portfolio.css"/>" rel="stylesheet" type="text/css">
    <link href="<c:url value="/assets/frontend/pages/css/gallery.css"/>" rel="stylesheet" type="text/css">
</head>
<body>
<div class="main">
    <!-- BEGIN SIDEBAR & CONTENT -->
    <a class="btn red" data-toggle="modal" href="#basic">
        当前的文件文件名:${file.ufileName}&nbsp;文件编号：${file.ufileId}</a>
    <br><br>
    <input type="hidden" value="${file.ufileId}" class="fileId" id="fileId"/>
    <ul class="nav nav-tabs">
        <li  class="active">
            <a href="#tab_2_1" data-toggle="tab">已关联</a>
        </li>
        <li>
            <a href="#tab_2_2" data-toggle="tab">未关联</a>
        </li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane fade active in" id="tab_2_1">
            <div class="row">
                <table class="table table-striped table-bordered table-hover" >
                    <thead>
                    <tr>
                        <th>
                            文件Id
                        </th>
                        <th>
                            文件名
                        </th>
                        <th>
                            操作
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach items="${file1}" var="file1">
                        <tr class="odd gradeX">
                            <td>
                                    ${file1.fileId2}
                            </td>
                            <td>
                                    ${file1.ufileName}
                            </td>
                            <td>
                            <button class="delete btn red btn-xs black faa-flash animated-hover "  onclick="addRelation.deleteRelation(${file.ufileId},${file1.fileId2})">删除</button>
                            </td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="tab-pane fade" id="tab_2_2">
            <div class="row">
                <div class="portlet-body">
                    <!-- BEGIN CONTENT -->
                    <div class="col-md-1 col-sm-8">
                        <button id="sample_editable_1_new" class="btn green" onclick="addRelation.addFileRelationDo(${file.ufileId})">
                            添加关联<i class="fa fa-plus"></i>
                        </button>
                    </div>
                    <div class="col-md-12 col-sm-4">
                        <select class="form-control select2" id="fastSearch">
                            <option></option>
                            <c:forEach items="${file3}" var="file3">
                                <option value="${file3.ufileId}">${file3.ufileId}</option>
                            </c:forEach>
                        </select>
                    </div>
                    <!-- END CONTENT -->
                    <table class="table table-striped table-bordered table-hover" id="sample_1">
                        <thead>
                        <tr>
                            <th class="table-checkbox">
                            </th>
                            <th>
                                知识名
                            </th>
                            <th>
                                知识Id
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        <c:forEach var="file3" items="${file3}">
                            <tr class="odd gradeX">
                                <td style="text-align:center">
                                    <input type="checkbox" name="fileId2" title="${file3.ufileId}" value="${file3.ufileId}">
                                </td>
                                <td>
                                        ${file3.ufileId}
                                </td>
                                <td>
                                        ${file3.ufileName}
                                </td>
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<jsp:include page="../includes/script.jsp"/>
<script src="<c:url value="/js/user/addRelation.js"/>" type="text/javascript"></script>
<%--xinde--%>
<script type="text/javascript">
    $(document).ready(function () {

        addRelation.initSelect2();
    });
</script>
</body>
</html>