<%--
  Created by IntelliJ IDEA.
  User: qiutiandong
  Date: 2016/5/27 0027
  Time: 16:27
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
    <title>${User.username}&nbsp;的百宝箱</title>
    <%@include file="../includes/style.jsp"%>
</head>
<body>
<%@include file="../includes/header.jsp"%>

<input type="hidden" value="${User.userId}" id="userId">
<div class="main">
    <div class="container">
        <ul class="breadcrumb">
            <li><a href="javascript:;" onclick="toHome()">首页</a></li>
            <li><a href="javascript:;">用户中心</a></li>
            <li class="active">百宝箱</li>
        </ul>
        <!-- BEGIN SIDEBAR & CONTENT -->
        <div class="row margin-bottom-40">
            <!-- BEGIN SIDEBAR -->
            <%@include file="../includes/user-menu.jsp"%>
            <!-- END SIDEBAR -->
            <!-- BEGIN CONTENT -->
            <div class="col-md-9 col-sm-9">
                <ul class="nav nav-tabs">
                    <li  class="active">
                        <a href="#tab_2_1" data-toggle="tab">图片类</a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane fade active in" id="tab_2_1">
                        <div class="row">
                            <div class="portlet-body">
                                <div class="col-md-12 col-sm-13">
                                    <div class="content-form-page">
                                        <div class="row">
                                            <div class="content-form-page">
                                                <div class="row">
                                                    <div class="portlet-body">
                                                        <div style="text-align:right;height: 28px">
                                                            <form id="query">
                                                                <input id="ufileName" type="text" name="expertName" value="" style="margin-bottom: 0" class="input-small0" style="height:28px" placeholder="search">
                                                                <a class="btn btn-small blue" onclick="Arrangement.search()">
                                                                    <i class="fa fa-search faa-shake animated-hover"></i>查询</a>
                                                            </form>
                                                        </div>
                                                        <div class="table-responsive">
                                                            <div class="table-toolbar">
                                                                <div class="btn-group pull-right">
                                                                    <a href="" class="btn default btn-sm dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                                                        按类型查看 <span class="fa fa-angle-down">
										                                </span>
                                                                    </a>
                                                                    <ul class="dropdown-menu pull-right">
                                                                        <li>
                                                                            <a href="handArrangementImg" id="regional_stat_world">
                                                                                图片类 </a>
                                                                        </li>
                                                                        <li>
                                                                            <a href="handArrangementVideo" id="regional_stat_usa">
                                                                                视频类</a>
                                                                        </li>
                                                                        <li>
                                                                            <a href="handArrangementMusic" id="regional_stat_europe">
                                                                                音频类 </a>
                                                                        </li>
                                                                        <li>
                                                                            <a href="handArrangementOther" id="regional_stat_russia">
                                                                                其他 </a>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                            <table class="table table-striped table-bordered table-advance table-hover" id="datatable_ajax">
                                                                <input id="checkId" name="checkId" type="hidden" value="1"/>
                                                                <%--<col width="20%">--%>
                                                                <%--<col width="20%">--%>
                                                                <%--<col width="20%">--%>
                                                                <%--<col width="10%">--%>
                                                                <col width="10%">
                                                                <col width="20%">
                                                                <thead>
                                                                <tr>
                                                                    <th class="text-center">
                                                                        <i class="fa fa-tag"></i> 知识名称
                                                                    </th>
                                                                    <th class="text-center">
                                                                        <i class="fa fa-clock-o"></i>
                                                                        查看时间
                                                                    </th>
                                                                    <th class="text-center">
                                                                        <i class="fa fa-file"></i>
                                                                        查看详细
                                                                    </th>
                                                                    <th class="text-center">
                                                                        <i class="fa fa-wrench"></i>
                                                                        操作
                                                                    </th>
                                                                    <th class="text-center">
                                                                        <i class="fa fa-mail-forward "></i>
                                                                        发送
                                                                    </th>
                                                                    <th class="text-center">
                                                                        <i class="fa fa-chain "></i>
                                                                        关联
                                                                    </th>
                                                                </tr>
                                                                </thead>
                                                                <tbody class="text-center">
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END CONTENT -->
        </div>
        <!-- END SIDEBAR & CONTENT -->
    </div>
</div>
<div class="modal fade" id="deleteRowConfirm" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">确认</h4>
            </div>
            <div class="modal-body">确认删除当前文件？</div>
            <div class="modal-footer">
                <button type="button" class="btn btn-sm blue filter-cancel" data-dismiss="modal">取消</button>
                <button type="button" class="btn btn-sm red filter-cancel" id="deleteRow">删除</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<div class="modal fade" id="addRowConfirm" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">确认</h4>
            </div>
            <div class="modal-body">确认发送到常用文档？</div>
            <div class="modal-footer">
                <button type="button" class="btn btn-sm blue filter-cancel" data-dismiss="modal">取消</button>
                <button type="button" class="btn btn-sm green filter-cancel" id="addRow">确认</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<div class="modal fade" id="modalDialog" tabindex="-1" role="basic" aria-hidden="true" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog" style="width: 900px;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title" id="dialogTitle"></h4>
            </div>
            <div class="modal-body">
                <div class="portlet-body">
                    <iframe id="userEditIframe" style="border: none;width:858px;height:350px;" frameBorder="0"></iframe>
                    <center>
                        <a href="###" onclick="javascript:filePreview('${file.fileUrl}')"  class="btn btn-success">预览</a>
                        <%--<button onclick="fileDownload('${p.filePath}');" class="btn btn-info">下载</button>--%>
                        <a href="###" onclick="javascript:fileDownload('${file.fileUrl}');"  class="btn btn-info"> 下载</a>
                    </center>
                </div>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<%@include file="../includes/script.jsp"%>
<script src="<c:url value="/js/plugins/data-tables/jquery.dataTables.js"/>" type="text/javascript"></script>
<script src="<c:url value="/js/plugins/data-tables/DT_bootstrap.js"/>" type="text/javascript"></script>
<script src="<c:url value="/js/global/datatable.js"/>" type="text/javascript"></script>
<script src="<c:url value="/js/user/ArrangementImg.js"/>" type="text/javascript"></script>
<script type="text/javascript" src="<c:url value="/js/user/upload.js"/>"></script>
<script type="text/javascript">
    $(document).ready(function (){
        Arrangement.init();
    })
</script>
</body>
</html>
