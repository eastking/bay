<%--
  Created by IntelliJ IDEA.
  User: qiutiandong
  Date: 2016/5/27 0027
  Time: 16:26
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
    <title>${User.username}&nbsp;的整理箱</title>
    <%@include file="../includes/style.jsp"%>
</head>
<body>
<%@include file="../includes/header.jsp"%>

<div class="main">
    <div class="container">
        <ul class="breadcrumb">
            <li><a href="javascript:;" onclick="toHome()">首页</a></li>
            <li><a href="javascript:;">用户中心</a></li>
            <li class="active">整理箱</li>
        </ul>
        <!-- BEGIN SIDEBAR & CONTENT -->
        <div class="row margin-bottom-40">
            <!-- BEGIN SIDEBAR -->
            <%@include file="../includes/user-menu.jsp"%>
            <!-- END SIDEBAR -->
            <!-- BEGIN CONTENT -->
            <div class="col-md-9 col-sm-9">
                <ul class="nav nav-tabs">
                    <li  class="active">
                        <a href="#tab_2_1" data-toggle="tab">关联</a>
                    </li>
                    <li>
                        <a href="#tab_2_2" data-toggle="tab">规则</a>
                    </li>
                    <li>
                        <a href="#tab_2_3" data-toggle="tab">分享</a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane fade" id="tab_2_3">
                        <div class="row">
                            <div class="portlet-body">
                                <div class="col-md-12 col-sm-13">
                                    <div class="content-form-page">
                                        <div class="row">
                                            <form action="#" class="form-horizontal" id="share">
                                                <input type="hidden" value="${User.userId}" id="userId" name="userId">
                                                <div class="form-body">
                                                    <div id="up">
                                                        <div class="form-group col-md-12">
                                                            <div id="file_name" class="form-group">
                                                                <label class="col-md-2 control-label">选择分享的文件:</label>
                                                                <div class="col-md-4">
                                                                    <select class="form-control select2" id="fastSearch" name="fastSearch">
                                                                        <option></option>
                                                                        <c:forEach items="${myFiles}" var="userFileVO">
                                                                            <option value="${userFileVO.ufileId}">${userFileVO.ufileName}</option>
                                                                        </c:forEach>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="form-group col-md-12">
                                                            <div id="file_keyword" class="form-group">
                                                                <label class="col-md-2 control-label">选择分享的领域:</label>
                                                                <div class="col-md-4">
                                                                    <select class="form-control select2" id="fastSearch3" name="fastSearch3">
                                                                        <option></option>
                                                                        <c:forEach items="${cacheThemes}" var="theme">
                                                                            <option value="${theme.themeName}">${theme.themeName}</option>
                                                                        </c:forEach>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="form-group col-md-12">
                                                            <label class="col-md-2">关键词:</label>
                                                            <input id="keyword" name="keyword" class="form-control input-inline default col-md-4" type="text">
                                                        </div>

                                                        <div class="form-group col-md-12">
                                                            <label class="col-md-2">描述：</label>
                                                            <textarea id="desc" name="desc" cols="40" rows="4" class="col-md-4"></textarea>
                                                        </div>

                                                    </div>

                                                    <div class="form-actions fluid">
                                                        <div class="col-md-offset-4 col-md-4">
                                                            <a class="btn green btn-sm" onclick="Treasure.addShare()" id="addshare">共享</a>
                                                        </div>
                                                    </div>

                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="tab_2_2">
                        <div class="row">
                            <div class="portlet-body">
                                <div class="container">
                                    <div class="form-body">

                                        <div>
                                            <div class="form-group">
                                                <div id="" class="form-group">
                                                    <label class="col-md-3 control-label">设置自动清除垃圾箱天数：</label>
                                                    <div class="col-md-8">
                                                        <select class="form-control input-xsmall input-sm">
                                                            <option value="5">5天</option>
                                                            <option value="10">10天</option>
                                                            <option value="20">20天</option>
                                                            <option value="30">30天</option>
                                                            <option value="60">60天</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                </div>
                            </div>
                        </div>
                    </div>
                        </div>
                    <div class="tab-pane fade active in" id="tab_2_1">
                        <div class="form-group col-md-12">
                            <div id="file_name1" class="form-group">
                                <label class="col-md-4 control-label">请选择要查看关联的文件:</label>
                                <div class="col-md-4">
                                    <select class="form-control select2" id="fastSearch4">
                                        <option></option>
                                        <c:forEach items="${file}" var="file">
                                            <option value="${file.ufileId}">${file.ufileName} 编号:${file.ufileId}</option>
                                        </c:forEach>
                                    </select>
                                </div>
                            </div>
                            <br><br>
                            <table class="table table-striped table-bordered table-hover" id="sample_1">
                                <thead>
                                <tr>
                                    <th class="text-center">
                                        知识id
                                    </th>
                                    <th class="text-center">
                                        知识名
                                    </th>
                                    <th class="text-center">
                                        查看详细
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                <c:forEach var="File" items="${File}">
                                    <c:if test="${File.ufileId != null}">
                                        <tr class="odd gradeX">
                                            <td>
                                                    ${File.ufileId}
                                            </td>
                                            <td>
                                                    ${File.ufileName}
                                            </td>
                                            <td>
                                                <a class="add btn green" onclick="Treasure.getInfomation(${File.ufileId})"><i class="fa fa-file-o "></i>查看详细</a>
                                            </td>
                                        </tr>
                                    </c:if>
                                </c:forEach>
                                </tbody>
                            </table>
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <!-- END CONTENT -->
    </div>
    <!-- END SIDEBAR & CONTENT -->
</div>
<div class="modal fade" id="modalDialog" tabindex="-1" role="basic" aria-hidden="true" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog" style="width: 900px;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title" id="dialogTitle"></h4>
            </div>
            <div class="modal-body">
                <div class="portlet-body">
                    <iframe id="userEditIframe" style="border: none;width:858px;height:350px;" frameBorder="0"></iframe>
                </div>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<%@include file="../includes/script.jsp"%>
<script src="<c:url value="/assets/global/plugins/jquery-validation/js/jquery.validate.min.js"/>" type="text/javascript"></script>
<script src="<c:url value="/js/user/Treasure.js"/>" type="text/javascript"></script>
<script type="text/javascript" src="<c:url value="/js/user/upload.js"/>"></script>

<script type="text/javascript">
    $(document).ready(function (){
        Treasure.initSelect2();
    })
</script>
</body>
</html>