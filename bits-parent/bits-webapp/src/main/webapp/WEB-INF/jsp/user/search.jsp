<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>主题列表</title>
    <jsp:include page="../includes/style.jsp"/>
    <link href="<c:url value="/assets/frontend/pages/css/portfolio.css"/>" rel="stylesheet" type="text/css">
    <link href="<c:url value="/assets/frontend/pages/css/gallery.css"/>" rel="stylesheet" type="text/css">
    <style>
        .content-page a {
            font-weight: 600;
            color: #ffffff;
            text-decoration: none;
        }

        .content-page a:hover {
            color: #ffffff;
            text-decoration: none;
        }

        .owl-buttons {
            position: relative !important;
            margin: 10px auto;
            top: 0px !important;
            right: 0px !important;
            left: 0px !important;
            text-align: center;
        }
        strong{
            text-align: center;
        }
        .topIcon {
            position:absolute;
            top:0px;
            right:0px;
        }
    </style>


    <style type="text/css">

        input.百货产业 { background-color: #996699 !important;
            background-image: none !important;
            border-color: #4b8df8 !important;
            color: white !important;}
        input.超市产业 { background-color: #303030 !important;
            background-image: none !important;
            border-color: #4b8df8 !important;
            color: white !important;}
        input.其他产业 { background-color: #FF6600 !important;
            background-image: none !important;
            border-color: #4b8df8 !important;
            color: white !important;}


    </style>
</head>
<body>
<jsp:include page="../includes/header.jsp"/>
<%@include file="../includes/heads.jsp"%>
<br>
<div class="main">
    <div class="container">
        <ul class="breadcrumb">
            <li><a href="javascript:void(0)" onclick="toHome()">首页</a></li>
            <li class="active">知识库</li>
        </ul>
        <!-- CONTENT -->
<a href="../attachment/1465171747329.xlsx" download="ww">下载</a>

        <!-- BEGIN SIDEBAR & CONTENT -->
    </div>
</div>
<jsp:include page="../includes/script.jsp"/>
<script src="<c:url value="/assets/global/plugins/jquery-mixitup/jquery.mixitup.min.js"/>"
        type="text/javascript"></script>
<script src="<c:url value="/js/plugins/echarts-2.2.1/build/dist/echarts-all.js"/>"></script>
<script src="<c:url value="/js/frontend/ThemePage.js"/>" type="text/javascript"></script>

</body>
</html>

