<%--
  Created by IntelliJ IDEA.
  User: Shy
  Date: 2015/8/10
  Time: 10:41
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>${User.username}&nbsp;的历史记录</title>
    <%@include file="../includes/style.jsp" %>
</head>
<body>
<%@include file="../includes/header.jsp" %>

<input type="hidden" value="${User.userId}" id="userId">
<div class="main">
    <div class="container">
        <ul class="breadcrumb">
            <li><a href="javascript:;" onclick="toHome()">首页</a></li>
            <li><a href="javascript:;">用户中心</a></li>
            <li class="active">历史记录</li>
        </ul>
        <!-- BEGIN SIDEBAR & CONTENT -->
        <div class="row margin-bottom-40">
            <!-- BEGIN SIDEBAR -->
            <%@include file="../includes/user-menu.jsp" %>
            <!-- END SIDEBAR -->
            <div class="col-md-9 col-sm-9">
                <ul class="nav nav-tabs">
                    <li  class="active">
                        <a href="#tab_2_1" data-toggle="tab">下载历史</a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane fade active in" id="tab_2_1">
                        <div class="row">
                            <div class="portlet-body">
                                <div class="col-md-12 col-sm-13">
                                    <div class="content-form-page">
                                        <div class="row">
                                            <!-- BEGIN CONTENT -->
                                            <div class="content-form-page">
                                                <div class="row">
                                                    <div class="portlet-body">
                                                        <div class="table-responsive">
                                                            <div class="table-toolbar">
                                                                <div class="btn-group pull-right">
                                                                    <a href="" class="btn default btn-sm dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                                                        历史类别 <span class="fa fa-angle-down">
										                                </span>
                                                                    </a>
                                                                    <ul class="dropdown-menu pull-right">
                                                                        <li>
                                                                            <a href="handleHistory" id="regional_stat_world">
                                                                                浏览历史 </a>
                                                                        </li>
                                                                        <li>
                                                                            <a href="handleHistoryUp" id="regional_stat_usa">
                                                                                上传历史</a>
                                                                        </li>
                                                                        <li>
                                                                            <a href="handleHistoryDown" id="regional_stat_germany">
                                                                                下载历史 </a>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                            <table class="table table-striped table-bordered table-advance table-hover" id="historyInfo">
                                                                <thead>
                                                                <tr>
                                                                    <th class="text-center">
                                                                        <i class="fa fa-tag"></i>
                                                                        知识名称
                                                                    </th>
                                                                    <th class="text-center">
                                                                        <i class="fa fa-chain "></i>
                                                                        知识链接
                                                                    </th>
                                                                    <th class="text-center">
                                                                        <i class="fa fa-clock-o"></i>
                                                                        下载时间
                                                                    </th>
                                                                    <th class="text-center">
                                                                        <i class="fa fa-wrench"></i>
                                                                        操作
                                                                    </th>
                                                                </tr>
                                                                </thead>
                                                                <tbody class="text-center">
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="deleteRowConfirm" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">确认</h4>
            </div>
            <div class="modal-body">确认删除当前历史记录？</div>
            <div class="modal-footer">
                <button type="button" class="btn btn-sm blue filter-cancel" data-dismiss="modal">取消</button>
                <button type="button" class="btn btn-sm red filter-cancel" id="deleteRow">删除</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<%@include file="../includes/script.jsp"%>
<script src="<c:url value="/js/plugins/data-tables/jquery.dataTables.js"/>" type="text/javascript"></script>
<script src="<c:url value="/js/plugins/data-tables/DT_bootstrap.js"/>" type="text/javascript"></script>
<script src="<c:url value="/js/global/datatable.js"/>" type="text/javascript"></script>
<script src="<c:url value="/js/user/HistoryPageDown.js"/>" type="text/javascript"></script>
<script>
    $(document).ready(function (){
        HistoryPage.init();
    })
</script>
</body>
</html>
