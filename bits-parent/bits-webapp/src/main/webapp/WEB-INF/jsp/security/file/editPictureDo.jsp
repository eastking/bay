
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8"/>
    <title>用户新增页</title>
    <%@include file="../../includes/adminCommon.jsp"%>
    <script src="<c:url value="/js/admin/systemUser.js"/>" type="text/javascript"></script>
    <script src="<c:url value="/js/admin/systemFile.js"/>" type="text/javascript"></script>
</head>
<body style="background: #ffffff">
<div class="page-container">
    <div class="page-content-wrapper">
        <div class="page-content">
            <div class="row">
                <div class="col-md-12">
                    <div class="portlet box">
                        <div class="portlet-body form">
                            <!-- BEGIN FORM-->
                            <form action="#" class="form-horizontal">
                                <div class="form-body">
                                    <div class="row">


                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label class="control-label col-sm-3">文件ID
                                                    <span class="required" aria-required="true">* </span>
                                                </label>

                                                <div class="col-sm-6">
                                                    <input type="text" class="form-control input-medium" id="fileId"
                                                           name="fileId"
                                                           value="${fileVO.fileId}"/>
                                                </div>


                                            </div>
                                        </div>

                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label class="control-label col-sm-3">文件名
                                                    <span class="required" aria-required="true">* </span>
                                                </label>

                                                <div class="col-sm-6">
                                                    <input type="text" class="form-control input-medium" id="fileName"
                                                           name="fileName"
                                                           value="${fileVO.fileName}"/>
                                                </div>


                                            </div>
                                        </div>

                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label class="control-label col-sm-3">文件类型
                                                    <span class="required" aria-required="true">* </span>
                                                </label>

                                                <div class="col-sm-6">
                                                    <input type="text" class="form-control input-medium" id="fileType"
                                                           name="fileType"
                                                           value="${fileVO.fileType}"/>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label class="control-label col-sm-3">文件大小
                                                    <span class="required" aria-required="true">* </span>
                                                </label>

                                                <div class="col-sm-6">
                                                    <input type="text" class="form-control input-medium" id="fileSize"
                                                           name="fileSize"
                                                           value="${fileVO.fileSize}"/>
                                                </div>
                                            </div>
                                        </div>

                                    </div>


                                </div>
                                <div class="form-actions right">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="col-md-offset-3 col-md-9">
                                                <button type="button" class="btn default green"
                                                        id="saves" onclick=" editFileSave()">保存
                                                </button>
                                                &nbsp
                                                <button type="button" class="btn default"
                                                        onclick="quitFile()">关闭
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </form>
                            <!-- END FORM-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>
