<%--
  Created by IntelliJ IDEA.
  User: panzhuowen
  Date: 2014/10/17
  Time: 19:00
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
    <title>图片管理</title>
    <%@include file="../../includes/adminCommon.jsp"%>
    <script src="<c:url value="/js/admin/systemFile.js"/>" type="text/javascript"></script>
    <script src="<c:url value="/js/admin/systemUser.js"/>" type="text/javascript"></script>
</head>
<body class="page-header-fixed">
<%@include file="../../includes/top.jsp"%>
<div class="page-container" data-options="tools:'#tab-tools'">
    <div class="page-sidebar-wrapper">
        <div class="page-sidebar navbar-collapse collapse">
            <jsp:include page="../../includes/menu.jsp">
                <jsp:param name="systemNum" value="2"/>
                <jsp:param name="systemLeaf" value="2_2"/>
            </jsp:include>
        </div>
    </div>
    <div class="page-content-wrapper">
        <div class="page-content" style="min-height:1033px !important" >
            <!-- BEGIN PAGE HEADER-->
            <div class="row">
                <div class="col-md-12">
                    <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                    <h3 class="page-title"></h3>
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <i class="fa fa-home"></i>
                            <span>主页</span>
                            <i class="fa fa-angle-right"></i>
                        </li>
                        <li>
                            <span>用户管理</span>
                            <i class="fa fa-angle-right"></i>
                        </li>
                        <li>
                            <span></span>
                        </li>
                    </ul>
                    <!-- END PAGE TITLE & BREADCRUMB-->
                </div>
            </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
            <div class="row">

                <div class="portlet box blue">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-edit"></i>Editable Table
                        </div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse">
                            </a>
                            <a href="#portlet-config" data-toggle="modal" class="config">
                            </a>
                            <a href="javascript:;" class="reload"  onclick=" refreshPicture()">
                            </a>
                            <a href="javascript:;" class="remove">
                            </a>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="table-toolbar">
                            <%--<div class="btn-group">--%>
                            <%--<button id="sample_editable_1_new" class="btn green">--%>
                            <%--Add New <i class="fa fa-plus"></i>--%>
                            <%--</button>--%>
                            <%--</div>--%>
                            <%--<div class="btn-group pull-right">--%>
                                <%--<button class="btn dropdown-toggle" data-toggle="dropdown">Tools <i class="fa fa-angle-down"></i>--%>
                                <%--</button>--%>
                                <%--<ul class="dropdown-menu pull-right">--%>
                                    <%--<li>--%>
                                        <%--<a href="#">--%>
                                            <%--Print </a>--%>
                                    <%--</li>--%>
                                    <%--<li>--%>
                                        <%--<a href="#">--%>
                                            <%--Save PDF </a>--%>
                                    <%--</li>--%>
                                    <%--<li>--%>
                                        <%--<a href="#">--%>
                                            <%--Export to Excel </a>--%>
                                    <%--</li>--%>



                                <%--</ul>--%>
                            <%--</div>--%>

                            <a href="#" class="btn btn-default" onclick="deleteManyFileInfo()"><i class="fa fa-trash-o faa-spin animated-hover"></i><spring:message code="COMMON_DELETE"/></a>


                        </div>
                        <table class="table table-striped table-hover table-bordered" id="sample_editable_1">
                            <thead>
                            <tr>

                                <th>
                                    选择框
                                </th>
                                <th>
                                    文件
                                </th>
                                <th>
                                    文件名
                                </th>
                                <th>
                                    文件类型
                                </th>
                                <th>
                                    文件大小
                                </th>
                                <th>
                                    Edit
                                </th>
                                <th>
                                    download
                                </th>
                            </tr>
                            </thead>
                            <tbody>

                            <c:forEach var="fileVO" items="${fileList}">
                                <tr>
                                    <td> <input type="checkbox" name="fileIds" value="${fileVO.fileId}"></td>







                                    <td> <a   target="_blank"  href="<c:url value="${fileVO.filePath}"/>">
                                        <img src="<c:url value="${fileVO.filePath}"/>" id="ContentShow" style="width: 100px;height: 100px; ">
                                    </a></td>
                                    <td>${fileVO.fileName}</td>
                                    <td>${fileVO.fileType}</td>

                                    <td>${fileVO.fileSize}</td>



                                        <%--<td> <img src="<c:url value=" ${fileVO.fileAddress}"/>"></td>--%>

                                    <td>
                                        <a class="edit btn default btn-xs purple" onclick="editInfo(' + ${fileVO.fileId} + ')" id="edit"><i class="fa fa-edit faa-shake animated-hover"></i>编辑</a>

                                    </td>


                                    <td>       <a href="<c:url value="${fileVO.filePath}"/>" download="${fileVO.fileName}.jpg">下载</a>

                                    </td>
                                </tr>
                            </c:forEach>
                            </tbody>
                        </table>
                    </div>
                </div>






            </div>
            <!-- END PAGE CONTENT-->
        </div>
    </div>
</div>
<!-- BEGIN FOOTER -->
<%@include file="../../includes/footer.jsp" %>
<div class="modal fade" id="modalDialog" tabindex="-1" role="basic" aria-hidden="true" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog" style="width: 900px;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title" id="dialogTitle"></h4>
            </div>
            <div class="modal-body">
                <div class="portlet-body">
                    <iframe id="userEditIframe" style="border: none;width:858px;height:350px;" frameBorder="0"></iframe>
                </div>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- END FOOTER -->
<div class="modal fade" id="deleteRowConfirm" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">确认</h4>
            </div>
            <div class="modal-body">确认删除当前行？</div>
            <div class="modal-footer">
                <button type="button" class="btn default" data-dismiss="modal">取消</button>
                <button type="button" class="btn blue" id="deleteRow">删除</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
</body>
</html>
<script type="text/javascript">
    $(function () {
        userListTable.init();
    });
</script>
