<%--
  Created by IntelliJ IDEA.
  User: mafuyu
  Date: 2016/5/19
  Time: 18:34
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<html>
<head>
    <title>问答详情</title>
    <jsp:include page="../includes/style.jsp"/>

</head>
<body>
<jsp:include page="../includes/header.jsp"/>

<div class="main">
    <div class="container">
        <ul class="breadcrumb">
            <li><a href="index.html">首页</a></li>
            <li><a href="javascript:void(0)" onclick="toLiterature()">专家问答</a></li>
            <li class="active">问答详情</li>
        </ul>
        <!-- BEGIN SIDEBAR & CONTENT -->
        <div class="row margin-bottom-40">
            <!-- BEGIN CONTENT -->
            <div class="col-md-12 col-sm-12">
                <div class="content-page">
                    <div class="row">
                        <!-- BEGIN LEFT SIDEBAR -->
                        <div class="col-md-12 col-sm-12 blog-item">
                            <h2>${Literature.title}</h2>
                            <ul class="blog-info">
                                <li><i class="fa fa-calendar"></i> ${Literature.appDate}</li>
                                <li><i class="fa fa-comments"></i> ${Literature.unit}同济大学热能工程系;</li>
                                <li><i class="fa fa-tags"></i>${Literature.authorCn}</li>
                            </ul>
                            <p>${Literature.abs}</p>
                        </div>
                        <hr>
                        <h2>相关专家</h2>

                        <div class="comments">
                            <c:forEach items="${Experts}" var="expert">
                                <div class="media">
                                    <a href="javascript:void(0)" class="pull-left">
                                        <img src="<c:url value="${expert.img}"/>"
                                             alt=""
                                             class="media-object" style="width:100px">
                                    </a>

                                    <div class="media-body">
                                        <h4 class="media-heading">
                                            <a href="javascript:void(0)" onclick="toExpertDetail('${expert.id}')">${expert.expertName}
                                            </a></h4>

                                        <p></p>

                                        <p><i class="fa fa-tags"></i><span>&nbsp;单位:${expert.expertOrg}</span></p>

                                        <p><i class="fa fa-tags"></i><span>&nbsp;专家领域:
                                            <c:choose>
                                                <c:when test="${!expert.themeList.isEmpty()}">
                                                    <c:forEach items="${expert.themeList}" var="theme">
                                                        <a href="javascript:void(0)" class="btn default" onclick="toThemeExperts('${theme.id}')">${theme.themeName}</a>
                                                    </c:forEach>
                                                </c:when>
                                                <c:when test="${expert.themeList.isEmpty()}">
                                                    <span style="font-size:17px;font-weight: bolder"><c:out value="暂无"></c:out></span>
                                                </c:when>
                                            </c:choose>
                                        </span></p>
                                    </div>
                                    <div class="col-md-9">
                                        <button class="btn red"  data-target="#deleteRowConfirm" data-toggle="modal">向该专家提问</button>
                                    </div>
                                </div>
                            </c:forEach>
                        </div>
                        <br><br>
                        <div>
                            <ul class="nav nav-tabs">
                                <li  class="active">
                                    <a href="#tab_2_1" data-toggle="tab">已回答</a>
                                </li>
                                <li>
                                    <a href="#tab_2_2" data-toggle="tab">未回答</a>
                                </li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane fade active in" id="tab_2_1">
                                    <div class="row">
                                        <div class="note note-success">
                                                <div class="message">
                                                <span class="arrow">
                                                </span>
                                                    <a href="#email" class="name">

                                                        <a class="btn green" data-toggle="modal" href="#">
                                                            user 问
                                                        </a>
                                                    </a>
                                            <span class="datetime">
                                                ——2016-6-13
                                            </span>
                                                    <br> <br>
                                            <span class="body">
                                                时间片用完了，不管这个线程的优先级有多高都不会在运行，而是进入 就绪队列中，等待下一个时间片的到了?
                                            </span>
                                                </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="note note-danger">
                                            <div class="message">
                                                <span class="arrow">
                                                </span>
                                                <a href="#email" class="name">
                                                <a class="btn red" data-toggle="modal" href="#">
                                                    周伟国答
                                                </a>
                                                </a>
                                            <span class="datetime">
                                                ——2016-6-13
                                            </span>
                                                <br> <br>
                                            <span class="body">
                                                时间片用完了，不管这个线程的优先级有多高都不会在运行，而是进入 就绪队列中，等待下一个时间片的到了，那这个时间片到底要持续多长时间？在《深入理解Linux内核》中的第七章进程调度中，是这样描诉的，Linux采 取单凭经验的方法，即选择尽可能长、同时能保持良好相应时间的一个时间片。这里也没有给出一个具体的时间来，可能会根据不同的CPU 来定，还有就是多CPU 的情况。
                                            </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="note note-success">
                                            <div class="message">
                                                <span class="arrow">
                                                </span>
                                                <a href="#email" class="name">
                                                      <a class="btn green" data-toggle="modal" href="#">
                                                          user问
                                                </a>
                                                </a>
                                            <span class="datetime">
                                                ——2016-6-13
                                            </span>
                                                <br> <br>
                                            <span class="body">
                                                时间片用完了，不管这个线程的优先级有多高都不会在运行，而是进入 就绪队列中，等待下一个时间片的到了?
                                            </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="note note-danger">
                                            <div class="message">
                                                <span class="arrow">
                                                </span>
                                                <a href="#email" class="name">
                                                   <a class="btn red" data-toggle="modal" href="#">
                                                       周伟国 答
                                                </a>
                                                </a>
                                            <span class="datetime">
                                                ——2016-6-13
                                            </span>
                                                <br> <br>
                                            <span class="body">
                                                时间片用完了，不管这个线程的优先级有多高都不会在运行，而是进入 就绪队列中，等待下一个时间片的到了，那这个时间片到底要持续多长时间？在《深入理解Linux内核》中的第七章进程调度中，是这样描诉的，Linux采 取单凭经验的方法，即选择尽可能长、同时能保持良好相应时间的一个时间片。这里也没有给出一个具体的时间来，可能会根据不同的CPU 来定，还有就是多CPU 的情况。
                                            </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="note note-success">
                                            <div class="message">
                                                <span class="arrow">
                                                </span>
                                                <a href="#email" class="name">
                                                    <a class="btn green" data-toggle="modal" href="#">
                                                        user 问
                                                </a>
                                                </a>
                                            <span class="datetime">
                                                ——2016-6-13
                                            </span>
                                                <br> <br>
                                            <span class="body">
                                                时间片用完了，不管这个线程的优先级有多高都不会在运行，而是进入 就绪队列中，等待下一个时间片的到了?
                                            </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="note note-danger">
                                            <div class="message">
                                                <span class="arrow">
                                                </span>
                                                <a href="#email" class="name">
                                                      <a class="btn red" data-toggle="modal" href="#">
                                                          周伟国答
                                                </a>
                                                </a>
                                            <span class="datetime">
                                                ——2016-6-13
                                            </span>
                                                <br> <br>
                                            <span class="body">
                                                时间片用完了，不管这个线程的优先级有多高都不会在运行，而是进入 就绪队列中，等待下一个时间片的到了，那这个时间片到底要持续多长时间？在《深入理解Linux内核》中的第七章进程调度中，是这样描诉的，Linux采 取单凭经验的方法，即选择尽可能长、同时能保持良好相应时间的一个时间片。这里也没有给出一个具体的时间来，可能会根据不同的CPU 来定，还有就是多CPU 的情况。
                                            </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="note note-success">
                                            <div class="message">
                                                <span class="arrow">
                                                </span>
                                                <a href="#email" class="name">

                                                    <a class="btn green" data-toggle="modal" href="#">
                                                        user 问
                                                </a>
                                                </a>
                                            <span class="datetime">
                                                ——2016-6-13
                                            </span>
                                                <br> <br>
                                            <span class="body">
                                                时间片用完了，不管这个线程的优先级有多高都不会在运行，而是进入 就绪队列中，等待下一个时间片的到了?
                                            </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="note note-danger">
                                            <div class="message">
                                                <a href="#email" class="name">
                                                    <a class="btn red" data-toggle="modal" href="#">
                                                        周伟国答
                                                    </a>
                                                </a>
                                            <span class="datetime">
                                                ——2016-6-13
                                            </span>
                                                <br> <br>
                                            <span class="body">
                                                时间片用完了，不管这个线程的优先级有多高都不会在运行，而是进入 就绪队列中，等待下一个时间片的到了，那这个时间片到底要持续多长时间？在《深入理解Linux内核》中的第七章进程调度中，是这样描诉的，Linux采 取单凭经验的方法，即选择尽可能长、同时能保持良好相应时间的一个时间片。这里也没有给出一个具体的时间来，可能会根据不同的CPU 来定，还有就是多CPU 的情况。
                                            </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="tab_2_2">
                                        <div class="row">
                                            <div class="note note-success">
                                                <div class="message">
                                                <span class="arrow">
                                                </span>
                                                    <a href="#email" class="name">
                                                        <a class="btn green" data-toggle="modal" href="#">
                                                            user  问
                                                        </a>
                                                    </a>
                                            <span class="datetime">
                                                ——2016-6-13
                                            </span>
                                                    <br> <br>
                                            <span class="body">
                                                时间片用完了，不管这个线程的优先级有多高都不会在运行，而是进入 就绪队列中，等待下一个时间片的到了?
                                            </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="note note-success">
                                                <div class="message">
                                                <span class="arrow">
                                                </span>
                                                    <a href="#email" class="name">
                                                         <a class="btn green" data-toggle="modal" href="#">
                                                             admin问
                                                    </a>
                                                    </a>
                                            <span class="datetime">
                                                ——2016-6-13
                                            </span>
                                                    <br> <br>
                                            <span class="body">
                                                时间片用完了，不管这个线程的优先级有多高都不会在运行，而是进入 就绪队列中，等待下一个时间片的到了?
                                            </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="note note-success">
                                                <div class="message">
                                                <span class="arrow">
                                                </span>
                                                    <a href="#email" class="name">
                                                       <a class="btn green" data-toggle="modal" href="#">
                                                           admin  问
                                                    </a>
                                                    </a>
                                            <span class="datetime">
                                                ——2016-6-13
                                            </span>
                                                    <br> <br>
                                            <span class="body">
                                                时间片用完了，不管这个线程的优先级有多高都不会在运行，而是进入 就绪队列中，等待下一个时间片的到了?
                                            </span>
                                                </div>
                                            </div>
                                        </div>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="deleteRowConfirm" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">向专家提问</h4>
            </div>
            <div class="portlet box">
                <div class="portlet-body form">
                                            <!-- BEGIN FORM-->
                    <form action="#" class="form-horizontal" id="addForm">
                        <div class="form-body">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="control-label col-sm-3">问题
                                            <span class="required" aria-required="true">* </span>
                                        </label>
                                        <div class="col-sm-6">
                                            <input type="text" class="form-control input-medium" id="loginname"
                                                   name="loginname"maxlength="32" placeholder="请你要提的问题"
                                                   value=""/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="control-label col-sm-3">描述
                                            <span class="required" aria-required="true">* </span>
                                        </label>
                                        <div class="col-sm-6">
                                              <textarea style="   width: 311px; height: 166px;" id="maxlength_textarea" class="form-control" maxlength="225" rows="2" placeholder="This textarea has a limit of 225 chars.">
                                            </textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            <div class="modal-footer">
                <button type="button" class="btn default" data-dismiss="modal">取消</button>
                <button type="button" class="btn blue" id="deleteRow">向专家提问</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<jsp:include page="../includes/script.jsp"/>
</body>
</html>
