<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2016/6/8
  Time: 20:37
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <jsp:include page="../includes/style.jsp"/>
    <link href="<c:url value="/assets/frontend/pages/css/portfolio.css"/>" rel="stylesheet" type="text/css">
    <link href="<c:url value="/assets/frontend/pages/css/gallery.css"/>" rel="stylesheet" type="text/css">
    <title>搜索</title>
    <script type="text/javascript" src="<c:url value="/js/user/upload.js"/>"></script>
</head>
<jsp:include page="../includes/header.jsp"/>
<body style="background: #ffffff;font-size: 20px;">
<div class="modal fade" id="modalDialog" tabindex="-1" role="basic" aria-hidden="true" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog" style="width: 920px;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title" id="dialogTitle"></h4>
            </div>
            <div class="modal-body">
                <div class="portlet-body">
                    <iframe id="userEditIframe" style="border: none;width:860px;height:470px;" frameBorder="0"></iframe>
                </div>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<div class="main">
    <div class="container">
        <%--<div class="col-md-6">--%>
            <%--<!-- BEGIN Portlet PORTLET-->--%>
            <%--<div class="portlet box red">--%>
                <%--<div class="portlet-title">--%>
                    <%--<div class="caption">--%>
                        <%--<i class="fa fa-gift"></i>Advance Form--%>
                    <%--</div>--%>
                    <%--<div class="tools">--%>
                        <%--<a class="collapse" href="#">--%>
                        <%--</a>--%>
                        <%--<a class="config" data-toggle="modal" href="#portlet-config">--%>
                        <%--</a>--%>
                        <%--<a data-url="portlet_ajax_content_2.html" data-load="true" class="reload" href="#">--%>
                        <%--</a>--%>
                        <%--<a class="remove" href="#">--%>
                        <%--</a>--%>
                    <%--</div>--%>
                <%--</div>--%>
                <%--<div class="portlet-body form portlet-empty" style=""><form role="form">--%>
                    <%--<div class="form-body">--%>
                        <%--<div class="form-group">--%>
                            <%--<label for="exampleInputEmail1">Text</label>--%>
                            <%--<input type="email" placeholder="Enter text" id="exampleInputEmail1" class="form-control">--%>
			<%--<span class="help-block">--%>
			<%--A block of help text. </span>--%>
                        <%--</div>--%>
                        <%--<div class="form-group">--%>
                            <%--<label>Email Address</label>--%>
                            <%--<div class="input-group">--%>
				<%--<span class="input-group-addon">--%>
				<%--<i class="fa fa-envelope"></i>--%>
				<%--</span>--%>
                                <%--<input type="text" placeholder="Email Address" class="form-control">--%>
                            <%--</div>--%>
                        <%--</div>--%>

                    <%--</div>--%>
                    <%--<div class="form-actions">--%>
                        <%--<button class="btn blue" type="submit">Submit</button>--%>
                        <%--<button class="btn default" type="button">Cancel</button>--%>
                    <%--</div>--%>
                <%--</form>--%>
                    <%--<script>--%>
                        <%--jQuery(document).ready(function() {--%>
                            <%--Metronic.initAjax();--%>
                        <%--});--%>
                    <%--</script></div>--%>
            <%--</div>--%>
            <%--<!-- END Portlet PORTLET-->--%>
        <%--</div>--%>

<div class="portlet-body">
    <div class="table-responsive">
        <table class="table table-bordered table-hover">
            <thead>
            <tr>
                <th>

                    文件名
                </th>
                <th>
                    类型
                </th>
                <th>
                    关键字
                </th>
                <th>
                    查看
                </th>
                <th>
                    操作
                </th>
            </tr>

            </thead>
            <tbody>

            <a name="file">
                <c:forEach items="${f}" var="f">
                    <tr>

                        <td>

                            <img src="<c:url value="${f.filePath}"/>" style="weight:30px;height: 30px">
                                ${f.fileName}
                        </td>
                        <td class="success">
                                ${f.fileType}
                        </td>
                        <td class="warning">
                                ${f.fileKeyword}
                        </td>
                        <td class="success">
                            <a href="#" onclick="javascript:getInfo('${f.fileId}')">查看详细</a>
                            <input type="hidden" value="${f.fileId}" id="ffileId">
                            <input type="hidden" value="${f.fileAbstrat}" id="ffileAbstrat">
                            <input type="hidden" value="${f.fileUrl}" id="ffileUrl">
                            <input type="hidden" value="${f.filePath}" id="ffilePath">
                            <input type="hidden" value="${f.fileKeyword}" id="fpfileKeyword">
                            <input type="hidden" value="${f.fileType}" id="fpfileType">
                            <input type="hidden" value="${f.fileName}" id="fpfileName">
                        </td>
                        <td>
                            <button type="button" class="btn btn-primary" onclick="Collection.addCollect3()">收藏</button>
                            <a href="###" onclick="javascript:filePreview('${f.fileUrl}')"  class="btn btn-success">预览</a>
                                <%--<button type="button" class="btn btn-success">预览</button>--%>
                            <a href="###" onclick="javascript:fileDownload('${f.fileUrl}');"  class="btn btn-info"> 下载</a>
                        </td>
                    </tr>
                </c:forEach>
            </a>




            </tbody>
        </table>
    </div>
</div>
        </div>
    </div>

<jsp:include page="../includes/script.jsp"/>

</body>
</html>