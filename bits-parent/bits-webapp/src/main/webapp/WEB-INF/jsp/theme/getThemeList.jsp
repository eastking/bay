<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>主题列表</title>


    <jsp:include page="../includes/style.jsp"/>
    <link href="<c:url value="/assets/frontend/pages/css/portfolio.css"/>" rel="stylesheet" type="text/css">
    <link href="<c:url value="/assets/frontend/pages/css/gallery.css"/>" rel="stylesheet" type="text/css">
    <link herf="<c:url value="/css/theme/theme.css"/>" rel="stylesheet" type="text/css">
    <link href="<c:url value="/assets/global/css/components.css"/>" rel="stylesheet" type="text/css"/>

<script type="text/css">
    .content-page a {
        font-weight: 600;
        color: #ffffff;
        text-decoration: none;
    }
    .content-page a:hover {
        color: #ffffff;
        text-decoration: none;
    }
    .owl-buttons {
        position: relative !important;
        margin: 10px auto;
        top: 0px !important;
        right: 0px !important;
        left: 0px !important;
        text-align: center;
    }
    strong{
        text-align: center;
    }
    .topIcon {
        position:absolute;
        top:0px;
        right:0px;
    }
    input.计算机科学与工程 { background-color: #996699 !important;
        background-image: none !important;
        border-color: #4b8df8 !important;
        color: white !important;}
    input.基础科学 { background-color: #303030 !important;
        background-image: none !important;
        border-color: #4b8df8 !important;
        color: white !important;}
    input.机电工程 { background-color: #FF6600 !important;
        background-image: none !important;
        border-color: #4b8df8 !important;
        color: white !important;}
</script>


</head>
<body>
<jsp:include page="../includes/header.jsp"/>
<input type="hidden" value="${User.userId}" id="userId">
<div class="modal fade" id="modalDialog" tabindex="-1" role="basic" aria-hidden="true" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog" style="width: 900px;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title" id="dialogTitle"></h4>
            </div>
            <div class="modal-body">
                <div class="portlet-body">
                    <iframe id="userEditIframe" style="border: none;width:858px;height:350px;" frameBorder="0"></iframe>
                </div>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<div class="main">
    <div class="container">
        <ul class="breadcrumb">
            <li><a href="javascript:void(0)" onclick="toHome()">首页</a></li>
            <li class="active">组织的知识库</li>
        </ul>
        <!-- CONTENT -->
        <div class="row margin-bottom-40">
            <!-- BEGIN CONTENT -->
            <div class="col-md-12 col-sm-12">
                <div class="row margin-top-10 margin-bottom-10">
                    <div class="col-md-8">
                        <h1>组织的知识库</h1>
                    </div>
                    <div class="col-md-4">
                        <select class="form-control select2" id="fastSearch">
                            <option></option>
                            <c:forEach items="${cacheThemes}" var="theme">
                                <option value="${theme.id}">${theme.themeName}</option>
                            </c:forEach>
                        </select>
                    </div>
                </div>
                <div class="content-page">
                    <div class="portlet">
                    <div class="portlet-title">
                        <div class="row">
                            <div class="col-md-6">
                                <!-- BEGIN SAMPLE TABLE PORTLET-->
                                <div class="portlet">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-bell-o"></i>热门专业
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                                <ul class="feeds">
                                                    <c:forEach items="${TopTheme}" var="theme">
                                                        <li >
                                                            <a href="javascript:;" onclick="toThemeExperts('${theme.id}')">
                                                                <div class="col1">
                                                                    <div class="cont">
                                                                        <div class="cont-col1">
                                                                            <div class="label label-sm label-info">
                                                                                <i class="fa  fa-star"></i>
                                                                            </div>
                                                                        </div>
                                                                        <div class="cont-col2">
                                                                            <div class="desc">
                                                                                    ${theme.themeName}
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col2">
                                                                    <div class="date">
                                                                        just now
                                                                    </div>
                                                                </div>
                                                            </a>
                                                        </li>
                                                    </c:forEach>
                                                </ul>
                                    </div>
                                </div>
                                <!-- END SAMPLE TABLE PO RTLET-->
                            </div>
                            <div class="col-md-6">
                                <!-- BEGIN SAMPLE TABLE PORTLET-->
                                <div class="portlet">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-bell-o"></i>最新知识
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <ul class="feeds">
                                            <c:forEach items="${NewKnowledge}" var="New">
                                                <li >
                                                    <a href="javascript:;" onclick="ThemePage.togetinfo('${New.fileId}')">
                                                        <div class="col1">
                                                            <div class="cont">
                                                                <div class="cont-col1">
                                                                    <div class="label label-sm label-danger">
                                                                        <i class="fa fa-tag"></i>
                                                                    </div>
                                                                </div>
                                                                <div class="cont-col2">
                                                                    <div class="desc">
                                                                            ${New.fileName}
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col2">
                                                            <div class="date">
                                                                ${New.fileTime}
                                                            </div>
                                                        </div>
                                                    </a>
                                                </li>
                                            </c:forEach>
                                        </ul>
                                    </div>
                                </div>
                                <!-- END SAMPLE TABLE PORTLET-->
                            </div>
                        </div>
                        </div>
                    </div>

                    <div class="portlet">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-gift"></i>知识领域你知道
                            </div>
                            <div class="tools">
                                <a href="javascript:;" class="collapse">
                                </a>
                            </div>
                        </div>
                        <div class="portlet-body" style="height: 350px" id="dataKnow">
                        </div>
                    </div>
                    <!--数据你知道-->
                    <!--所有主题-->
                    <div class="portlet">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-gift"></i>所有领域
                            </div>
                            <div class="tools">
                                <a href="javascript:;" class="collapse">
                                </a>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <div class="filter-v1">
                                <ul class="mix-filter">
                                    <li data-filter="all" class="filter active">所有主题</li>
                                    <c:forEach items="${parentTheme}" var="parentTheme">
                                        <li data-filter="${parentTheme.themeName}"
                                            class="filter">${parentTheme.themeName}</li>
                                    </c:forEach>
                                </ul>
                                <div class="row mix-grid thumbnails">
                                    <c:forEach items="${parentTheme}" var="parentTheme">
                                        <c:forEach items="${parentTheme.childThemeTree}" var="childTheme"
                                                   varStatus="iter">
                                            <div class="col-md-3 col-sm-4 mix mix_all ${parentTheme.themeName} gallery-item"
                                                 style="display: block; opacity: 1; ">
                                                <a title="${childTheme.themeName}">

                                                        <div class="visual">
                                                            <img src="<c:url value="${childTheme.img}"/>" id="ContentShow" style="width: 220px;height: 100px; ">
                                                        </div>

                                                    <input type="submit" class="${parentTheme.themeName}" value="${childTheme.themeName}"  onclick="toThemeExperts('${childTheme.id}')" style="width: 220px;">
                                                    <div class="zoomix" style="width: 220px;height: 100px; ">
                                                        <i class="fa fa-search" style="cursor: pointer"
                                                           onclick="toThemeExperts('${childTheme.id}')"></i>
                                                    </div>
                                                </a>
                                            </div>
                                        </c:forEach>
                                    </c:forEach>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--所有主题-->
                </div>
            </div>
            <!-- END CONTENT -->
        </div>
        <!-- BEGIN SIDEBAR & CONTENT -->
    </div>
</div>



<jsp:include page="../includes/script.jsp"/>

</body>
<script src="<c:url value="/assets/global/plugins/jquery-mixitup/jquery.mixitup.min.js"/>" type="text/javascript"></script>
<script src="<c:url value="/js/plugins/echarts-2.2.1/build/dist/echarts-all.js"/>"></script>
<script src="<c:url value="/js/frontend/ThemePage.js"/>" type="text/javascript"></script>

<script type="text/javascript">
    $(document).ready(function () {
        ThemePage.init();
        ThemePage.initBlockColor();
        ThemePage.initTopIcon();
        ThemePage.initChart();
        ThemePage.initSelect2();
    });
</script>


</html>
