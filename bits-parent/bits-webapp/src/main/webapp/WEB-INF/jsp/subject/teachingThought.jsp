<%--
  Created by IntelliJ IDEA.
  User: mafuyu
  Date: 2016/6/4
  Time: 19:52
  To change this template use File | Settings | File Templates.
--%>
<%--
  Created by IntelliJ IDEA.
  User: mafuyu
  Date: 2016/5/19
  Time: 18:34
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<html>
<head>
    <title>教育思想</title>
    <jsp:include page="../includes/style.jsp"/>
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
    <link href="<c:url value="/assets/global/css/components.css"/>" rel="stylesheet" type="text/css"/>
    <link href="<c:url value="/assets/frontend/pages/css/portfolio.css"/>" rel="stylesheet" type="text/css">
    <link href="<c:url value="/assets/frontend/pages/css/gallery.css"/>" rel="stylesheet" type="text/css">
    <link herf="<c:url value="/css/theme/theme.css"/>" rel="stylesheet" type="text/css">

    <script type="text/css">
        .content-page a {
            font-weight: 600;
            color: #ffffff;
            text-decoration: none;
        }
        .content-page a:hover {
            color: #ffffff;
            text-decoration: none;
        }
        .owl-buttons {
            position: relative !important;
            margin: 10px auto;
            top: 0px !important;
            right: 0px !important;
            left: 0px !important;
            text-align: center;
        }
        strong{
            text-align: center;
        }
        .topIcon {
            position:absolute;
            top:0px;
            right:0px;
        }
        input.计算机科学与工程 { background-color: #996699 !important;
            background-image: none !important;
            border-color: #4b8df8 !important;
            color: white !important;}
        input.基础科学 { background-color: #303030 !important;
            background-image: none !important;
            border-color: #4b8df8 !important;
            color: white !important;}
        input.机电工程 { background-color: #FF6600 !important;
            background-image: none !important;
            border-color: #4b8df8 !important;
            color: white !important;}
    </script>

</head>
<body>
<jsp:include page="../includes/header.jsp"/>
<div class="main">
    <div class="container">
        <ul class="breadcrumb">
                <li><a href="index.html">首页</a></li>
                <li><a href="javascript:void(0)" onclick="">主题</a></li>
                <li class="active">教育思想</li>
            </ul>

        <div class="container">
                <ul class="nav nav-tabs">
                    <li  class="active">
                        <a href="#tab_1_1" data-toggle="tab">最新知识</a>
                    </li>
                    <li>
                        <a href="#tab_1_2" data-toggle="tab">最热知识</a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane fade active in" id="tab_1_1">
                        <div class="row">
                            <div class="col-md-12 news-page">
                            <div class="row">
                                <div class="col-md-5">
                                    <div id="myCarouse2" class="carousel image-carousel slide">
                                        <div class="carousel-inner">
                                            <div class="active item">
                                                <img src="<c:url value="/assets/admin/pages/media/gallery/image5.jpg"/>" class="img-responsive" alt="">
                                                <div class="carousel-caption">
                                                    <h4>
                                                        <a href="">
                                                            First Thumbnail label </a>
                                                    </h4>
                                                    <p>
                                                        Cras justo odio, dapibus ac facilisis in, egestas eget quam.
                                                    </p>
                                                </div>
                                            </div>
                                            <div class="item">
                                                <img src="<c:url value="/assets/admin/pages/media/gallery/image2.jpg"/>" class="img-responsive" alt="">
                                                <div class="carousel-caption">
                                                    <h4>
                                                        <a href="">
                                                            Second Thumbnail label </a>
                                                    </h4>
                                                    <p>
                                                        Cras justo odio, dapibus ac facilisis in, egestas eget quam.
                                                    </p>
                                                </div>
                                            </div>
                                            <div class="item">
                                                <img src="<c:url value="/assets/admin/pages/media/gallery/image1.jpg"/>" class="img-responsive" alt="">
                                                <div class="carousel-caption">
                                                    <h4>
                                                        <a href="">
                                                            Third Thumbnail label </a>
                                                    </h4>
                                                    <p>
                                                        Cras justo odio, dapibus ac facilisis in, egestas eget quam.
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Carousel nav -->
                                        <a class="carousel-control left" href="#myCarousel" data-slide="prev">
                                            <i class="m-icon-big-swapleft m-icon-white"></i>
                                        </a>
                                        <a class="carousel-control right" href="#myCarousel" data-slide="next">
                                            <i class="m-icon-big-swapright m-icon-white"></i>
                                        </a>
                                    </div>
                                    <div class="news-blocks">
                                        <h3>
                                            <a href="">
                                                Google Glass Technology.. </a>
                                        </h3>
                                        <div class="news-block-tags">
                                            <strong>CA, USA</strong>
                                            <em>3 hours ago</em>
                                        </div>
                                        <p>
                                            <img class="news-block-img pull-right" src="<c:url value="/assets/admin/pages/media/gallery/image1.jpg"/>" alt="">At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident
                                        </p>
                                        <a href="" class="news-block-btn">
                                            Read more <i class="m-icon-swapright m-icon-black"></i>
                                        </a>
                                    </div>
                                </div>
                                <!--end col-md-5-->
                                <div class="col-md-3">
                                    <div class="news-blocks">
                                        <h3>
                                            <a href="">
                                                Vero eos et accusam </a>
                                        </h3>
                                        <div class="news-block-tags">
                                            <strong>CA, USA</strong>
                                            <em>3 hours ago</em>
                                        </div>
                                        <p>
                                            <img class="news-block-img pull-right" src="<c:url value="/assets/admin/pages/media/gallery/image2.jpg"/>" alt="">At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident
                                        </p>
                                        <a href="" class="news-block-btn">
                                            Read more <i class="m-icon-swapright m-icon-black"></i>
                                        </a>
                                    </div>
                                    <div class="news-blocks">
                                        <h3>
                                            <a href="">
                                                Sias excepturi sint occae </a>
                                        </h3>
                                        <div class="news-block-tags">
                                            <strong>Vancouver, Canada</strong>
                                            <em>3 hours ago</em>
                                        </div>
                                        <p>
                                            <img class="news-block-img pull-right" src="<c:url value="/assets/admin/pages/media/gallery/image4.jpg"/>" alt="">At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident
                                        </p>
                                        <a href="" class="news-block-btn">
                                            Read more <i class="m-icon-swapright m-icon-black"></i>
                                        </a>
                                    </div>
                                </div>
                                <!--end col-md-4-->
                                <div class="col-md-3">
                                    <div class="news-blocks">
                                        <h3>
                                            <a href="">
                                                Vero eos et accusam </a>
                                        </h3>
                                        <div class="news-block-tags">
                                            <strong>CA, USA</strong>
                                            <em>3 hours ago</em>
                                        </div>
                                        <p>
                                            <img class="news-block-img pull-right" src="<c:url value="/assets/admin/pages/media/gallery/image2.jpg"/>" alt="">At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident
                                        </p>
                                        <a href="" class="news-block-btn">
                                            Read more <i class="m-icon-swapright m-icon-black"></i>
                                        </a>
                                    </div>
                                    <div class="news-blocks">
                                        <h3>
                                            <a href="">
                                                Sias excepturi sint occae </a>
                                        </h3>
                                        <div class="news-block-tags">
                                            <strong>Vancouver, Canada</strong>
                                            <em>3 hours ago</em>
                                        </div>
                                        <p>
                                            <img class="news-block-img pull-right" src="<c:url value="/assets/admin/pages/media/gallery/image4.jpg"/>" alt="">At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident
                                        </p>
                                        <a href="" class="news-block-btn">
                                            Read more <i class="m-icon-swapright m-icon-black"></i>
                                        </a>
                                    </div>
                                </div>
                                <!--end col-md-3-->
                            </div>
                        </div>
                        </div>
                    </div>


                    <div class="tab-pane fade" id="tab_1_2">
                        <div class="row">
                            <div class="col-md-12 news-page">
                                <div class="row">
                                    <div class="col-md-5">
                                        <div id="myCarousel" class="carousel image-carousel slide">
                                            <div class="carousel-inner">
                                                <div class="active item">
                                                    <img src="<c:url value="/assets/admin/pages/media/gallery/image5.jpg"/>" class="img-responsive" alt="">
                                                    <div class="carousel-caption">
                                                        <h4>
                                                            <a href="">
                                                                First Thumbnail label </a>
                                                        </h4>
                                                        <p>
                                                            Cras justo odio, dapibus ac facilisis in, egestas eget quam.
                                                        </p>
                                                    </div>
                                                </div>
                                                <div class="item">
                                                    <img src="<c:url value="/assets/admin/pages/media/gallery/image2.jpg"/>" class="img-responsive" alt="">
                                                    <div class="carousel-caption">
                                                        <h4>
                                                            <a href="">
                                                                Second Thumbnail label </a>
                                                        </h4>
                                                        <p>
                                                            Cras justo odio, dapibus ac facilisis in, egestas eget quam.
                                                        </p>
                                                    </div>
                                                </div>
                                                <div class="item">
                                                    <img src="<c:url value="/assets/admin/pages/media/gallery/image1.jpg"/>" class="img-responsive" alt="">
                                                    <div class="carousel-caption">
                                                        <h4>
                                                            <a href="">
                                                                Third Thumbnail label </a>
                                                        </h4>
                                                        <p>
                                                            Cras justo odio, dapibus ac facilisis in, egestas eget quam.
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- Carousel nav -->
                                            <a class="carousel-control left" href="#myCarousel" data-slide="prev">
                                                <i class="m-icon-big-swapleft m-icon-white"></i>
                                            </a>
                                            <a class="carousel-control right" href="#myCarousel" data-slide="next">
                                                <i class="m-icon-big-swapright m-icon-white"></i>
                                            </a>
                                        </div>
                                        <div class="news-blocks">
                                            <h3>
                                                <a href="">
                                                    Google Glass Technology.. </a>
                                            </h3>
                                            <div class="news-block-tags">
                                                <strong>CA, USA</strong>
                                                <em>3 hours ago</em>
                                            </div>
                                            <p>
                                                <img class="news-block-img pull-right" src="<c:url value="/assets/admin/pages/media/gallery/image1.jpg"/>" alt="">At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident
                                            </p>
                                            <a href="" class="news-block-btn">
                                                Read more <i class="m-icon-swapright m-icon-black"></i>
                                            </a>
                                        </div>
                                    </div>
                                    <!--end col-md-5-->
                                    <div class="col-md-3">
                                        <div class="news-blocks">
                                            <h3>
                                                <a href="">
                                                    Vero eos et accusam </a>
                                            </h3>
                                            <div class="news-block-tags">
                                                <strong>CA, USA</strong>
                                                <em>3 hours ago</em>
                                            </div>
                                            <p>
                                                <img class="news-block-img pull-right" src="<c:url value="/assets/admin/pages/media/gallery/image2.jpg"/>" alt="">At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident
                                            </p>
                                            <a href="" class="news-block-btn">
                                                Read more <i class="m-icon-swapright m-icon-black"></i>
                                            </a>
                                        </div>
                                        <div class="news-blocks">
                                            <h3>
                                                <a href="">
                                                    Sias excepturi sint occae </a>
                                            </h3>
                                            <div class="news-block-tags">
                                                <strong>Vancouver, Canada</strong>
                                                <em>3 hours ago</em>
                                            </div>
                                            <p>
                                                <img class="news-block-img pull-right" src="<c:url value="/assets/admin/pages/media/gallery/image4.jpg"/>" alt="">At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident
                                            </p>
                                            <a href="" class="news-block-btn">
                                                Read more <i class="m-icon-swapright m-icon-black"></i>
                                            </a>
                                        </div>
                                    </div>
                                    <!--end col-md-4-->
                                    <div class="col-md-3">
                                        <div class="news-blocks">
                                            <h3>
                                                <a href="">
                                                    Vero eos et accusam </a>
                                            </h3>
                                            <div class="news-block-tags">
                                                <strong>CA, USA</strong>
                                                <em>3 hours ago</em>
                                            </div>
                                            <p>
                                                <img class="news-block-img pull-right" src="<c:url value="/assets/admin/pages/media/gallery/image2.jpg"/>" alt="">At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident
                                            </p>
                                            <a href="" class="news-block-btn">
                                                Read more <i class="m-icon-swapright m-icon-black"></i>
                                            </a>
                                        </div>
                                        <div class="news-blocks">
                                            <h3>
                                                <a href="">
                                                    Sias excepturi sint occae </a>
                                            </h3>
                                            <div class="news-block-tags">
                                                <strong>Vancouver, Canada</strong>
                                                <em>3 hours ago</em>
                                            </div>
                                            <p>
                                                <img class="news-block-img pull-right" src="<c:url value="/assets/admin/pages/media/gallery/image4.jpg"/>" alt="">At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident
                                            </p>
                                            <a href="" class="news-block-btn">
                                                Read more <i class="m-icon-swapright m-icon-black"></i>
                                            </a>
                                        </div>
                                    </div>
                                    <!--end col-md-3-->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <div class="container">
            <ul class="nav nav-tabs">
                <li  class="active">
                    <a href="#tab_2_1" data-toggle="tab">所有知识</a>
                </li>
                <li>
                    <a href="#tab_2_2" data-toggle="tab">文档</a>
                </li>
                <li>
                <a href="#tab_2_3" data-toggle="tab">视频</a>
                </li>
                <li>
                    <a href="#tab_2_4" data-toggle="tab">图片</a>
                </li>
                <li>
                    <a href="#tab_2_5" data-toggle="tab">其他</a>
                </li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane fade active in" id="tab_2_1">
                    <div class="row">
                        <div class="portlet-body">
                            <div class="container">
                                <div class="scroller" style="height: 300px;" data-always-visible="1" data-rail-visible="0">
                                    <ul class="feeds">
                                        <li >
                                            <a href="#">
                                                <div class="col1">
                                                    <div class="cont">
                                                        <div class="cont-col1">
                                                            <div class="label label-sm label-success">
                                                                <i class="fa fa-bar-chart-o"></i>
                                                            </div>
                                                        </div>
                                                        <div class="cont-col2">
                                                            <div class="desc">
                                                                这是一个图表.
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col2">
                                                    <div class="date">
                                                        20 mins
                                                    </div>
                                                </div>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <div class="col1">
                                                    <div class="cont">
                                                        <div class="cont-col1">
                                                            <div class="label label-sm label-danger">
                                                                <i class="fa fa-film"></i>
                                                            </div>
                                                        </div>
                                                        <div class="cont-col2">
                                                            <div class="desc">
                                                                这是一个视频
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col2">
                                                    <div class="date">
                                                        24 mins
                                                    </div>
                                                </div>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <div class="col1">
                                                    <div class="cont">
                                                        <div class="cont-col1">
                                                            <div class="label label-sm label-info">
                                                                <i class="fa fa-tasks"></i>
                                                            </div>
                                                        </div>
                                                        <div class="cont-col2">
                                                            <div class="desc">
                                                               这是一个压缩吧包
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col2">
                                                    <div class="date">
                                                        30 mins
                                                    </div>
                                                </div>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <div class="col1">
                                                    <div class="cont">
                                                        <div class="cont-col1">
                                                            <div class="label label-sm label-success">
                                                                <i class="fa fa-file-text"></i>
                                                            </div>
                                                        </div>
                                                        <div class="cont-col2">
                                                            <div class="desc">
                                                                这是一个文档
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col2">
                                                    <div class="date">
                                                        24 mins
                                                    </div>
                                                </div>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <div class="col1">
                                                    <div class="cont">
                                                        <div class="cont-col1">
                                                            <div class="label label-sm label-default">
                                                                <i class="fa fa-music"></i>
                                                            </div>
                                                        </div>
                                                        <div class="cont-col2">
                                                            <div class="desc">
                                                                这是一个音频
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col2">
                                                    <div class="date">
                                                        2 hours
                                                    </div>
                                                </div>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <div class="col1">
                                                    <div class="cont">
                                                        <div class="cont-col1">
                                                            <div class="label label-sm label-default">
                                                                <i class="fa fa-picture-o"></i>
                                                            </div>
                                                        </div>
                                                        <div class="cont-col2">
                                                            <div class="desc">
                                                                这是一个图片.
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col2">
                                                    <div class="date">
                                                        20 mins
                                                    </div>
                                                </div>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="tab_2_2">
                    <div class="row">
                        <div class="portlet-body">
                                <div class="container">
                                    <div class="scroller" style="height: 300px;" data-always-visible="1" data-rail-visible="0">
                                        <ul class="feeds">
                                            <li>
                                                <a href="#">
                                                    <div class="col1">
                                                        <div class="cont">
                                                            <div class="cont-col1">
                                                                <div class="label label-sm label-success">
                                                                    <i class="fa fa-file-text"></i>
                                                                </div>
                                                            </div>
                                                            <div class="cont-col2">
                                                                <div class="desc">
                                                                    这是一个文档
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col2">
                                                        <div class="date">
                                                            24 mins
                                                        </div>
                                                    </div>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    <div class="col1">
                                                        <div class="cont">
                                                            <div class="cont-col1">
                                                                <div class="label label-sm label-success">
                                                                    <i class="fa fa-file-text"></i>
                                                                </div>
                                                            </div>
                                                            <div class="cont-col2">
                                                                <div class="desc">
                                                                    这是一个文档
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col2">
                                                        <div class="date">
                                                            24 mins
                                                        </div>
                                                    </div>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    <div class="col1">
                                                        <div class="cont">
                                                            <div class="cont-col1">
                                                                <div class="label label-sm label-success">
                                                                    <i class="fa fa-file-text"></i>
                                                                </div>
                                                            </div>
                                                            <div class="cont-col2">
                                                                <div class="desc">
                                                                    这是一个文档
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col2">
                                                        <div class="date">
                                                            24 mins
                                                        </div>
                                                    </div>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    <div class="col1">
                                                        <div class="cont">
                                                            <div class="cont-col1">
                                                                <div class="label label-sm label-success">
                                                                    <i class="fa fa-file-text"></i>
                                                                </div>
                                                            </div>
                                                            <div class="cont-col2">
                                                                <div class="desc">
                                                                    这是一个文档
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col2">
                                                        <div class="date">
                                                            24 mins
                                                        </div>
                                                    </div>
                                                </a>
                                            </li>
                                            <li>
                                            <a href="#">
                                                <div class="col1">
                                                    <div class="cont">
                                                        <div class="cont-col1">
                                                            <div class="label label-sm label-success">
                                                                <i class="fa fa-file-text"></i>
                                                            </div>
                                                        </div>
                                                        <div class="cont-col2">
                                                            <div class="desc">
                                                                这是一个文档
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col2">
                                                    <div class="date">
                                                        24 mins
                                                    </div>
                                                </div>
                                            </a>
                                        </li>
                                            <li>
                                            <a href="#">
                                                <div class="col1">
                                                    <div class="cont">
                                                        <div class="cont-col1">
                                                            <div class="label label-sm label-success">
                                                                <i class="fa fa-file-text"></i>
                                                            </div>
                                                        </div>
                                                        <div class="cont-col2">
                                                            <div class="desc">
                                                                这是一个文档
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col2">
                                                    <div class="date">
                                                        24 mins
                                                    </div>
                                                </div>
                                            </a>
                                        </li>
                                            <li>
                                            <a href="#">
                                                <div class="col1">
                                                    <div class="cont">
                                                        <div class="cont-col1">
                                                            <div class="label label-sm label-success">
                                                                <i class="fa fa-file-text"></i>
                                                            </div>
                                                        </div>
                                                        <div class="cont-col2">
                                                            <div class="desc">
                                                                这是一个文档
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col2">
                                                    <div class="date">
                                                        24 mins
                                                    </div>
                                                </div>
                                            </a>
                                        </li>
                                            <li>
                                                <a href="#">
                                                    <div class="col1">
                                                        <div class="cont">
                                                            <div class="cont-col1">
                                                                <div class="label label-sm label-success">
                                                                    <i class="fa fa-file-text"></i>
                                                                </div>
                                                            </div>
                                                            <div class="cont-col2">
                                                                <div class="desc">
                                                                    这是一个文档
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col2">
                                                        <div class="date">
                                                            24 mins
                                                        </div>
                                                    </div>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="tab_2_3">
                    <div class="row recent-work margin-bottom-40">
                        <div class="col-md-3">
                            <div class="row">
                                <div class="recent-work-item">
                                    <em>
                                        <a class="recent-work-description" href="#" onclick="showVideo()">
                                            <img alt="" src="<c:url value="/assets/frontend/pages/img/works/img1.jpg"/>" class="img-responsive">
                                        </a>
                                    </em>
                                    <a class="recent-work-description" href="#" onclick="showVideo()">
                                        <strong>视频</strong>
                                        <b>Agenda corp.</b>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="row">
                                <div class="recent-work-item">
                                    <em>
                                        <a class="recent-work-description" href="#" onclick="showVideo()">
                                            <img alt="" src="<c:url value="/assets/frontend/pages/img/works/img2.jpg"/>" class="img-responsive">
                                        </a>
                                    </em>
                                    <a class="recent-work-description" href="#" onclick="showVideo()">
                                        <strong>视频</strong>
                                        <b>Agenda corp.</b>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="tab_2_4">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row recent-work margin-bottom-40">
                                    <div class="col-md-3">
                                        <div class="recent-work-item">
                                            <em>
                                                <a data-rel="fancybox-button" href="<c:url value="/assets/frontend/pages/img/works/img1.jpg"/>" class="fancybox-button">
                                                        <img alt="" src="<c:url value="/assets/frontend/pages/img/works/img1.jpg"/>" class="img-responsive">
                                                </a>
                                            </em>

                                            <a class="recent-work-description fancybox-button"data-rel="fancybox-button" href="<c:url value="/assets/frontend/pages/img/works/img1.jpg"/>">
                                                <strong>图片一</strong>
                                                <b>来自：大大</b>
                                            </a>
                                        </div>

                                    </div>
                                    <div class="col-md-3">
                                        <div class="recent-work-item">
                                            <a data-rel="fancybox-button" href="<c:url value="/assets/frontend/pages/img/works/img1.jpg"/>" class="fancybox-button">
                                                <img alt="" src="<c:url value="/assets/frontend/pages/img/works/img1.jpg"/>" class="img-responsive">
                                            </a>
                                            <a class="recent-work-description" href="#">
                                                <strong>图片一</strong>
                                                <b>来自：大大</b>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="recent-work-item">
                                            <a data-rel="fancybox-button" href="<c:url value="/assets/frontend/pages/img/works/img1.jpg"/>" class="fancybox-button">
                                                <img alt="" src="<c:url value="/assets/frontend/pages/img/works/img1.jpg"/>" class="img-responsive">
                                            </a>
                                            <a class="recent-work-description" href="#">
                                                <strong>图片一</strong>
                                                <b>来自：大大</b>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="recent-work-item">
                                            <a data-rel="fancybox-button" href="<c:url value="/assets/frontend/pages/img/works/img1.jpg"/>" class="fancybox-button">
                                                <img alt="" src="<c:url value="/assets/frontend/pages/img/works/img1.jpg"/>" class="img-responsive">
                                            </a>
                                            <a class="recent-work-description" href="#">
                                                <strong>图片一</strong>
                                                <b>来自：大大</b>
                                            </a>
                                        </div>
                                    </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="tab_2_5">
                    <div class="row">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modalDialog" tabindex="-1" role="basic" aria-hidden="true" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog" style="width: 900px;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title" id="dialogTitle"></h4>
            </div>
            <div class="modal-body">
                <div class="portlet-body">
                    <iframe id="userEditIframe" style="border: none;width:858px;height:550px;" frameBorder="0"></iframe>
                </div>
                <div class="content">
                <button type="button" class="btn green">发送至常用文档</button>
                <button type="button" class="btn green">取消</button>
                </div>
            </div>

        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
    <jsp:include page="../includes/script.jsp"/>
</body>
</html>