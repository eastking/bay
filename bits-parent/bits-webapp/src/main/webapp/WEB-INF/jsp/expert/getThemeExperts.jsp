<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>主题信息</title>

    <jsp:include page="../includes/style.jsp"/>
    <link href="<c:url value="/assets/frontend/pages/css/portfolio.css"/>" rel="stylesheet" type="text/css">
    <link href="<c:url value="/assets/frontend/pages/css/gallery.css"/>" rel="stylesheet" type="text/css">
    <style>
        .thumbnail > img {
            width: 100%;
        }

        .thumbnail small {
            line-height: 1.4;
            height: 30px;
        }

    </style>
    <script type="text/javascript">
        function on_search(){
            $("#mySearch").unbind("click");
            var div = $(".search-box");
            div.css("display") === "none" && div.show() || div.hide();
        }
    </script>
</head>
<body>
<jsp:include page="../includes/header.jsp"/>
<input type="hidden" value="${User.userId}" id="userId">
<div class="modal fade" id="modalDialog" tabindex="-1" role="basic" aria-hidden="true" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog" style="width: 920px;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title" id="dialogTitle"></h4>
            </div>
            <div class="modal-body">
                <div class="portlet-body">
                    <iframe id="userEditIframe" style="border: none;width:860px;height:470px;" frameBorder="0"></iframe>
                    <center>
                        <%--<button type="button" class="btn btn-success">预览</button>--%>
                        <a href="###" onclick="javascript:filePreview('${file.fileUrl}','${file.fileId}')"  class="btn btn-success">预览</a>
                        <%--<button onclick="fileDownload('${p.filePath}');" class="btn btn-info">下载</button>--%>
                        <a href="###" onclick="javascript:fileDownload('${file.fileUrl}','${file.fileId}');"  class="btn btn-info"> 下载</a>
                    </center>

                </div>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<div class="main">
    <div class="container">
        <ul class="breadcrumb">
            <li><a href="#" onclick="toHome()">首页</a></li>
            <li class="active">领域详情</li>
        </ul>
        <div class="col-md-8">
            <div class="form-group pull-right">
                <label class="control-label col-md-3"
                       style="padding: 0 0; font-size: 1.2em">关注&nbsp;:</label>

                <div class="col-md-9" style="height: 2em;">
                    <input type="checkbox" class="make-switch"
                           data-on-color="success" data-off-color="danger" data-on-text="开"
                           data-off-text="关" id="focusThisTheme"
                    <c:if test="${User.userId == null}">
                        <c:out value="disabled=disabled"></c:out>
                    </c:if>
                    <c:if test="${!isFocus}">
                        <c:out value="checked='checked'"></c:out>
                    </c:if>
                    >
                    <input type="hidden" value="${currentTheme.id}" id="themeId">
                </div>
            </div>
        </div>
        <div class="col-md-12 blog-page">
            <div class="row">
                <div class="col-md-9 article-block">
                    <h1 style="display: inline-block">${currentTheme.themeName}</h1>
                    <div class="blog-tag-data">

                        <img src="<c:url value="${currentTheme.img}"/>" class="img-responsive" alt="">
                        <div class="row">
                            <div class="col-md-6">
                                <ul class="list-inline blog-tags">
                                    <li>
                                        <a href="#">
                                            <i class="fa fa-tags"></i>
                                            简介
                                        </a>


                                    </li>
                                </ul>
                            </div>
                            <%--<div class="col-md-6 blog-tag-data-inner">--%>
                                <%--<ul class="list-inline">--%>
                                    <%--<li>--%>
                                        <%--<i class="fa fa-calendar"></i>--%>

                                        <%--April 16, 2013--%>
                                    <%--</li>--%>
                                    <%--<li>--%>
                                        <%--<i class="fa fa-comments"></i>--%>

                                        <%--38 Comments--%>
                                    <%--</li>--%>
                                <%--</ul>--%>
                            <%--</div>--%>
                        </div>
                    </div>
                    <!--end news-tag-data-->
                    <div>

                        <blockquote class="hero">
                            <p>
                                &nbsp; &nbsp; ${currentTheme.keyWord}

                        </blockquote>

                    </div>
                    <hr>





                    <div class="portlet box">
                        <form action="#" id="form_sample_1" class="form-horizontal" method="post" enctype="multipart/form-data" >
                            <input id="fsearch" type="text" name="fsearch"  style="margin-bottom: 0" class="input-small0" style="height:28px">
                            <a class="btn btn-small blue" onclick="fileSearch()">
                                <i class="fa fa-search faa-shake animated-hover"></i>查询</a>
                        </form>
                        <div class="portlet-body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover">
                                    <thead>
                                    <tr>
                                        <th>

                                            文件名
                                        </th>
                                        <th>
                                            类型
                                        </th>
                                        <th>
                                            关键字
                                        </th>
                                        <th>
                                            查看
                                        </th>
                                        <th>
                                            操作
                                        </th>
                                    </tr>

                                    </thead>
                                    <tbody>
                                    <a name="picture">
                                        <c:forEach items="${pictureList}" var="p">

                                            <tr>

                                                <td>
                                                    <img src="<c:url value="${p.filePath}"/>" style="weight:30px;height: 30px;"/>${p.fileName}
                                                </td>
                                                <td class="success">
                                                        ${p.fileType}
                                                </td>
                                                <td class="warning">
                                                        ${p.fileKeyword}
                                                </td>
                                                <td class="success">
                                                    <a href="#" onclick="javascript:getInfo('${p.fileId}')">查看详细</a>
                                                </td>
                                                <td>
                                                    <button type="button" class="btn btn-primary" onclick="Collection.addCollect('${p.fileId}')">收藏</button>
                                                        <%--<button type="button" class="btn btn-success">预览</button>--%>
                                                    <a href="###" onclick="javascript:filePreview('${p.fileUrl}','${p.fileId}')"  class="btn btn-success">预览</a>
                                                        <%--<button onclick="fileDownload('${p.filePath}');" class="btn btn-info">下载</button>--%>
                                                    <a href="###" onclick="javascript:fileDownload('${p.fileUrl}','${p.fileId}');"  class="btn btn-info"> 下载</a>
                                                </td>
                                            </tr>
                                        </c:forEach>
                                    </a>

                                    <a name="music">
                                        <c:forEach items="${musicList}" var="m">
                                            <tr>

                                                <td>
                                                    <img src="<c:url value="${m.filePath}"/>"  style="weight:30px;height: 30px;"/>
                                                        ${m.fileName}
                                                </td>
                                                <td class="success">
                                                        ${m.fileType}
                                                </td>
                                                <td class="warning">
                                                        ${m.fileKeyword}
                                                </td>
                                                <td class="success">
                                                    <a href="#" onclick="javascript:getInfo('${m.fileId}')">查看详细</a>
                                                </td>
                                                <td>
                                                    <button type="button" class="btn btn-primary" onclick="Collection.addCollect('${m.fileId}')">收藏</button>
                                                    <a href="###" onclick="javascript:filePreview('${m.fileUrl}','${m.fileId}')"  class="btn btn-success">预览</a>
                                                    <a href="###" onclick="javascript:fileDownload('${m.fileUrl}','${m.fileId}');"  class="btn btn-info"> 下载</a>
                                                </td>
                                            </tr>
                                        </c:forEach>
                                    </a>
                                    <a name="video">
                                        <c:forEach items="${videoList}" var="v">
                                            <tr>
                                                <td>
                                                    <img src="<c:url value="${v.filePath}"/>"  style="weight:30px;height: 30px;"/>${v.fileName}
                                                </td>
                                                <td class="success">
                                                        ${v.fileType}
                                                </td>
                                                <td class="warning">
                                                        ${v.fileKeyword}
                                                </td>
                                                <td class="success">
                                                    <a href="#" onclick="javascript:getInfo('${v.fileId}')">查看详细</a>
                                                </td>
                                                <td>
                                                    <button type="button" class="btn btn-primary" onclick="Collection.addCollect('${v.fileId}')">收藏</button>
                                                    <a href="###" onclick="javascript:filePreview('${v.fileUrl}','${v.fileId}')"  class="btn btn-success">预览</a>
                                                    <a href="###" onclick="javascript:fileDownload('${v.fileUrl}','${v.fileId}');"  class="btn btn-info"> 下载</a>
                                                </td>
                                            </tr>
                                        </c:forEach>
                                    </a>

                                    <a name="file">
                                        <c:forEach items="${fileList}" var="f">
                                            <tr>

                                                <td>
                                                    <img src="<c:url value="${f.filePath}"/>"  style="weight:30px;height: 30px;"/>
                                                        ${f.fileName}
                                                </td>
                                                <td class="success">
                                                        ${f.fileType}
                                                </td>
                                                <td class="warning">
                                                        ${f.fileKeyword}
                                                </td>
                                                <td class="success">
                                                    <a href="#" onclick="javascript:getInfo('${f.fileId}')">查看详细</a>
                                                </td>
                                                <td>
                                                    <button type="button" class="btn btn-primary" onclick="Collection.addCollect('${f.fileId}')">收藏</button>
                                                    <a href="###" onclick="javascript:filePreview('${f.fileUrl}','${f.fileId}')"  class="btn btn-success">预览</a>
                                                    <%--<button type="button" class="btn btn-success">预览</button>--%>
                                                    <a href="###" onclick="javascript:fileDownload('${f.fileUrl}','${f.fileId}');"  class="btn btn-info"> 下载</a>
                                                </td>
                                            </tr>
                                        </c:forEach>
                                    </a>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 blog-sidebar">
                    <h3>推荐专家</h3>
                    <ul class="list-inline blog-images"  id="expertList">
                        <c:forEach items="${experts}" var="expert">

                            <li  class="col-md-3">
                                <a class="fancybox-button" data-rel="fancybox-button" title="${expert.expertName}  &nbsp; &nbsp;${expert.expertOrg}" href="../../assets/admin/pages/media/blog/1.jpg">
                                    <img  onclick="toExpertDetail('${expert.id}')" alt="" src="<c:url value="${expert.img}"/>">
                                </a>

                            </li>

                        </c:forEach>

                    </ul>



                    <div class="space20">
                    </div>
                    <h3>Top</h3>
                    <div class="top-news">

                        <a href="#music" class="btn green">
								<span>
								音频 </span>
                            <%--<em>Posted on: April 15, 2013</em>--%>
                            <em>
                                <i class="fa fa-tags"></i>
                            </em>
                            <i class="fa fa-music top-news-icon"></i>
                        </a>
                        <a href="#video" class="btn blue">
								<span>
								视频 </span>
                            <%--<em>Posted on: April 14, 2013</em>--%>
                            <em>
                                <i class="fa fa-tags"></i>
                            </em>
                            <i class="fa fa-globe top-news-icon"></i>
                        </a>
                        <a href="#file" class="btn yellow">
								<span>
								图片 </span>
                            <%--<em>Posted on: April 13, 2013</em>--%>
                            <em>
                                <i class="fa fa-tags"></i>
                            </em>
                            <i class="fa fa-book top-news-icon"></i>
                        </a>
                        <a href="#picture" class="btn purple">
								<span>
								其它</span>
                            <%--<em>Posted on: April 12, 2013</em>--%>
                            <em>
                                <i class="fa fa-tags"></i>
                            </em>
                            <i class="fa fa-bolt top-news-icon"></i>
                        </a>
                    </div>
                    <div class="space20">
                    </div>
                    <h3>其他热门领域</h3>
                    <ul class="list-inline sidebar-tags">
                     <c:forEach items="${TopTheme}" var="theme">
                        <li>

                         <a href="javascript:;" onclick="toThemeExperts('${theme.id}')">
                                <i class="fa fa-tags"></i>  ${theme.themeName} </a>
                        </li>

                     </c:forEach>

                    </ul>
                    <div class="space20">
                    </div>

                </div>
            </div>
            <!--end col-md-3-->
        </div>

    </div>
</div>


<!-- END PAGE CONTENT-->
<jsp:include page="../includes/footer.jsp"/>
<jsp:include page="../includes/script.jsp"/>

<![endif]-->
<script type="text/javascript" src="<c:url value="/js/plugins/pagination.js"/>"></script>
<script type="text/javascript" src="<c:url value="/js/user/Collention.js"/>"></script>
<script type="text/javascript" src="<c:url value="/js/user/upload.js"/>"></script>
<script>


    jQuery(document).ready(function() {
        Metronic.init(); // init metronic core components
        Layout.init(); // init current layout
    });
</script>
<script type="text/javascript">
    $(document).ready(function () {
        $("#focusThisTheme").on('switchChange.bootstrapSwitch', function (event, state) {
            var isChecked = state ? false : true;
            if (isChecked) {
                addUserFocusThemes([$("#themeId").val()]);
            } else {
                delUserFocusThemes([$("#themeId").val()]);
            }
        });
        $("#expertList").paging({
            itemSelector: "li.col-md-3",
            showItem: 12
        });
        $("#pictureLists").paging({
            itemSelector: "li.col-md-3",
            showItem: 4
        });
        $("#musicLists").paging({
            itemSelector: "li.col-md-3",
            showItem: 1
        });

    })
</script>
</body>
</html>
