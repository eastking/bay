<%--
  Created by IntelliJ IDEA.
  User: qiutiandong
  Date: 2016/6/10 0010
  Time: 21:31
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>查看详情信息</title>
    <jsp:include page="../includes/style.jsp"/>
</head>
<body>

<!-- BEGIN FORM-->

<form class="form-horizontal">
    <%--<div class="form-group">--%>
    <%--<label class="col-md-3 control-label font-blue">评分:${file.fileId}</label>--%>
    <%--<div class="col-md-9">--%>
    <%--<p class="form-control-static">--%>
    <%--<i class="fa fa-star font-blue"></i>--%>
    <%--<i class="fa fa-star font-blue"></i>--%>
    <%--<i class="fa fa-star font-blue"></i>--%>
    <%--<i class="fa fa-star font-blue"></i>--%>
    <%--<i class="fa fa-star font-blue"></i>--%>
    <%--</p>--%>
    <%--</div>--%>
    <%--</div>--%>
    <div class="form-group">
        <label class="col-md-3 control-label font-blue">编号:</label>
        <div class="col-md-6">

                ${file.fileId}

        </div>
    </div>
    <div class="form-group">
        <label class="col-md-3 control-label font-blue">关键字:</label>
        <div class="col-md-6">
            <p class="form-control-static">
                ${file.fileName}
            </p>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-3 control-label font-blue">上传时间:</label>
        <div class="col-md-6">
            <p class="form-control-static">
                ${file.fileTime}
            </p>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-3 control-label font-blue">上传人:</label>
        <div class="col-md-6">
            <p class="form-control-static">
                ${file.userId}
            </p>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-3 control-label font-blue">主题:</label>
        <div class="col-md-6">
            <p class="form-control-static">
                ${file.themeId}
            </p>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-3 control-label font-blue">类型:</label>
        <div class="col-md-6">
            <p class="form-control-static">
                ${file.fileType}
            </p>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-3 control-label font-blue">说明:</label>
        <div class="col-md-6">
            <p class="form-control-static">
                ${file.fileKeyword}
            </p>
        </div>
    </div>
</form>
</body>
</html>

