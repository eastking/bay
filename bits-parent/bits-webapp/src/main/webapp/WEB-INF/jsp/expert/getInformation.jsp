<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2016/6/8
  Time: 16:36
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <jsp:include page="../includes/style.jsp"/>
</head>
<body>

<!-- BEGIN FORM-->
<div class="main">
    <div class="col-md-12 col-sm-12">
        <div class="portlet blue-hoki box">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-cogs"></i>详细信息
                </div>
            </div>
            <div class="portlet-body">
                <%--文件描述--%>
                <ul class="nav nav-tabs">
                    <li  class="active">
                        <a href="#tab_2_1" data-toggle="tab">音频描述</a>
                    </li>

                </ul>
                <div class="tab-content">
                    <div class="tab-pane fade active in" id="tab_2_1">
                        <div class="row">
                            <div class="container">
                                <div class="row static-info">
                                    <div class="col-md-5 col-sm-2 name">
                                        评分:
                                    </div>
                                    <div class="col-md-5 col-sm-5 value">
                                        <i class="fa fa-star font-blue"></i>
                                        <i class="fa fa-star font-blue"></i>
                                        <i class="fa fa-star font-blue"></i>
                                        <i class="fa fa-star font-blue"></i>
                                        <i class="fa fa-star font-blue"></i>
                                    </div>
                                </div>
                                <div class="row static-info">
                                    <div class="col-md-5 col-sm-2 name">
                                        文件名:
                                    </div>
                                    <div class="col-md-5 value">
                                        ${file.fileName}
                                    </div>
                                </div>
                                <div class="row static-info">
                                    <div class="col-md-5 col-sm-2 name">
                                        知识类型:
                                    </div>
                                    <div class="col-md-5 value">
                                        ${file.fileType}
                                    </div>
                                </div>
                                <div class="row static-info">
                                    <div class="col-md-5 col-sm-2 name">
                                        浏览量:
                                    </div>
                                    <div class="col-md-5 value">
                                        12345
                                    </div>
                                </div>
                                <div class="row static-info">
                                    <div class="col-md-5 col-sm-2 name">
                                        下载量:
                                    </div>
                                    <div class="col-md-5 value">
                                        12345
                                    </div>
                                </div>
                                <div class="row static-info">
                                    <div class="col-md-5 col-sm-2 name">
                                        关键词:
                                    </div>
                                    <div class="col-md-7 value">
                                        ${file.fileKeyword}
                                    </div>
                                </div>
                                <div class="row static-info">
                                    <div class="col-md-5 col-sm-2 name">
                                        所属主题:
                                    </div>
                                    <div class="col-md-5 value">
                                        ${file.themeId}
                                    </div>
                                </div>
                                <div class="row static-info">
                                    <div class="col-md-5 col-sm-2 name">
                                        缩略图:
                                    </div>
                                    <div class="col-md-5 value">
                                        <img src="<c:url value="${file.filePath}"/>" height="200"  width="200">
                                    </div>
                                </div>
                                <div class="row static-info">
                                    <div class="col-md-5 col-sm-2 name">
                                        知识摘要:
                                    </div>
                                    <div class="col-md-5 value">
                                        ${file.fileAbstrat}
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

                <%--评论--%>
                <%--评论--%>
                <ul class="nav nav-tabs">
                    <li  class="active">
                        <a href="#tab_3_1" data-toggle="tab">专家评论</a>
                    </li>

                </ul>
                <div class="tab-content">
                    <div class="tab-pane fade active in" id="tab_3_1">
                        <div class="row">
                            <div class="testimonials-v1">
                                <div id="myCarousel" class="carousel slide">
                                    <!-- Carousel items -->
                                    <div class="carousel-inner">
                                        <div class="active item">
                                            <blockquote><p>Denim you probably haven't heard of. Lorem ipsum dolor met consectetur adipisicing sit amet, consectetur adipisicing elit, of them jean shorts sed magna aliqua. Lorem ipsum dolor met.</p></blockquote>
                                            <div class="carousel-info">
                                                <img class="pull-left" src="<c:url value="/assets/frontend/pages/img/people/img1-small.jpg"/>" alt="">
                                                <div class="pull-left">
                                                    <span class="testimonials-name">Lina Mars</span>
                                                    <span class="testimonials-post">Commercial Director</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="item">
                                            <blockquote><p>Raw denim you Mustache cliche tempor, williamsburg carles vegan helvetica probably haven't heard of them jean shorts austin. Nesciunt tofu stumptown aliqua, retro synth master cleanse. Mustache cliche tempor, williamsburg carles vegan helvetica.</p></blockquote>
                                            <div class="carousel-info">
                                                <img class="pull-left" src="<c:url value="/assets/frontend/pages/img/people/img5-small.jpg"/>" alt="">
                                                <div class="pull-left">
                                                    <span class="testimonials-name">Kate Ford</span>
                                                    <span class="testimonials-post">Commercial Director</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="item">
                                            <blockquote><p>Reprehenderit butcher stache cliche tempor, williamsburg carles vegan helvetica.retro keffiyeh dreamcatcher synth. Cosby sweater eu banh mi, qui irure terry richardson ex squid Aliquip placeat salvia cillum iphone.</p></blockquote>
                                            <div class="carousel-info">
                                                <img class="pull-left" src="<c:url value="/assets/frontend/pages/img/people/img2-small.jpg"/>" alt="">
                                                <div class="pull-left">
                                                    <span class="testimonials-name">Jake Witson</span>
                                                    <span class="testimonials-post">Commercial Director</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Carousel nav -->
                                    <a class="left-btn" href="#myCarousel" data-slide="prev"></a>
                                    <a class="right-btn" href="#myCarousel" data-slide="next"></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <br><br><br><br>
        </div>
    </div>
</div>

</body>
<script src="../../../assets/global/plugins/respond.min.js"></script>
<![endif]-->
<script src="<c:url value="/assets/global/plugins/jquery-migrate-1.2.1.min.js"/> " type="text/javascript"></script>
<script src="<c:url value="/assets/global/plugins/bootstrap/js/bootstrap.min.js"/> " type="text/javascript"></script>
</html>
