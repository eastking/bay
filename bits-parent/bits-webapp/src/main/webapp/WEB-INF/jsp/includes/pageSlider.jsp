<%--
  Created by IntelliJ IDEA.
  User: gao2
  Date: 15-4-5
  Time: 上午10:29
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
    <div class="fullscreen-container revolution-slider" style="height: 100% !important;">
        <div class="fullwidthbanner">
            <ul id="revolutionul">
                <li data-transition="fade" data-slotamount="8" data-masterspeed="700" data-delay="3000"
                    data-thumb="">
                    <!-- THE MAIN IMAGE IN THE FIRST SLIDE -->
                    <img src="<c:url value="/assets/frontend/pages/img/revolutionslider/bg10.jpg"/>" alt="">

                    <%--<div class="caption lft slide_title slide_item_left"--%>
                    <%--data-x="30"--%>
                    <%--data-y="105"--%>
                    <%--data-speed="400"--%>
                    <%--data-start="1500"--%>
                    <%--data-easing="easeOutExpo">--%>
                    <%--想寻找某一领域的专家？--%>
                    <%--</div>--%>
                    <%--<div class="caption lft slide_subtitle slide_item_left"--%>
                    <%--data-x="30"--%>
                    <%--data-y="180"--%>
                    <%--data-speed="400"--%>
                    <%--data-start="2000"--%>
                    <%--data-easing="easeOutExpo">--%>
                    <%--BAYMAX为您量身打造--%>
                    <%--</div>--%>
                    <%--<div class="caption lft slide_desc slide_item_left"--%>
                    <%--data-x="30"--%>
                    <%--data-y="220"--%>
                    <%--data-speed="400"--%>
                    <%--data-start="2500"--%>
                    <%--data-easing="easeOutExpo">--%>
                    <%--<p>登录本系统后能够获取您所需要的专家信息,</p>--%>
                    <%--<p>为您呈现相应主题专家网络。</p>--%>
                    <%--</div>--%>
                    <%--<div class="caption lfb"--%>
                    <%--data-x="640"--%>
                    <%--data-y="40"--%>
                    <%--data-speed="700"--%>
                    <%--data-start="1500"--%>
                    <%--data-easing="easeOutExpo">--%>
                    <%--<img src="<c:url value="/assets/frontend/pages/img/revolutionslider/index_second.png"/>" alt="Image 1">--%>
                    <%--</div>--%>
                </li>
                <!-- THE NEW SLIDE -->
                <li data-transition="fade" data-slotamount="8" data-masterspeed="700" data-delay="3000"
                    data-thumb="">
                    <!-- THE MAIN IMAGE IN THE FIRST SLIDE -->
                    <img src="<c:url value="/assets/frontend/pages/img/revolutionslider/bg11.jpg"/>" alt="">

                    <%--<div class="caption lft slide_title_white slide_item_left"--%>
                         <%--data-x="30"--%>
                         <%--data-y="90"--%>
                         <%--data-speed="400"--%>
                         <%--data-start="1500"--%>
                         <%--data-easing="easeOutExpo">--%>
                        <%--欢迎来到<br><span class="slide_title_white_bold">KBMS知识库管理系统</span>--%>
                    <%--</div>--%>
                    <%--<div class="caption lft slide_subtitle_white slide_item_left"--%>
                         <%--data-x="185"--%>
                         <%--data-y="245"--%>
                         <%--data-speed="400"--%>
                         <%--data-start="2000"--%>
                         <%--data-easing="easeOutExpo">--%>
                        <%--整理您的专属知识--%>
                    <%--</div>--%>

                    <%--<div class="caption lfb"--%>
                         <%--data-x="640"--%>
                         <%--data-y="30"--%>
                         <%--data-speed="700"--%>
                         <%--data-start="1000"--%>
                         <%--data-easing="easeOutExpo">--%>
                        <%--<img src="<c:url value="/assets/frontend/pages/img/revolutionslider/robot_index.png"/>" alt="Image 1">--%>
                    <%--</div>--%>
                </li>
                <!-- THE SECOND SLIDE -->
                <!-- THE THIRD SLIDE -->
                <li data-transition="fade" data-slotamount="8" data-masterspeed="700" data-delay="3000"
                    data-thumb="">
                    <!-- THE MAIN IMAGE IN THE FIRST SLIDE -->
                    <img src="<c:url value="/assets/frontend/pages/img/revolutionslider/bg4.jpg"/> " alt="">

                    <div class="caption lft slide_title"
                         data-x="30"
                         data-y="105"
                         data-speed="400"
                         data-start="1500"
                         data-easing="easeOutExpo">
                        KBMS知识库管理系统
                    </div>
                    <div class="caption lft slide_subtitle"
                         data-x="30"
                         data-y="180"
                         data-speed="400"
                         data-start="2000"
                         data-easing="easeOutExpo">
                        信息化时代信息无价
                    </div>
                    <div class="caption lft slide_desc"
                         data-x="30"
                         data-y="225"
                         data-speed="400"
                         data-start="2500"
                         data-easing="easeOutExpo">
                        如何管理信息<br> 拥有更多知识财富
                    </div>
                    <div class="caption lfb"
                         data-x="640"
                         data-y="20"
                         data-speed="700"
                         data-start="1500"
                         data-easing="easeOutBack">
                        <img src="<c:url value="/assets/frontend/pages/img/revolutionslider/index_third.png"/> "
                             alt="Image 2">
                    </div>
                </li>
                <!-- THE FORTH SLIDE -->
                <%--<li data-transition="fade" data-slotamount="8" data-masterspeed="700" data-delay="3000"--%>
                    <%--data-thumb="">--%>
                    <%--<img src="<c:url value="/assets/frontend/pages/img/revolutionslider/bg13.jpg"/> " alt="">--%>

                    <%--&lt;%&ndash;<div class="caption lfr slide_title"&ndash;%&gt;--%>
                         <%--&lt;%&ndash;data-x="470"&ndash;%&gt;--%>
                         <%--&lt;%&ndash;data-y="100"&ndash;%&gt;--%>
                         <%--&lt;%&ndash;data-speed="600"&ndash;%&gt;--%>
                         <%--&lt;%&ndash;data-start="1000"&ndash;%&gt;--%>
                         <%--&lt;%&ndash;data-easing="easeOutExpo">&ndash;%&gt;--%>
                        <%--&lt;%&ndash;24小时在线为您解答&ndash;%&gt;--%>
                    <%--&lt;%&ndash;</div>&ndash;%&gt;--%>
                    <%--&lt;%&ndash;<div class="caption lfr slide_subtitle"&ndash;%&gt;--%>
                         <%--&lt;%&ndash;data-x="470"&ndash;%&gt;--%>
                         <%--&lt;%&ndash;data-y="170"&ndash;%&gt;--%>
                         <%--&lt;%&ndash;data-speed="400"&ndash;%&gt;--%>
                         <%--&lt;%&ndash;data-start="1500"&ndash;%&gt;--%>
                         <%--&lt;%&ndash;data-easing="easeOutExpo">&ndash;%&gt;--%>
                        <%--&lt;%&ndash;比特能专家机器人为您服务&ndash;%&gt;--%>
                    <%--&lt;%&ndash;</div>&ndash;%&gt;--%>
                    <%--&lt;%&ndash;<div class="caption lfr slide_desc"&ndash;%&gt;--%>
                         <%--&lt;%&ndash;data-x="470"&ndash;%&gt;--%>
                         <%--&lt;%&ndash;data-y="220"&ndash;%&gt;--%>
                         <%--&lt;%&ndash;data-speed="400"&ndash;%&gt;--%>
                         <%--&lt;%&ndash;data-start="2000"&ndash;%&gt;--%>
                         <%--&lt;%&ndash;data-easing="easeOutExpo">&ndash;%&gt;--%>
                        <%--&lt;%&ndash;还在位专家难找而头疼吗<br>比特能专家来帮助你&ndash;%&gt;--%>
                    <%--&lt;%&ndash;</div>&ndash;%&gt;--%>
                    <%--&lt;%&ndash;<a class="caption lfr btn yellow slide_btn" href="javascript:;"&ndash;%&gt;--%>
                       <%--&lt;%&ndash;data-x="470"&ndash;%&gt;--%>
                       <%--&lt;%&ndash;data-y="280"&ndash;%&gt;--%>
                       <%--&lt;%&ndash;data-speed="400"&ndash;%&gt;--%>
                       <%--&lt;%&ndash;data-start="2500"&ndash;%&gt;--%>
                       <%--&lt;%&ndash;data-easing="easeOutExpo">&ndash;%&gt;--%>
                        <%--&lt;%&ndash;专家水平难辨&ndash;%&gt;--%>
                    <%--&lt;%&ndash;</a>&ndash;%&gt;--%>
                <%--</li>--%>
                <%--<!-- THE FIFTH SLIDE -->--%>
                <li data-transition="fade" data-slotamount="8" data-masterspeed="700" data-delay="3000"
                    data-thumb="">
                    <img src="<c:url value="/assets/frontend/pages/img/revolutionslider/bg6.jpg"/> " alt="">

                    <div class="caption lfl slide_title slide_item_left"
                         data-x="30"
                         data-y="125"
                         data-speed="400"
                         data-start="1500"
                         data-easing="easeOutExpo">
                        互联网时代 &amp; KBMS
                    </div>
                    <div class="caption lfl slide_subtitle slide_item_left"
                         data-x="30"
                         data-y="200"
                         data-speed="400"
                         data-start="2000"
                         data-easing="easeOutExpo">
                        信息化时代 &amp; 更多的是电子化财富
                    </div>
                    <div class="caption lfl slide_desc slide_item_left"
                         data-x="30"
                         data-y="245"
                         data-speed="400"
                         data-start="2500"
                         data-easing="easeOutExpo">
                        想要更高效的管理知识，就来KBMS知识库管理系统<br>
                    </div>
                    <div class="caption lfb"
                         data-x="670"
                         data-y="20"
                         data-speed="700"
                         data-start="1500"
                         data-easing="easeOutBack">
                        <img src="<c:url value="/assets/frontend/pages/img/revolutionslider/robot_index.png"/> "
                             alt="Image 1">
                    </div>
                </li>
            </ul>
            <div class="tp-bannertimer tp-bottom"></div>
        </div>
    </div>
<!-- END SLIDER -->
