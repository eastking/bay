
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
  <title></title>

  <link href="<c:url value="/css/jquery-accordion-menu.css"/>" rel="stylesheet" type="text/css" />
  <link href="<c:url value="/css/font-awesome.css"/>" rel="stylesheet" type="text/css" />
  <script src="<c:url value="/js/jquery-1.11.2.min.js"/>" type="text/javascript"></script>
  <script src="<c:url value="/js/jquery-accordion-menu.js"/>" type="text/javascript"></script>
</head>
<body>
<%--<div class="col-md-3 col-sm-3">--%>
<%--<div class="content">--%>

  <%--<div  class="jquery-accordion-menu red">--%>
    <%--<ul id="demo-list">--%>
      <%--<li class="active"><a href="#"><i class="fa fa-home"></i>Home </a></li>--%>
      <%--<li><a href="handArrangement"><i class="fa fa-suitcase"></i>百宝箱 </a></li>--%>
      <%--<li><a href="handTreasure"><i class="fa fa-briefcase"></i> 整理箱</a></li>--%>
      <%--<li><a href="handDustbin"><i class="fa fa-suitcase"></i> 垃圾箱</a></li>--%>
      <%--<li><a href="handFileUpload"><i class="fa fa-upload"></i>上传文件 </a></li>--%>
      <%--<li><a href="handleFocus"><i class="fa fa-home"></i>你的关注 </a></li>--%>
      <%--<li><a href="handleHistory"><i class="fa fa-suitcase"></i> 历史纪录</a>--%>
      <%--</li>--%>
      <%--<li><a href="handleUserInfo"><i class="fa fa-user"></i>个人资料</a></li>--%>
      <%--<li><a href="handlePassword"><i class="fa fa-envelope"></i>修改密码 </a></li>--%>
    <%--</ul>--%>
  <%--</div>--%>
<%--</div>--%>
  <%--</div>--%>

<div class="sidebar col-md-3 col-sm-5">
  <ul class="list-group margin-bottom-25 sidebar-menu">
    <li class="list-group-item clearfix"><a href="handArrangement"><i class="fa fa-suitcase"></i>百宝箱</a></li>
    <li class="list-group-item clearfix"><a href="handTreasure"><i class="fa fa-briefcase"></i>整理箱</a></li>
    <li class="list-group-item clearfix"><a href="handDustbin"><i class="fa fa-trash-o"></i>垃圾箱</a></li>
    <%--<li class="list-group-item clearfix"><a href="handleFocus"><i class="fa fa-angle-right"></i>你的关注</a></li>--%>
    <li class="list-group-item clearfix"><a href="handCommon"><i class="fa fa-list-alt"></i>常用文档</a></li>
    <li class="list-group-item clearfix"><a href="handFileUpload"><i class="fa fa-upload"></i>上传文件</a></li>
    <li class="list-group-item clearfix"><a href="handleHistory"><i class="fa fa-columns"></i>历史记录</a></li>
    <li class="list-group-item clearfix"><a href="handleUserInfo"><i class="fa fa fa-user"></i>个人资料</a></li>
    <li class="list-group-item clearfix"><a href="handlePassword"><i class="fa fa-pencil-square-o"></i>修改密码</a></li>

  </ul>
</div>
<!-- END SIDEBAR -->

</body>
</html>
