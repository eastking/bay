<%--
  Created by IntelliJ IDEA.
  User: gao2
  Date: 15-4-5
  Time: 上午10:17
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<html>
<head>
<style>
    #login-btn{
        border-radius: 100px !important;
        position: relative;
        top: 25px;
        margin-left: 30px;
    }
    #user-center{
        border-radius: 100px !important;
        position: relative;
        top: 25px;
        margin-left: 30px;
    }
    #logout{
        border-radius: 100px !important;
        position: relative;
        top: 25px;
        margin-left: 5px;
    }
</style>
<script>
    function logout() {
        location.href = basePath + "logout";
    }

    function toHome() {
        location.href = basePath;
    }
    function selfCenter(){
        location.href = basePath + "userCenter/handArrangement";
    }
    function toLogin(){
        location.href = basePath + "login";
    }
    function toRegistration(){
        location.href = basePath + "registration";
    }
    function toBack(){
        window.history.back();
    }
    function toPatent(){
        location.href = basePath + "patent/getPatentList";
    }
    function toExpert(){
        location.href = basePath + "expert/getExpertList";
    }
    function toClass(){
        location.href = basePath + "theme/getThemeList";
    }
    function toThemeMetadata() {
        location.href = basePath + "theme/ThemeMetadata";
    }
    function toMetadata(){
        location.href = basePath + "metadata/getMetadata";
    }
    function toPatentDetail(id){
        location.href = basePath + "patent/getPatentDetail?id=" + id;
    }
    function toExpertDetail(id){
        location.href = basePath + "expert/getExpertDetail?id=" + id;
    }
    function toThemeExperts(themeId){
        window.location.href = basePath + "expert/getThemeExperts?themeId=" + themeId;
    }
    function toAnswer(){
        location.href = basePath + "autoAnswer/getPage";
    }
    function toNetwork(){
        location.href = basePath + "expertWeb/getExpertWeb";
    }
    function toRecommend(){
        location.href = basePath + "expertRecommend/getExpertRecommend";
    }

    function toExpertAnswer(){
        location.href = basePath + "questionAndAnswer/getTheExperts";

    }

    function toTheme1(themeId1){
        location.href = basePath +"theme1/getThemeDetail?themeId=" + themeId1;
    }
    function teachingThought(){
        location.href = basePath + "subject/teachingThought";
    }

    function teachingThought(){
        location.href = basePath + "subject/teachingThought";
    }

    //教学资源
    function teachingResources(){
        location.href= basePath + "subject/teachingResources"
    }
    //科学研究
    function scientificResearch(){
        location.href=basePath + "subject/scientificResearch"
    }
    //校园文化
    function campusCulture(){
        location.href=basePath + "subject/campusCulture"
    }



    function showVideo(){
        $("#dialogTitle").text("视频播放");
        $("#userEditIframe").attr("src", basePath + "show/showVideo");
        $("#modalDialog").modal("show");
    }
</script>
</head>
<body>
<div class="header">
    <div class="container">
        <a class="site-logo" href="#"><img src="<c:url value="/image/img/logo/logo(160x34).png"/>"
                                           alt="KBMS"></a>
        <a href="javascript:void(0);" class="mobi-toggler"><i class="fa fa-bars"></i></a>
        <!-- BEGIN NAVIGATION -->
        <div class="header-navigation pull-right font-transform-inherit">
            <ul>
                <li>
                    <%--<a href="#" onclick="toClass()">类别</a>--%>
                    <a class="dropdown-toggle" data-toggle="dropdown" data-target="#" href="javascript:;">
                        知识类别
                        <i class="fa fa-angle-down"></i>
                    </a>
                    <ul class="dropdown-menu">
                        <li><a href="javascript:;" onclick="toClass()">组织</a></li>
                        <li><a href="javascript:;" onclick="">部门</a></li>
                        <li><a href="javascript:;" onclick="">个人</a></li>
                    </ul>
                </li>

                <%--<li class="dropdown">--%>

                    <%--<a class="dropdown-toggle" data-toggle="dropdown" data-target="#" href="javascript:;">--%>
                        <%--主题--%>
                        <%--<i class="fa fa-angle-down"></i>--%>
                    <%--</a>--%>
                    <%--<ul class="dropdown-menu">--%>
                        <%--<li><a href="javascript:;" onclick="teachingThought()">教育思想</a></li>--%>
                        <%--<li><a href="javascript:;" onclick="teachingResources()">教学资源</a></li>--%>
                        <%--<li><a href="javascript:;" onclick="scientificResearch()">科学研究</a></li>--%>
                        <%--<li><a href="javascript:;" onclick="campusCulture()">校园文化</a></li>--%>
                    <%--</ul>--%>
                <%--</li>--%>

                <li>
                    <a href="#" onclick="toThemeMetadata()">
                        元数据
                        <i class="fa fa-angle-down"></i>
                    </a>
                </li>
                <li class="dropdown">

                    <a class="dropdown-toggle" data-toggle="dropdown" data-target="#" href="javascript:;">
                        专家
                        <i class="fa fa-angle-down"></i>
                    </a>
                    <ul class="dropdown-menu">
                        <li><a href="javascript:;" onclick="toNetwork()">专家网络</a></li>
                        <li><a href="javascript:;" onclick="toRecommend()">专家推荐</a></li>
                    </ul>
                </li>

                <li class="dropdown">

                    <a class="dropdown-toggle" data-toggle="dropdown" data-target="#" href="javascript:;">
                        问答
                        <i class="fa fa-angle-down"></i>
                    </a>
                    <ul class="dropdown-menu">
                        <li><a href="javascript:;" onclick="toAnswer()">知识问答</a></li>
                        <li><a href="javascript:;" onclick="toExpertAnswer()">求助专家</a></li>
                        <li><a href="http://localhost:8080/java-china" onclick="">研讨社区</a></li>
                    </ul>
                </li>
                <li>
                    <a href="#" onclick="toMetadata()">搜索</a>
                </li>
                <li>
                    <a href="#" onclick="toHome()">
                        首页
                        <i class="fa fa-angle-down"></i>
                    </a>
                </li>
                <li class="menu-search">
                    <span class="sep"></span>
                    <i class="fa fa-search search-btn" id="mySearch" onclick="on_search()"></i>

                    <div class="search-box">
                        <form action="search">
                            <div class="input-group">
                                <input type="text" name="key" class="form-control" placeholder="全局检索">
                                <input type="hidden" value="global" name="direction"/>
                                <span class="input-group-btn">
                                  <button class="btn btn-primary" type="submit">搜索</button>
                                </span>
                            </div>
                        </form>
                    </div>

                </li>
                <li>
                    <c:if test="${User.username == null}">
                        <button class="btn green pull-right" id="login-btn" onclick="toLogin()">
                            登陆
                        </button>
                    </c:if>
                    <c:if test="${User.username != null}">
                        <button class="btn red pull-right" onclick="logout()" id="logout">注销</button>
                        <button class="btn yellow pull-right" id="user-center" onclick="selfCenter('${User.userId}')">${User.username}</button>
                    </c:if>
                </li>
            </ul>
        </div>
        <!-- END NAVIGATION -->
    </div>
</div>
</body>
</html>
