<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2016/6/8
  Time: 20:37
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <jsp:include page="../includes/style.jsp"/>
    <link href="<c:url value="/assets/frontend/pages/css/portfolio.css"/>" rel="stylesheet" type="text/css">
    <link href="<c:url value="/assets/frontend/pages/css/gallery.css"/>" rel="stylesheet" type="text/css">
    <title>搜索</title>
</head>
<jsp:include page="../includes/header.jsp"/>
<input type="hidden" value="${User.userId}" id="userId">

<body style="background: #ffffff">
<div class="main">
    <div class="container">
        <ul class="breadcrumb">

            <li class="active">搜索</li>
        </ul>

        <form action="#" id="form_sample_1" class="form-horizontal" method="post" enctype="multipart/form-data">



            <div  class="input-group input-large date-picker input-daterange"  style="margin-left: 300px;">

                <div class="form-group">
                    <label class="col-md-3 control-label">文件名：</label>
                    <div class="col-md-9">
                        <input  id="fname" name="fname"  type="text" class="form-control input-inline input-medium">
                    </div>
                </div>
                <div class="form-group">
                    <select name="A" size="1" aria-controls="datatable_orders" class="form-control input-xsmall input-sm">
                        <option value="1" selected="selected">与</option>
                        <option value="0">或</option>
                    </select>
                </div>

                <div class="form-group">
                    <label class="col-md-3 control-label">关键字：</label>
                    <div class="col-md-9">
                        <input  id="fkeyword" name="fkeyword" type="text" class="form-control input-inline input-medium">
                    </div>
                </div>

                <div class="form-group">
                    <select name="B" size="1" aria-controls="datatable_orders" class="form-control input-xsmall input-sm">
                        <option value="1" selected="selected">与</option>
                        <option value="0">或</option>
                    </select>
                </div>

                <div class="form-group">
                    <label class="col-md-3 control-label">时间：</label>
                    <div class="col-md-9">
                        <input  id="ftime" name="ftime" type="text" class="form-control input-inline input-medium">
                    </div>
                </div>

                <div class="btn-group">



                    <div class="col-md-9"  style="margin-left: 100px;">
                        <select  name="ftype" size="1" aria-controls="datatable_orders" class="form-control input-xsmall input-sm">

                            <option value="所有">所有</option>
                            <option value="图片">图片</option>
                            <option value="视频">视频</option>
                            <option value="音频">音频</option>
                            <option value="其它">其它</option>
                        </select>
                    </div>
                </div>

                <div  style="margin-left: 60px;">
                    <button  onclick="abcSearch()" class="btn btn-default btn-lg dropdown-toggle" type="button" data-toggle="dropdown" style="margin-left: 0px; width: 200px;">
                        搜索 <i class="fa fa-angle-down"></i>
                    </button>
                </div>



            </div>
            <hr>
            <!-- /input-group -->




        </form>


        <div class="portlet-body">
            <div class="table-responsive">
                <table class="table table-bordered table-hover">

        <c:forEach items="${f}" var="p">

            <tr>

                <td>
                    <img src="<c:url value="${p.filePath}"/>" style="weight:30px;height: 30px;"/>${p.fileName}
                </td>
                <td class="success">
                        ${p.fileType}
                </td>
                <td class="warning">
                        ${p.fileKeyword}
                </td>
                <td class="success">
                    <a href="#" onclick="javascript:getInfo('${p.fileId}')">查看详细</a>
                </td>
                <td>
                    <button type="button" class="btn btn-primary" onclick="Collection.addCollect('${p.fileId}')">收藏</button>
                        <%--<button type="button" class="btn btn-success">预览</button>--%>
                    <a href="###" onclick="javascript:filePreview('${p.fileUrl}','${p.fileId}')"  class="btn btn-success">预览</a>
                        <%--<button onclick="fileDownload('${p.filePath}');" class="btn btn-info">下载</button>--%>
                    <a href="###" onclick="javascript:fileDownload('${p.fileUrl}','${p.fileId}');"  class="btn btn-info"> 下载</a>
                </td>
            </tr>
        </c:forEach>
                    </table>
                </div>
            </div>

    </div>
</div>
<!-- END FORM-->
<!-- END PAGE CONTENT-->

<jsp:include page="../includes/script.jsp"/>
<script type="text/javascript" src="<c:url value="/js/user/upload.js"/>"></script>
</body>

</html>
