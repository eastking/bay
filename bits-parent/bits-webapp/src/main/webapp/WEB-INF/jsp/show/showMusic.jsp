
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <title></title>
    <jsp:include page="../includes/style.jsp"/>
    <link rel="stylesheet" href="<c:url value="/css/demo.css" />"/>
    <link rel="stylesheet" href="<c:url value="/css/audioplayer.css"/>" />
</head>
<body>
<div class="main">
    <div class="col-md-12 col-sm-12">
                <div class="portlet blue-hoki box">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-cogs"></i>详细信息
                        </div>
                    </div>
                    <div class="portlet-body">
                        <%--音乐播放--%>
                        <ul class="nav nav-tabs">
                            <li  class="active">
                                <a href="#tab_1_1" data-toggle="tab">音乐播放</a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane fade active in" id="tab_1_1">
                                <div class="row">
                                    <div class="container">
                                        <div id="wrapper">
                                            <audio preload="auto" controls>
                                                <source src="<c:url value="${file.filePath}"/>">
                                            </audio>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <br><br><br><br>
                            <%--文件描述--%>
                        <ul class="nav nav-tabs">
                            <li  class="active">
                                <a href="#tab_2_1" data-toggle="tab">音频描述</a>
                            </li>

                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane fade active in" id="tab_2_1">
                                <div class="row">
                                    <div class="container">
                                        <div class="row static-info">
                                            <div class="col-md-5 col-sm-2 name">
                                                评分:
                                            </div>
                                            <div class="col-md-5 col-sm-5 value">
                                                    <i class="fa fa-star font-blue"></i>
                                                    <i class="fa fa-star font-blue"></i>
                                                    <i class="fa fa-star font-blue"></i>
                                                    <i class="fa fa-star font-blue"></i>
                                                    <i class="fa fa-star font-blue"></i>
                                            </div>
                                        </div>
                                        <div class="row static-info">
                                            <div class="col-md-5 col-sm-2 name">
                                                文件名:
                                            </div>
                                            <div class="col-md-5 value">
                                                ${file.fileName}
                                            </div>
                                        </div>
                                        <div class="row static-info">
                                            <div class="col-md-5 col-sm-2 name">
                                                知识类型:
                                            </div>
                                            <div class="col-md-5 value">
                                                ${file.fileType}
                                            </div>
                                        </div>
                                        <div class="row static-info">
                                            <div class="col-md-5 col-sm-2 name">
                                                浏览量:
                                            </div>
                                            <div class="col-md-5 value">
                                                12345
                                            </div>
                                        </div>
                                        <div class="row static-info">
                                            <div class="col-md-5 col-sm-2 name">
                                                下载量:
                                            </div>
                                            <div class="col-md-5 value">
                                                12345
                                            </div>
                                        </div>
                                        <div class="row static-info">
                                            <div class="col-md-5 col-sm-2 name">
                                                关键词:
                                            </div>
                                            <div class="col-md-7 value">
                                                ${file.fileKeyword}
                                            </div>
                                        </div>
                                        <div class="row static-info">
                                            <div class="col-md-5 col-sm-2 name">
                                                所属主题:
                                            </div>
                                            <div class="col-md-5 value">
                                                ${file.themeId}
                                            </div>
                                        </div>
                                        <div class="row static-info">
                                            <div class="col-md-5 col-sm-2 name">
                                                缩略图:
                                            </div>
                                            <div class="col-md-5 col-sm-2">
                                                <img src="<c:url value="${file.filePath}"/>" height="200"  width="200">
                                            </div>
                                        </div>
                                        <div class="row static-info">
                                            <div class="col-md-5 col-sm-2 name">
                                                知识摘要:
                                            </div>
                                            <div class="col-md-5 value">
                                                ${file.fileAbstrat}
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <%--评论--%>
                            <%--评论--%>
                            <ul class="nav nav-tabs">
                                <li  class="active">
                                    <a href="#tab_3_1" data-toggle="tab">专家评论</a>
                                </li>

                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane fade active in" id="tab_3_1">
                                    <div class="row">
                                        <div class="testimonials-v1">
                                            <div id="myCarousel" class="carousel slide">
                                                <!-- Carousel items -->
                                                <div class="carousel-inner">
                                                    <div class="active item">
                                                        <blockquote><p>械键盘在近两年可谓是出尽了风头，从最早的天价级产品到基础大众普及产品，这主要是由于机械键盘的价格逐渐走向了平民化。说起机械键盘，就不得不提到其十分关键也是确定其高端地位的部分——机械轴。</p></blockquote>
                                                        <div class="carousel-info">
                                                            <img class="pull-left" src="<c:url value="/assets/frontend/pages/img/people/img1-small.jpg"/>" alt="">
                                                            <div class="pull-left">
                                                                <span class="testimonials-name">周卫国</span>
                                                                <span class="testimonials-post">计算机学家</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="item">
                                                        <blockquote><p>械键盘在近两年可谓是出尽了风头，从最早的天价级产品到基础大众普及产品，这主要是由于机械键盘的价格逐渐走向了平民化。说起机械键盘，就不得不提到其十分关键也是确定其高端地位的部分——机械轴。</p></blockquote>
                                                        <div class="carousel-info">
                                                            <img class="pull-left" src="<c:url value="/assets/frontend/pages/img/people/img5-small.jpg"/>" alt="">
                                                            <div class="pull-left">
                                                                <span class="testimonials-name">周国</span>
                                                                <span class="testimonials-post">计算机学家</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="item">
                                                        <blockquote><p>械键盘在近两年可谓是出尽了风头，从最早的天价级产品到基础大众普及产品，这主要是由于机械键盘的价格逐渐走向了平民化。说起机械键盘，就不得不提到其十分关键也是确定其高端地位的部分——机械轴。.</p></blockquote>
                                                        <div class="carousel-info">
                                                            <img class="pull-left" src="<c:url value="/assets/frontend/pages/img/people/img2-small.jpg"/>" alt="">
                                                            <div class="pull-left">
                                                                <span class="testimonials-name">周卫</span>
                                                                <span class="testimonials-post">计算机学家</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- Carousel nav -->
                                                <a class="left-btn" href="#myCarousel" data-slide="prev"></a>
                                                <a class="right-btn" href="#myCarousel" data-slide="next"></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    </div>
                    <br><br><br><br>
                </div>
            </div>
</div>
<script src="<c:url value="/js/jquery.js"/>"></script>
<script src="<c:url value="/js/audioplayer.js"/>"></script>
<script src="../../../assets/global/plugins/respond.min.js"></script>
<![endif]-->
<script src="<c:url value="/assets/global/plugins/jquery-migrate-1.2.1.min.js"/> " type="text/javascript"></script>
<script src="<c:url value="/assets/global/plugins/bootstrap/js/bootstrap.min.js"/> " type="text/javascript"></script>

<script>
    $( function() {
        $( 'audio' ).audioPlayer();
    });
</script>
</body>
</html>