/*
* DictConstant.java
* Created on  202015/3/14 14:19
* 版本       修改时间         作者        修改内容
* V1.0.1     2015/3/14     panzhuowen    初始版本
*
*/

package com.ccut.bits.util;

/**
 * 类的描述信息
 *
 * @author panzhuowen
 * @version 1.0.1
 */
public final class DictConstant {
    public DictConstant() {
    }
    public static final String adminRoleName = "系统管理员";
    public static final String userRoleName = "系统用户";
}
