package com.ccut.bits.index;

import com.ccut.bits.constant.Constant;
import com.ccut.bits.expert.dao.ExpertDao;
import com.ccut.bits.expert.model.Expert;
import com.ccut.bits.files.dao.FileDao;
import com.ccut.bits.files.entity.FileVO;
import com.ccut.bits.intelligence.dao.IntelligenceDao;
import com.ccut.bits.intelligence.model.Intelligence;
import com.ccut.bits.literature.dao.LiteratureDao;
import com.ccut.bits.literature.model.Literature;
import com.ccut.bits.lucene.LuceneIndex;
import com.ccut.bits.patent.dao.PatentDao;
import com.ccut.bits.patent.model.Patent;
import com.ccut.bits.theme.dao.ThemeDao;
import com.ccut.bits.theme.model.Theme;
import org.apache.lucene.document.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * 每天凌晨2点进行重新索引，以保证以最新词库进行分词索引，提高命中准确性
 */
@Service("indexService")
public class IndexServiceImp implements IndexService {
    @Autowired
    private ExpertDao expertDao;
    @Autowired
    private PatentDao patentDao;
    @Autowired
    private LiteratureDao literatureDao;

    @Autowired
    private FileDao fileDao;
    @Autowired
    private IntelligenceDao intelligenceDao;
    @Autowired
    private ThemeDao themeDao;
    @Autowired
    private LuceneIndex luceneIndex;
    private List<Field> fields = new ArrayList<>();

    private void setFieldScore(Map<String, Float> score) {
        Iterator<Field> i = this.fields.iterator();
        Field field;
        while (i.hasNext()) {
            field = i.next();
            if (score.keySet().contains(field.name()))
            field.setBoost(score.get(field.name()));
        }
    }

    private void clearFields() {
        if (this.fields == null) {
            this.fields = new ArrayList<>();
        } else {
            this.fields.clear();
        }
    }

    private Document getDocument() {
        Document doc = new Document();
        Iterator<Field> i = this.fields.iterator();
        while (i.hasNext()) {
            doc.add(i.next());
        }
        return doc;
    }

    @Override
    public boolean initLiteratureIndex() {
        List<Literature> result = literatureDao.getAllLiterature(null, null);
        List<Document> documents = new LinkedList<>();
        Map<String, Float> score = new HashMap<>();
        score.put(Constant.Index.Literature.FIELD_TITLE, 15F);
        score.put(Constant.Index.Literature.FIELD_KEY, 3F);
        for (Literature li : result) {
            this.clearFields();
            fields.add(new IntField(Constant.Index.Literature.FIELD_ID, li.getId(), Field.Store.YES));
            fields.add(new TextField(Constant.Index.Literature.FIELD_TITLE, li.getTitle(), Field.Store.YES));
            fields.add(new TextField(Constant.Index.Literature.FIELD_ABS, li.getAbs(), Field.Store.YES));
            fields.add(new TextField(Constant.Index.Literature.FIELD_KEY, li.getKey(), Field.Store.YES));
            fields.add(new StringField(Constant.Index.Literature.FIELD_AUTHOR, li.getAuthorCn(), Field.Store.YES));
            fields.add(new StringField(Constant.Index.Literature.FIELD_UNIT, li.getUnit(), Field.Store.YES));
            fields.add(new StringField(Constant.Index.CLASS, Constant.Index.LITERATURE, Field.Store.YES));
            fields.add(new StringField(Constant.Index.Literature.FIELD_DATE, li.getAppDate(), Field.Store.YES));
            this.setFieldScore(score);
            documents.add(this.getDocument());
        }
        return luceneIndex.createIndex(documents);
    }

    @Override
    public boolean initExpertIndex() {
        List<Expert> result = expertDao.getAllExpert(null, null);
        List<Document> docs = new LinkedList<Document>();
        Document document = null;
        for (Expert expert : result) {
            document = new Document();
            document.add(new IntField(Constant.Index.Expert.FIELD_ID, expert.getId(), Field.Store.YES));
            document.add(new TextField(Constant.Index.Expert.FIELD_NAME, expert.getExpertName(), Field.Store.YES));
            document.add(new TextField(Constant.Index.Expert.FIELD_ORGANIZATION, expert.getExpertOrg(), Field.Store.YES));
            document.add(new StringField(Constant.Index.CLASS, Constant.Index.EXPERT, Field.Store.YES));
            fields.add(new StringField(Constant.Index.CLASS, Constant.Index.FILEVO, Field.Store.YES));
            docs.add(document);
        }
        return luceneIndex.createIndex(docs);
    }

    @Override
    public boolean initPatentIndex() {
        List<Patent> result = patentDao.getAllPatent(null, null);
        List<Document> documents = new LinkedList<>();
        Map<String,Float> score = new HashMap<>();
        score.put(Constant.Index.Patent.FIELD_TITLE,15F);
        for (Patent patent : result) {
            this.clearFields();
            fields.add(new IntField(Constant.Index.Patent.FIELD_ID, patent.getId(), Field.Store.YES));
            fields.add(new TextField(Constant.Index.Patent.FIELD_TITLE, patent.getTitle(), Field.Store.YES));
            fields.add(new TextField(Constant.Index.Patent.FIELD_ABS, patent.getAbs(), Field.Store.YES));
            fields.add(new StringField(Constant.Index.Patent.FIELD_APPLICANT, patent.getApplicant(), Field.Store.YES));
            fields.add(new StringField(Constant.Index.Patent.FIELD_INVENTOR, patent.getInventor(), Field.Store.YES));
            fields.add(new StringField(Constant.Index.Patent.FIELD_DATE, patent.getDate(), Field.Store.YES));
            fields.add(new StringField(Constant.Index.CLASS, Constant.Index.PATENT, Field.Store.YES));
            this.setFieldScore(score);
            documents.add(this.getDocument());
        }
        return luceneIndex.createIndex(documents);
    }

    @Override
    public boolean initIntelligenceIndex() {
        List<Intelligence> result = intelligenceDao.getAllIntelligence();
        List<Document> documents = new LinkedList<>();
        Document document = null;
        for (Intelligence intel : result) {
            document = new Document();
            document.add(new IntField(Constant.Index.Intelligence.FIELD_ID, intel.getId(), Field.Store.YES));
            document.add(new StringField(Constant.Index.Intelligence.FIELD_URL, intel.getUrl(), Field.Store.YES));
            document.add(new StringField(Constant.Index.Intelligence.FIELD_DATE, intel.getDate(), Field.Store.YES));
            document.add(new TextField(Constant.Index.Intelligence.FIELD_TITLE, intel.getTitle(), Field.Store.YES));
            document.add(new TextField(Constant.Index.Intelligence.FIELD_ABS, intel.getAbs(), Field.Store.YES));
            document.add(new StringField(Constant.Index.CLASS, Constant.Index.INTELLIGENCE, Field.Store.YES));
            documents.add(document);
        }
        return luceneIndex.createIndex(documents);
    }

    @Override
    public boolean initThemeIndex() {
        List<Theme> themes = themeDao.getAllTheme();
        List<Document> documents = new LinkedList<>();
        Document document = null;
        for (Theme theme : themes) {
            document = new Document();
            document.add(new IntField(Constant.Index.Theme.FIELD_ID, theme.getId(), Field.Store.YES));
            document.add(new TextField(Constant.Index.Theme.FIELD_THEME_NAME, theme.getThemeName(), Field.Store.YES));
            document.add(new TextField(Constant.Index.Theme.FIELD_KEY_WORD, theme.getKeyWord(), Field.Store.YES));
            documents.add(document);
        }
        return luceneIndex.createIndex(documents);
    }

    @Override
    public boolean initFileVOIndex(){
        System.out.print("我的索引");
        List<FileVO> result=fileDao.getALLfile(null,null);
        List<Document> documents = new LinkedList<>();
        Document document = null;

        for (FileVO fileVO : result ) {
            document = new Document();

            document.add(new StringField(Constant.Index.FileVO.FIELD_NAME,fileVO.getFileName(), Field.Store.YES));
            document.add(new StringField(Constant.Index.FileVO.FIELD_KEY,fileVO.getFileKeyword(), Field.Store.YES));
            document.add(new StringField(Constant.Index.FileVO.FIELD_URL,fileVO.getFileUrl(), Field.Store.YES));
            document.add(new StringField(Constant.Index.FileVO.FIELD_DATE,fileVO.getFileTime(), Field.Store.YES));
            document.add(new StringField(Constant.Index.FileVO.FIELD_ID,fileVO.getFileId(), Field.Store.YES));


            documents.add(document);
        }
        return luceneIndex.createIndex(documents);

    }

    @Override
    @Scheduled(cron = "0 0 2  * * ?")
    public void scheduledReWriteIndex() {
        autoCreateIndex();
    }

    @Override
    public void autoCreateIndex() {
      initLiteratureIndex();
        initExpertIndex();
        initPatentIndex();
        initIntelligenceIndex();
        initFileVOIndex();
    }
}
