package com.ccut.bits.search.model;

import com.ccut.bits.constant.Constant;
import com.ccut.bits.page.Page;
import com.ccut.bits.util.PageUtil;
import com.ccut.bits.util.StringUtil;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by Demon on 2015/4/12.
 */
public class SearchRequest {
    private String key;
    private String field;
    private String[] fields;
    private String direction;
    private Page page;
    private boolean isBlank = false;
    private static final String FIELD = "field";
    private static final String KEY = "key";
    private static final String DIRECTION = "direction";


    public SearchRequest(HttpServletRequest request){
        String key = request.getParameter(KEY);
        String field = request.getParameter(FIELD);
        String direction = request.getParameter(DIRECTION);
        if(StringUtil.isEmpty(key) || StringUtil.isEmpty(direction)) {
            isBlank = true;
            return;
        }
        this.key = PageUtil.encoding(key, Constant.Page.ISO_8859_1, Constant.Page.UTF_8);
        this.field = field;
        this.direction = direction;
        this.page = new Page(request);
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    public String[] getFields() {
        return fields;
    }

    public void setFields(String[] fields) {
        this.fields = fields;
    }

    public String getDirection(){
        return direction;
    }

    public void setDirection(String direction){
        this.direction = direction;
    }

    public Page getPage() {
        return page;
    }

    public void setPage(Page page) {
        this.page = page;
    }

    public boolean isValid(){
        return isBlank;
    }
}
