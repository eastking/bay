package com.ccut.bits.autoanswer.model;

/**
 * Created by qiutiandong on 2016/6/10 0010.
 */
public class ResultBean {
    private String code;
    private String text;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
