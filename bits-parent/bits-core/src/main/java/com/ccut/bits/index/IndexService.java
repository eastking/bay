package com.ccut.bits.index;

/**
 * Created by Demon on 2015/5/5.
 */
public interface IndexService {
    boolean initLiteratureIndex();

    boolean initExpertIndex();

    boolean initPatentIndex();

    boolean initIntelligenceIndex();

    boolean initThemeIndex();
    boolean initFileVOIndex();

    void scheduledReWriteIndex();

    void autoCreateIndex();
}
