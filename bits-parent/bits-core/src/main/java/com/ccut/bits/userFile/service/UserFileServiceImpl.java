package com.ccut.bits.userFile.service;

import com.ccut.bits.model.User;
import com.ccut.bits.userCenter.dao.UserCenterDao;
import com.ccut.bits.userCenter.model.History;
import com.ccut.bits.userFile.dao.UserFileDao;
import com.ccut.bits.userFile.model.UserFileVO;
import org.apache.commons.fileupload.FileUploadException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;


/**
 * Created by Administrator on 2016/6/6.
 */
@Service
public class UserFileServiceImpl implements UserFileService {
    @Autowired
    private UserFileDao userFileDao;
    @Autowired
    private UserCenterDao userCenterDao;
    private final Integer DEFAULT_TOP_SIZE = 10;

    @Override
    public List<UserFileVO> getUserArrangementListPage(UserFileVO userFileVO) {
        List<UserFileVO> userFiles = userFileDao.getUserArrangementListPage(userFileVO);
        return userFiles;
    }

    @Override
    public List<UserFileVO> getUserArrangementImgListPage(UserFileVO userFileVO) {
        List<UserFileVO> userFiles = userFileDao.getUserArrangementImgListPage(userFileVO);
        return userFiles;
    }

    @Override
    public List<UserFileVO> getUserArrangementVideoListPage(UserFileVO userFileVO) {
        List<UserFileVO> userFiles = userFileDao.getUserArrangementVideoListPage(userFileVO);
        return userFiles;
    }

    @Override
    public List<UserFileVO> getUserArrangementMusicListPage(UserFileVO userFileVO) {
        List<UserFileVO> userFiles = userFileDao.getUserArrangementMusicListPage(userFileVO);
        return userFiles;
    }

    @Override
    public List<UserFileVO> getUserArrangementOtherListPage(UserFileVO userFileVO) {
        List<UserFileVO> userFiles = userFileDao.getUserArrangementOtherListPage(userFileVO);
        return userFiles;
    }

    @Override
    public List<UserFileVO> getUserCommonListPage(UserFileVO userFileVO) {
        List<UserFileVO> userFiles = userFileDao.getUserCommonListPage(userFileVO);
        return userFiles;
    }

    @Override
    public UserFileVO findById(int id) {
        return userFileDao.findById(id);
    }

    @Override
    public void editFileName(UserFileVO userFileVO) {

        userFileDao.editFileName(userFileVO);
    }

    @Override
    public void delArrangement(int id) {
        userFileDao.delArrangement(id);
    }

    @Override
    public void delArrangementImg(int id) {
        userFileDao.delArrangementImg(id);
    }

    @Override
    public void delArrangementMusic(int id) {
        userFileDao.delArrangementMusic(id);
    }

    @Override
    public void delArrangementVideo(int id) {
        userFileDao.delArrangementVideo(id);
    }

    @Override
    public void delArrangementOther(int id) {
        userFileDao.delArrangementOther(id);
    }

    @Override
    public void addCommon(UserFileVO userFileVO) {
        userFileDao.addCommon(userFileVO);
    }

    @Override
    public List<UserFileVO> getFileNameById(int id) {
        return userFileDao.getFileNameById(id);
    }

    @Override
    public void addShare(UserFileVO userFileVO) {
        userFileDao.addShare(userFileVO);
    }

    @Override
    public List<UserFileVO> getNewKnowledge() {
        return userFileDao.getNewKnowledge(DEFAULT_TOP_SIZE);
    }

    @Override
    public void addCollect(UserFileVO userFileVO) {
        userFileDao.addCollect(userFileVO);
    }

    @Override
    public List<UserFileVO> getDelUserArrangementListPage(UserFileVO userFileVO) {
        List<UserFileVO> userFiles = userFileDao.getDelUserArrangementListPage(userFileVO);
        return userFiles;
    }

    @Override
    public void delArrangementDus(int id) {
        userFileDao.delArrangementDus(id);
    }

    @Override
    public void addArrangement(UserFileVO userFileVO) {
        userFileDao.addArrangement(userFileVO);
    }

    @Override
    public List<UserFileVO> search(String fsearch) {
        return userFileDao.search(fsearch);
    }


    @Override
    public void uploadFile(MultipartFile file, HttpServletRequest request, HttpSession session, HttpServletResponse response, int i) throws IOException, FileUploadException {

        String fileName = request.getParameterValues("fileName")[i];
        String fileKeyword = request.getParameterValues("fileKeyword")[i];
        String fileAbstrat = request.getParameterValues("explain")[i];
        String fileId = "";
        String realUrl = "";
        String suffix = "";//后缀
        String id="";

//        try {
//            request.setCharacterEncoding("UTF-8");
//        } catch (UnsupportedEncodingException e) {
//            e.printStackTrace();
//        }
        try {
            if (!file.isEmpty()) {
                byte[] bytes = file.getBytes();
                String path = request.getSession().getServletContext().getRealPath("/");

                File upFile = new File(path + "attachment");

                if (!upFile.exists()) {
                    upFile.mkdirs();
                }
                if (file.getSize() / 1000 > 10240) {
//                    throw new ValidateErrorException("上传附件不能大于10M!");
                }
                String fileNameArray[] = file.getOriginalFilename().split("\\.");
                String url = path + "attachment/" + new Date().getTime() + "." + fileNameArray[fileNameArray.length - 1];

                realUrl = "attachment/" + new Date().getTime() + "." + fileNameArray[fileNameArray.length - 1];

                fileId = String.valueOf(new Date().getTime());

                id=fileId.substring(fileId.length()-4,fileId.length());
                suffix = fileNameArray[fileNameArray.length - 1];


                if (!upFile.exists()) {
                    upFile.mkdirs();
                }
                FileOutputStream fos = new FileOutputStream(url); // 上传到写死的上传路径
                fos.write(bytes);  //写入文件
                fos.close();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return;
        } catch (IOException e) {
            e.printStackTrace();
            return;
        }
        User user = (User) session.getAttribute("User");

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");//可以方便地修改日期格式
        Date now = new Date();
        String hehe = dateFormat.format(now);


        UserFileVO uploadVO = new UserFileVO();

        if ((suffix.equals("png")) || (suffix.equals("jpg")) || (suffix.equals("bmp")) || (suffix.equals("gif")) || (suffix.equals("jpeg"))) {
            uploadVO.setUfileType("图片");
            uploadVO.setUfilePath("/files/picture/000.jpg");

        } else if ((suffix.equals("rmvb")) || (suffix.equals("mp4")) || (suffix.equals("3gp")) || (suffix.equals("avi")) || (suffix.equals("wmv"))) {
            uploadVO.setUfileType("视频");
            uploadVO.setUfilePath("/files/picture/001.jpg");
        } else if ((suffix.equals("rmvb")) || (suffix.equals("mp4")) || (suffix.equals("3gp")) || (suffix.equals("avi")) || (suffix.equals("wmv"))) {
            uploadVO.setUfileType("音频");
            uploadVO.setUfilePath("/files/picture/002.png");
        } else {

            if (suffix.equals("excel")) {
                uploadVO.setUfilePath("/files/picture/003.png");
            } else if (suffix.equals("doc")) {
                uploadVO.setUfilePath("/files/picture/004.png");
            } else if (suffix.equals("ppt")) {
                uploadVO.setUfilePath("/files/picture/005.png");
            } else if (suffix.equals("pdf")) {
                uploadVO.setUfilePath("/files/picture/006.png");
            } else {
                uploadVO.setUfilePath("/files/picture/007.png");
            }
            uploadVO.setUfileType("其它");
        }

        uploadVO.setUserId(user.getUserId());
        uploadVO.setUfileId(id);
        uploadVO.setUfileName(fileName);
        uploadVO.setUfileKeyword(fileKeyword);
        uploadVO.setUfileAbstract(fileAbstrat);
        uploadVO.setUfileUrl(realUrl);
        uploadVO.setUfileTime(hehe);
        userFileDao.addFile(uploadVO);
        History history=new History();
        history.setUserId(user.getUserId());
        history.setFileId(id);
        userCenterDao.addUpload(history);
        System.out.println("*************************" + uploadVO.getUfileUrl());

    }


    //关联文件增加
    @Override
    public void addFileRelation(String[] fileId1, String[] fileId2,int userId) {
        userFileDao.addFileRelation(fileId1,fileId2,userId);
        userFileDao.addFileRelation(fileId2,fileId1,userId);
    }
    //关联文件的显示
    @Override
    public List<UserFileVO> showFileRelation(int fileId1, int userId) {
        return userFileDao.showFileRelation(fileId1,userId);
    }
    //得到文件by用户id
    @Override
    public List<UserFileVO> getfileListById(int userId ,int fileId1) {
        return userFileDao.getfileListById(userId,fileId1);
    }
    //删除文件关联
    @Override
    public void deleteFileRelation(String[] fileId1, String[] fileId2, int userId) {
        userFileDao.deleteFileRelation(fileId1,fileId2,userId);
        userFileDao.deleteFileRelation(fileId2,fileId1,userId);
    }
    //显示除了关联文件的文件
    @Override
    public List<UserFileVO> showFileRelationTest(int userId, int fileId1, String[] fileId2) {
        return userFileDao.showFileRelationTest(userId,fileId1,fileId2);
    }
    //得到某个文件的关联文件编号数组
    @Override
    public String[] getFileRelation(int userId,int fileId1) {
        return userFileDao.getFileRelation(userId,fileId1);
    }


    @Override
    public UserFileVO findFileById(int id) {
        return userFileDao.findFileById(id);
    }

    @Override
    public List<UserFileVO> getAllFileList(int userId) {
        return userFileDao.getAllFileList(userId);
    }

}
