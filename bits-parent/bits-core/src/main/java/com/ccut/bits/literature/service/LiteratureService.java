package com.ccut.bits.literature.service;

import com.ccut.bits.literature.model.Literature;
import com.ccut.bits.page.Page;

import java.util.List;

/**
 * Created by Demon on 2015/4/11.
 */
public interface LiteratureService {
    List<Literature> getAllLiterature(Page page);

    int getTotalRecord();

    int getTotalRecordByThemeId(int themeId);

    List<Literature> getLiteratureByThemeId(int themeId, Page page);

    Literature getLiteratureById(int id);

    List<Literature> getLiteraturesByIds(List<Integer> ids);

    List<Literature> getLiteratureListPage(Literature literature);

    void delLiterature(int id);

    void addLiterature(Literature literature);

    void editLiterature(Literature literature);

}
