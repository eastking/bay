package com.ccut.bits.autoanswer.utls;

/**
 * Created by qiutiandong on 2016/6/10 0010.
 */
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Date;

import com.google.gson.Gson;
import com.ccut.bits.autoanswer.model.ChatMessageBean;
import com.ccut.bits.autoanswer.model.ResultBean;
import com.ccut.bits.autoanswer.model.ChatMessageBean.Type;

/**
 * @author pop 此工具类为了向图灵中发送数据（msg），也是从图灵获取数据（result）
 */
public class HttpUtil {
    private final static String URL = "http://www.tuling123.com/openapi/api";
    private final static String KEY = "b0938ac308a96e669fc1800f6e1cfbf7";

    public static ChatMessageBean doChat(String msg) {
        ChatMessageBean chatBean = new ChatMessageBean();
        String jsonResult = doGet(msg);
        try {
            Gson gson = new Gson();
            ResultBean result = gson.fromJson(jsonResult, ResultBean.class);
            chatBean.setText(result.getText());
        } catch (Exception e) {
            chatBean.setText("服务器繁忙，请稍后");
        }
        chatBean.setDate(new Date());
        chatBean.setType(Type.COMING);
        return chatBean;
    }

    /**
     * 从图灵中获取返回值
     *
     * @param msg
     * @return
     */
    public static String doGet(String msg) {
        String result = null;
        String url = pjURL(msg);
        InputStream is = null;
        ByteArrayOutputStream baos = null;
        try {
            URL urlGet = new URL(url);
            HttpURLConnection coon = (HttpURLConnection) urlGet
                    .openConnection();
            coon.setRequestMethod("GET");
            coon.setReadTimeout(5000);
            coon.setConnectTimeout(5000);

            is = coon.getInputStream();
            baos = new ByteArrayOutputStream();
            byte[] buf = new byte[512];
            int len = -1;
            while ((len = is.read(buf)) != -1) {
                baos.write(buf, 0, len);
            }

            baos.flush();
            result = new String(baos.toByteArray(),"utf-8");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {

            if (is != null) {
                try {
                    is.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return result;
    }

    /**
     * 拼接url
     *
     * @param msg
     * @return
     */
    private static String pjURL(String msg) {
        try {
            msg = URLEncoder.encode(msg, "utf-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        String url = URL + "?key=" + KEY + "&info=" + msg;
        return url;
    }

    public static void main(String[] args) {
        String test =  doGet("我");
        System.out.println(test);
    }
}
