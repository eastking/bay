package com.ccut.bits.userFile.dao;

import com.ccut.bits.annotation.mybatisScan;

import com.ccut.bits.userFile.model.UserFileVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * Created by Administrator on 2016/6/6.
 */
@mybatisScan
public interface UserFileDao {
    List<UserFileVO> getUserArrangementListPage(UserFileVO userFileVO);

    List<UserFileVO> getUserArrangementImgListPage(UserFileVO userFileVO);

    List<UserFileVO> getUserArrangementVideoListPage(UserFileVO userFileVO);

    List<UserFileVO> getUserArrangementMusicListPage(UserFileVO userFileVO);

    List<UserFileVO> getUserArrangementOtherListPage(UserFileVO userFileVO);

    List<UserFileVO> getUserCommonListPage(UserFileVO userFileVO);

    UserFileVO findById(@Param("id") int id);

    void editFileName(UserFileVO userFileVO);

    void delArrangement(@Param("id") int id);

    void delArrangementImg(@Param("id") int id);

    void delArrangementMusic(@Param("id") int id);

    void delArrangementVideo(@Param("id") int id);

    void delArrangementOther(@Param("id") int id);

    void addCommon(UserFileVO userFileVO);

    List<UserFileVO> getFileNameById(@Param("id") int id);

    void addShare(UserFileVO userFileVO);

    List<UserFileVO> getNewKnowledge(@Param("size") int size);

    void addCollect(UserFileVO userFileVO);

    List<UserFileVO> getDelUserArrangementListPage(UserFileVO userFileVO);

    void delArrangementDus(@Param("id") int id);

    void addArrangement(UserFileVO userFileVO);

    void addFile(UserFileVO userFileVO);

    //增加文件关联
    void addFileRelation(@Param("fileId1") String[] fileId1,@Param("fileId2")  String[] fileId2,@Param("userId")  int userId);
    //关联文件的显示
    List<UserFileVO> showFileRelation(@Param("fileId1") int fileId1, @Param("userId") int userId);
    //得到文件by用户id
    List<UserFileVO> getfileListById(@Param("userId") int userId,@Param("fileId1") int fileId1);
    //删除文件关联
    void deleteFileRelation(@Param("fileId1")  String[] fileId1,@Param("fileId2")  String[] fileId2,@Param("userId")  int userId);
    //显示除了关联文件的文件
    List<UserFileVO> showFileRelationTest(@Param("userId") int userId,@Param("fileId1") int fileId1,@Param("fileId2") String[] fileId2);
    //得到某个文件的关联文件编号数组
    String[] getFileRelation(@Param("userId") int userId,@Param("fileId1") int fileId1);
    //得到文件通过文件id
    UserFileVO findFileById(@Param("id") int id);
    //得到所有的文件
    List<UserFileVO> getAllFileList(@Param("userId") int userId);

    List<UserFileVO> search(@Param("fsearch") String fsearch);


}
