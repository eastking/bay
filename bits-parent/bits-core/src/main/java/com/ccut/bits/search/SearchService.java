package com.ccut.bits.search;

import com.ccut.bits.expert.model.Expert;
import com.ccut.bits.files.entity.FileVO;
import com.ccut.bits.intelligence.model.Intelligence;
import com.ccut.bits.literature.model.Literature;
import com.ccut.bits.patent.model.Patent;
import com.ccut.bits.search.model.SearchRequest;
import com.ccut.bits.search.model.SearchResponse;

/**
 * Created by Demon on 2015/4/15.
 */
public interface SearchService {
    SearchResponse<Literature> searchLiterature(SearchRequest request);

    SearchResponse<Expert> searchExpert(SearchRequest request);

    SearchResponse<FileVO> searchFileVO(SearchRequest request);

    SearchResponse<Patent> searchPatent(SearchRequest request);

    SearchResponse<Intelligence> searchGlobal(SearchRequest request);
}
