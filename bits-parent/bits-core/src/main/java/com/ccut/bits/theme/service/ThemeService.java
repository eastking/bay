package com.ccut.bits.theme.service;

import com.ccut.bits.theme.model.Theme;
import com.ccut.bits.theme.model.ThemeTree;

import java.util.List;
import java.util.Map;

/**
 * Created by Demon on 2015/4/18.
 */
public interface ThemeService {
    List<ThemeTree> getThemeTree();

    List<String> getThemeNames();

    List<Theme> getCacheThemeNames();

    List<Theme> getAllThemeNames();

    List<Theme> getTopTheme();

    Map<String,Integer> getChartData();

    Map<String,Integer> getDetailThemeChartData(Integer themeId);

    List<Integer> getAllThemeId();

    void updateThemeHotScore();
}
