package com.ccut.bits.autoanswer.model;

import java.util.Date;

/**
 * Created by qiutiandong on 2016/6/10 0010.
 */
public class ChatMessageBean {
    private String name;
    private String text;
    private Date date;
    private Type type;

    public ChatMessageBean() {
    }

    public ChatMessageBean(String text, Date date, Type type) {
        this.text = text;
        this.date = date;
        this.type = type;
    }


    public enum Type {
        OUTING, COMING
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }
}
