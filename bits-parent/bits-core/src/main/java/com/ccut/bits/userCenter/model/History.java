package com.ccut.bits.userCenter.model;

import com.ccut.bits.expert.model.Expert;
import com.ccut.bits.page.PageBase;
import com.ccut.bits.theme.model.Theme;

/**
 * 类的描述信息
 *
 * @author longshihui
 * @version 1.0.1
 */
public class History extends PageBase {
    private int id;
    private int userId;
    private Expert expert;
    private String time;
    private String filepath;
    private String downloadtime;
    private String uploadtime;
    private String filename;
    private String findtime;
    private String filetime;
    private String filetype;
    private int commonId;
    private String checktime;
    private String ufileName;
    private String ufilePath;
    private String ufileUrl;
    private String fileId;
    private String fileUrl;
    public String getUfileUrl() {
        return ufileUrl;
    }

    public void setUfileUrl(String ufileUrl) {
        this.ufileUrl = ufileUrl;
    }

    public String getUfileName() {
        return ufileName;
    }

    public void setUfileName(String ufileName) {
        this.ufileName = ufileName;
    }

    public String getUfilePath() {
        return ufilePath;
    }

    public void setUfilePath(String ufilePath) {
        this.ufilePath = ufilePath;
    }

    public String getFileId() {
        return fileId;
    }

    public void setFileId(String fileId) {
        this.fileId = fileId;
    }

    public String getFileUrl() {
        return fileUrl;
    }

    public void setFileUrl(String fileUrl) {
        this.fileUrl = fileUrl;
    }

    public String getChecktime() {
        return checktime;
    }

    public void setChecktime(String checktime) {
        this.checktime = checktime;
    }

    public int getCommonId() {
        return commonId;
    }

    public void setCommonId(int commonId) {
        this.commonId = commonId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public Expert getExpert() {
        return expert;
    }

    public void setExpert(Expert expert) {
        this.expert = expert;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getFilepath()   {   return filepath;}

    public void setFilepath(String filepath) {  this.filepath=filepath;}

    public String getDownloadtime() {   return downloadtime;}

    public void setDownloadtime(String downloadtime)   {   this.downloadtime=downloadtime;}

    public String getFilename() {   return filename;}

    public void setFilename(String filename)   {   this.filename=filename;}

    public String getUploadtime() { return uploadtime;}

    public void  setUploadtime(String uploadtime)    {   this.uploadtime=uploadtime;}

    public void setFindtime(String findtime) {   this.findtime=findtime;}

    public String getFindtime() {   return findtime;}

    public String getFiletime()   {   return filetime;}

    public void setFiletime(String filetime) {  this.filetime=filetime;}

    public String getFiletype()   {   return filetype;}

    public void setFiletype(String filetype) {  this.filetype=filetype;}


}
