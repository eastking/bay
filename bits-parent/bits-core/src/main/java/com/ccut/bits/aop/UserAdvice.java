package com.ccut.bits.aop;

import com.ccut.bits.model.User;
import com.ccut.bits.security.service.UserService;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpSession;

/**
 * Created by Dintama on 2015/8/9.
 */
@Aspect
public class UserAdvice {

    @Autowired
    private UserService userService;

    @Pointcut("execution(* getExpertById*(..))")
    private void addAddMethod(){
        System.out.println("测试：并不会被调用输出");
    }

    @Before("addAddMethod()")
    private void beforeExecute(JoinPoint point){
        HttpSession session = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest().getSession();

        User user = (User)session.getAttribute("User");
        if(user == null){
            System.out.println("该用户尚未登录");
            return;
        }
        Object[] args = point.getArgs();
        userService.addUserHistory(user.getUserId(), Integer.parseInt(args[0].toString()));
        System.out.println("记录用户浏览信息……");
    }

    @After("addAddMethod()")
    private void afterExecute(){
        System.out.println("完成。");
    }


}
