package com.ccut.bits.search;

import com.ccut.bits.constant.Constant;
import com.ccut.bits.expert.dao.ExpertDao;
import com.ccut.bits.expert.model.Expert;
import com.ccut.bits.files.entity.FileVO;
import com.ccut.bits.intelligence.model.Intelligence;
import com.ccut.bits.literature.model.Literature;
import com.ccut.bits.lucene.LuceneSearch;
import com.ccut.bits.patent.model.Patent;
import com.ccut.bits.relation.ExpertRelation;
import com.ccut.bits.relation.ThemeRelation;
import com.ccut.bits.search.model.SearchRequest;
import com.ccut.bits.search.model.SearchResponse;
import com.ccut.bits.theme.model.Theme;
import com.ccut.bits.util.HighlightUtil;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.Term;
import org.apache.lucene.queryparser.classic.MultiFieldQueryParser;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.Filter;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.QueryWrapperFilter;
import org.apache.lucene.search.TermQuery;
import org.apache.lucene.util.Version;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.wltea.analyzer.lucene.IKAnalyzer;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by Demon on 2015/4/15.
 */
@Service("searchService")
public class SearchServiceImp implements SearchService {
    @Autowired
    private LuceneSearch luceneSearch;
    @Autowired
    private ExpertRelation expertRelation;
    @Autowired
    private ThemeRelation themeRelation;
    @Autowired
    private ExpertDao expertDao;

    @Override
    public SearchResponse<Literature> searchLiterature(SearchRequest request) {
        luceneSearch.setPage(request.getPage());
        String[] fields = {
                Constant.Index.Literature.FIELD_TITLE,
                Constant.Index.Literature.FIELD_ABS,
                Constant.Index.Literature.FIELD_KEY
        };
        QueryParser parser = new MultiFieldQueryParser(Version.LUCENE_47, fields, new IKAnalyzer());
        Query query = null;
        try {
            query = parser.parse(request.getKey());
        } catch (ParseException e) {
            System.out.println(e.toString());
        }
        Filter filter = new QueryWrapperFilter(new TermQuery(new Term(Constant.Index.CLASS, Constant.Index.LITERATURE)));
        List<Document> docs = luceneSearch.search(query, filter);
        List<Literature> result = new LinkedList<>();
        Literature literature = null;
        for (Document doc : docs) {
            literature = new Literature();
            literature.setId(Integer.parseInt(doc.get(Constant.Index.Literature.FIELD_ID)));
            literature.setTitle(doc.get(Constant.Index.Literature.FIELD_TITLE));
            literature.setAbs(doc.get(Constant.Index.Literature.FIELD_ABS));
            literature.setAppDate(doc.get(Constant.Index.Literature.FIELD_DATE));
            literature.setAuthorCn(doc.get(Constant.Index.Literature.FIELD_AUTHOR));
            result.add(literature);
        }

        for (Literature l : result) {
            l.setTitle(HighlightUtil.getHighlighterString(query, Constant.Index.Literature.FIELD_TITLE, l.getTitle()));
            l.setAbs(HighlightUtil.getHighlighterString(query, Constant.Index.Literature.FIELD_ABS, l.getAbs()));
            l.setAuthorCn(HighlightUtil.getHighlighterString(query, Constant.Index.Literature.FIELD_AUTHOR, l.getAuthorCn()));
        }
        return new SearchResponse<Literature>(result, luceneSearch.getTotalHits());
    }



    @Override
    public SearchResponse<FileVO> searchFileVO(SearchRequest request) {
        luceneSearch.setPage(request.getPage());
        String[] fields = {
                Constant.Index.FileVO.FIELD_KEY,
        };
        Query query = null;
        try {
            query = new MultiFieldQueryParser(Version.LUCENE_47, fields, new IKAnalyzer()).parse(request.getKey());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Filter filter = new QueryWrapperFilter(new TermQuery(new Term(Constant.Index.CLASS, Constant.Index.FILEVO)));
        List<Document> docs = luceneSearch.search(query,null);

        List<FileVO> result = new LinkedList<>();
        FileVO fileVO = null;
        for (Document doc : docs) {
            fileVO = new FileVO();
            fileVO.setFileName(doc.get(Constant.Index.FileVO.FIELD_NAME));
            fileVO.setFileKeyword(doc.get(Constant.Index.FileVO.FIELD_KEY));
            fileVO.setFileId(doc.get(Constant.Index.FileVO.FIELD_ID));
            fileVO.setFileUrl(doc.get(Constant.Index.FileVO.FIELD_URL));
            fileVO.setFileTime(doc.get(Constant.Index.FileVO.FIELD_DATE));
            result.add(fileVO);
        }

        for (FileVO p : result) {
//            p.setFileName(HighlightUtil.getHighlighterString(query, Constant.Index.FileVO.FIELD_NAME, p.getFileName()));
            p.setFileKeyword(HighlightUtil.getHighlighterString(query, Constant.Index.FileVO.FIELD_KEY, p.getFileKeyword()));
//            p.setFileTime(HighlightUtil.getHighlighterString(query, Constant.Index.FileVO.FIELD_DATE, p.getFileAbstrat()));
       }

        return new SearchResponse<FileVO>(result, luceneSearch.getTotalHits());
    }

    @Override
    public SearchResponse<Patent> searchPatent(SearchRequest request) {
        luceneSearch.setPage(request.getPage());
        String[] fields = {
                Constant.Index.Patent.FIELD_TITLE,
                Constant.Index.Patent.FIELD_ABS,
        };
        Query query = null;
        try {
            query = new MultiFieldQueryParser(Version.LUCENE_47, fields, new IKAnalyzer()).parse(request.getKey());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Filter filter = new QueryWrapperFilter(new TermQuery(new Term(Constant.Index.CLASS, Constant.Index.PATENT)));

        List<Document> docs = luceneSearch.search(query, filter);

        List<Patent> result = new LinkedList<>();
        Patent patent = null;
        for (Document doc : docs) {
            patent = new Patent();
            patent.setId(Integer.parseInt(doc.get(Constant.Index.Patent.FIELD_ID)));
            patent.setAbs(doc.get(Constant.Index.Patent.FIELD_ABS));
            patent.setInventor(doc.get(Constant.Index.Patent.FIELD_INVENTOR));
            patent.setApplicant(doc.get(Constant.Index.Patent.FIELD_APPLICANT));
            patent.setTitle(doc.get(Constant.Index.Patent.FIELD_TITLE));
            patent.setDate(doc.get(Constant.Index.Patent.FIELD_DATE));
            result.add(patent);
        }

        for (Patent p : result) {
            p.setTitle(HighlightUtil.getHighlighterString(query, Constant.Index.Patent.FIELD_TITLE, p.getTitle()));
            p.setAbs(HighlightUtil.getHighlighterString(query, Constant.Index.Patent.FIELD_ABS, p.getAbs()));
        }

        return new SearchResponse<Patent>(result, luceneSearch.getTotalHits());
    }


    @Override
    public SearchResponse<Expert> searchExpert(SearchRequest request) {
        luceneSearch.setPage(request.getPage());
        Query query = null;
        try {
            query = new QueryParser(Version.LUCENE_47,request.getField(),new IKAnalyzer()).parse(request.getKey());
        } catch (ParseException e) {
            System.out.println(e.toString());
        }
        Filter filter = new QueryWrapperFilter(new TermQuery(new Term(Constant.Index.CLASS, Constant.Index.EXPERT)));
        List<Document> docs = luceneSearch.search(query, filter);

        List<Expert> result = new LinkedList<>();
        Expert expert = null;
        List<Integer> themeIds;
        for (Document doc : docs) {
            expert = expertDao.getExpertById(Integer.parseInt(doc.get(Constant.Index.Expert.FIELD_ID)));
            themeIds = new LinkedList<>();
            themeIds.addAll(expertRelation.getThemeIds(expert.getId()));
            expert.setThemeList((List<Theme>)themeRelation.getThemes(themeIds));
            result.add(expert);
        }
        switch (request.getField()) {
            case Constant.Index.Expert.FIELD_NAME:
                for (Expert e : result) {
                    e.setExpertName(HighlightUtil.getHighlighterString(query, Constant.Index.Expert.FIELD_NAME, e.getExpertName()));
                }
                break;
            case Constant.Index.Expert.FIELD_ORGANIZATION:
                for (Expert e : result) {
                    e.setExpertOrg(HighlightUtil.getHighlighterString(query, Constant.Index.Expert.FIELD_ORGANIZATION, e.getExpertOrg()));
                }
                break;
        }
        return new SearchResponse<Expert>(result, luceneSearch.getTotalHits());
    }


    @Override
    public SearchResponse<Intelligence> searchGlobal(SearchRequest request) {
        luceneSearch.setPage(request.getPage());
        String[] fields = new String[]{Constant.Index.Intelligence.FIELD_TITLE, Constant.Index.Intelligence.FIELD_ABS};
        Query query = null;
        try {
            query = new MultiFieldQueryParser(Version.LUCENE_47, fields, new IKAnalyzer()).parse(request.getKey());
        } catch (ParseException e) {
            System.out.println(e.toString());
        }
        Filter filter = new QueryWrapperFilter(new TermQuery(new Term(Constant.Index.CLASS, Constant.Index.INTELLIGENCE)));
        List<Document> docs = luceneSearch.search(query, filter);
        List<Intelligence> result = new LinkedList<>();
        Intelligence intel = null;
        for (Document doc : docs) {
            intel = new Intelligence();
            intel.setTitle(doc.get(Constant.Index.Intelligence.FIELD_TITLE));
            intel.setUrl(doc.get(Constant.Index.Intelligence.FIELD_URL));
            intel.setAbs(doc.get(Constant.Index.Intelligence.FIELD_ABS));
            intel.setDate(doc.get(Constant.Index.Intelligence.FIELD_DATE));
            result.add(intel);
        }

        for (Intelligence i : result) {
            i.setTitle(HighlightUtil.getHighlighterString(query, fields[0], i.getTitle()));
            i.setAbs(HighlightUtil.getHighlighterString(query, fields[1], i.getAbs()));
        }
        return new SearchResponse<Intelligence>(result, luceneSearch.getTotalHits());
    }

}
