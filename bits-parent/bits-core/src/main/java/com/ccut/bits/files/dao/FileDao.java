package com.ccut.bits.files.dao;


import com.ccut.bits.annotation.mybatisScan;
import com.ccut.bits.files.entity.FileVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * Created by Administrator on 2016/3/30.
 */
@mybatisScan
public interface FileDao {

    List<FileVO>  getALLfile(@Param("begin") Integer begin, @Param("size") Integer size);

    List<FileVO> getFileInfoList(FileVO fileVO);

    void addUploadAttachment(FileVO fileVO);

    List<FileVO> getUploadAttachmentList(int userId);

    void addFileInfo(FileVO fileVO);

    int checkFileId(FileVO fileVO);

    FileVO findFileById(@Param("id") int id);

    void deleteManyFileInfo(@Param("fileIds") String[] fileIds);

    List<FileVO> getPictureThemeId(FileVO fileVO);

    List<FileVO> getVideoThemeId(FileVO fileVO);

    List<FileVO> getMusicThemeId(FileVO fileVO);

    List<FileVO> getFileThemeId(FileVO fileVO);

    FileVO showFileInfo(String fileId);

    void editFileVO(FileVO fileVO);

    void  addFileInterest(FileVO fileVO);
    List<FileVO> search(@Param("fsearch") String fsearch);

    List<FileVO> searchInterest(FileVO fileVO);

    List<FileVO> search111(FileVO fileVO);

    List<FileVO> search110(FileVO fileVO);

    List<FileVO> search101(FileVO fileVO);

    List<FileVO> search100(FileVO fileVO);

    List<FileVO> search011(FileVO fileVO);

    List<FileVO> search010(FileVO fileVO);

    List<FileVO> search001(FileVO fileVO);

    List<FileVO> search000(FileVO fileVO);
}
