package com.ccut.bits.literature.service;

import com.ccut.bits.literature.dao.LiteratureDao;
import com.ccut.bits.literature.model.Literature;
import com.ccut.bits.page.Page;
import com.ccut.bits.relation.ThemeRelation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by Demon on 2015/4/11.
 */
@Service("literatureService")
public class LiteratureServiceImp implements LiteratureService {
    @Autowired
    private LiteratureDao literatureDao;
    @Autowired
    private ThemeRelation themeRelation;
    private Map<Integer,Map<String,Set<Integer>>> relationMap;

    @Override
    public List<Literature> getAllLiterature(Page page) {
        int size = page.getPageSize();
        int begin = (page.getPageNo() - 1) * page.getPageSize();
        return literatureDao.getAllLiterature(begin,size);
    }

    @Override
    public int getTotalRecord() {
        return literatureDao.getTotalRecord();
    }

    @Override
    public int getTotalRecordByThemeId(int themeId) {
        return themeRelation.totalLiterature(themeId);
    }

    @Override
    public List<Literature> getLiteratureByThemeId(int themeId, Page page) {
        int begin = (page.getPageNo() - 1) * page.getPageSize();
        return literatureDao.getLiteratureByThemeId(themeId,begin,page.getPageSize());
    }

    @Override
    public List<Literature> getLiteraturesByIds(List<Integer> ids) {
        if (ids.isEmpty()) return new LinkedList<>();
        return literatureDao.getLiteraturesByIds(ids);
    }

    @Override
    public List<Literature> getLiteratureListPage(Literature literature) {
        return literatureDao.getLiteratureListPage(literature);
    }

    @Override
    public void delLiterature(int id) {
        literatureDao.delLiterature(id);
    }

    @Override
    public void addLiterature(Literature literature) {
        literatureDao.addLiterature(literature);
    }

    @Override
    public void editLiterature(Literature literature) {
        literatureDao.editLiterature(literature);
    }

    @Override
    public Literature getLiteratureById(int id) {
        return literatureDao.getLiteratureById(id);
    }

}
