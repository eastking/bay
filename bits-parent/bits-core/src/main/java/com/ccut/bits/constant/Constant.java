package com.ccut.bits.constant;

/**
 * Created by Demon on 2015/5/15.
 */
public class Constant {
    public static boolean YES = true;
    public static boolean NO = false;

    public static class Page {
        public static final Integer SUGGEST_QUERY_SIZE = 300;
        public static final Integer DEFAULT_PAGE_INDEX = 1;
        public static final Integer DEFAULT_PAGE_SIZE = 6;
        public static final int FIRST_PAGE = 1;
        public static final String UTF_8 = "UTF-8";
        public static final String ISO_8859_1 = "ISO-8859-1";
        public static final String PAGE_NO = "pageNo";
        public static final String PAGE_SIZE = "pageSize";
    }

    public static class Search {
        public static final String KEY = "key";
        public static final String FIELD = "field";

        public static class DIRECTION {
            public static String LITERATURE = "literature";
            public static String PATENT = "patent";
            public static String EXPERT = "expert";
            public static String LITERATURE_CN = "文献";
            public static String PATENT_CN = "专利";
        }
    }




    public static class Index {
        public static final String LITERATURE = "literature";
        public static final String PATENT = "patent";
        public static final String EXPERT = "expert";
        public static final String FILEVO = "fileVO";
        public static final String CLASS = "class";
        public static final String INTELLIGENCE = "Intelligence";
        public static final String INDEX_PATH = "d:\\Index";


        public static class Literature {
            public static final String FIELD_ID = "id";
            public static final String FIELD_TITLE = "title";
            public static final String FIELD_ABS = "abs";
            public static final String FIELD_AUTHOR = "author";
            public static final String FIELD_JOURNAL = "journal";
            public static final String FIELD_KEY = "key";
            public static final String FIELD_UNIT = "unit";
            public static final String FIELD_DATE = "date";
            public static final String[] ALL_FIELD = new String[]{
                    Literature.FIELD_ID,
                    Literature.FIELD_ABS,
                    Literature.FIELD_KEY,
                    Literature.FIELD_TITLE,
                    Literature.FIELD_AUTHOR,
                    Literature.FIELD_DATE,
                    Literature.FIELD_JOURNAL,
                    Literature.FIELD_UNIT

            };
        }

        public static class FileVO{
            public static final String FIELD_ID = "id";
            public static final String FIELD_NAME = "name";
            public static final String FIELD_URL = "url";
            public static final String FIELD_DATE = "data";
            public static final String FIELD_KEY = "key";
            public static final String[] ALL_FILE = new String[]{
                    FileVO.FIELD_ID,
                    FileVO.FIELD_NAME,
                    FileVO.FIELD_URL,
                    FileVO.FIELD_DATE,
                    FileVO.FIELD_KEY
            };

        }

        public static class Patent {
            public static final String FIELD_ID = "id";
            public static final String FIELD_TITLE = "title";
            public static final String FIELD_ABS = "abs";
            public static final String FIELD_APPLICANT = "applicant";
            public static final String FIELD_INVENTOR = "inventor";
            public static final String FIELD_DATE = "date";
            public static final String[] ALL_FIELD = new String[]{
                    Patent.FIELD_ID,
                    Patent.FIELD_ABS,
                    Patent.FIELD_APPLICANT,
                    Patent.FIELD_DATE,
                    Patent.FIELD_INVENTOR,
                    Patent.FIELD_TITLE
            };
        }

        public static class Expert {
            public static final String FIELD_ID = "id";
            public static final String FIELD_NAME = "name";
            public static final String FIELD_ORGANIZATION = "organization";
            public static final String[] ALL_FIELD = new String[]{
                    Expert.FIELD_ID,
                    Expert.FIELD_NAME,
                    Expert.FIELD_ORGANIZATION
            };
        }




        public static class Intelligence {
            public static final String FIELD_ID = "id";
            public static final String FIELD_TITLE = "title";
            public static final String FIELD_URL = "url";
            public static final String FIELD_DATE = "data";
            public static final String FIELD_ABS = "abs";
        }

        public static class Theme {
            public static final String FIELD_ID = "id";
            public static final String FIELD_THEME_NAME = "themeName";
            public static final String FIELD_KEY_WORD = "keyword";
        }

    }
}
