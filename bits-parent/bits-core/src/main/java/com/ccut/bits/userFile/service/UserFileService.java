package com.ccut.bits.userFile.service;

import com.ccut.bits.userFile.model.UserFileVO;
import org.apache.commons.fileupload.FileUploadException;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;


/**
 * Created by Administrator on 2016/6/6.
 */
public interface UserFileService {
    List<UserFileVO> getUserArrangementListPage(UserFileVO userFileVO);

    List<UserFileVO> getUserArrangementImgListPage(UserFileVO userFileVO);

    List<UserFileVO> getUserArrangementMusicListPage(UserFileVO userFileVO);

    List<UserFileVO> getUserArrangementVideoListPage(UserFileVO userFileVO);

    List<UserFileVO> getUserArrangementOtherListPage(UserFileVO userFileVO);

    List<UserFileVO> getUserCommonListPage(UserFileVO userFileVO);

    UserFileVO findById(int id);

    void editFileName(UserFileVO userFileVO);

    void delArrangement(int id);

    void delArrangementImg(int id);

    void delArrangementMusic(int id);

    void delArrangementVideo(int id);

    void delArrangementOther(int id);

    void addCommon(UserFileVO userFileVO);

    List<UserFileVO> getFileNameById(int id);

    void addShare(UserFileVO userFileVO);

    List<UserFileVO> getNewKnowledge();

    void addCollect(UserFileVO userFileVO);

    List<UserFileVO> getDelUserArrangementListPage(UserFileVO userFileVO);

    void delArrangementDus(int id);

    void addArrangement(UserFileVO userFileVO);

      //上传
    void uploadFile(MultipartFile file, HttpServletRequest request, HttpSession session, HttpServletResponse response, int i) throws IOException, FileUploadException;

    //关联文件增加
    void addFileRelation( String[] fileId1,String[] fileId2,int userId);
    //关联文件的显示
    List<UserFileVO> showFileRelation(int fileId1,int userId);
    //得到文件by用户id
    List<UserFileVO> getfileListById( int userId,int fileId1);
    //删除文件关联
    void deleteFileRelation(String[] fileId1,String[] fileId2,int userId);
    //显示除了关联文件的文件
    List<UserFileVO> showFileRelationTest(int userId, int fileId1,String[]fileId2);

    //得到某个文件的关联文件编号数组
    String[] getFileRelation(int userId, int fileId1);
    //得到文件通过id
    UserFileVO findFileById(int id);

    List<UserFileVO> getAllFileList(int userId);

    List<UserFileVO> search(String fsearch);

}
