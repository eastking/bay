package com.ccut.bits.files.service;


import com.ccut.bits.files.entity.FileVO;
import com.ccut.bits.page.Page;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Created by Administrator on 2016/3/30.
 */
public interface FileService {
    List<FileVO> getALLfile(Page page);

    List<FileVO> getFileInfoList(FileVO fileVO);

   void checkFileId(FileVO fileVO);

    void deleteManyFileInfo(String[] fileIds);

    FileVO findFileById(int id);

    List<FileVO> getPictureThemeId(FileVO fileVO);

    List<FileVO> getVideoThemeId(FileVO fileVO);

    List<FileVO> getMusicThemeId(FileVO fileVO);

    List<FileVO> getFileThemeId(FileVO fileVO);

    void editFileVO(FileVO fileVO);

    List<FileVO> search(String fsearch);

    void downFile(String serverUrl, HttpServletRequest request, HttpServletResponse response);

    void addFileInterest(FileVO fileVO);

    String previewFile(String serverUrl, HttpServletRequest request, HttpServletResponse response) throws IOException;

    List<FileVO> searchInterest(FileVO fileVO);


    List<FileVO> search111(FileVO fileVO);

    List<FileVO> search110(FileVO fileVO);

    List<FileVO> search100(FileVO fileVO);

    List<FileVO> search101(FileVO fileVO);

    List<FileVO> search011(FileVO fileVO);

    List<FileVO> search010(FileVO fileVO);

    List<FileVO> search001(FileVO fileVO);

    List<FileVO> search000(FileVO fileVO);
}
