package com.ccut.bits.files.service;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.ccut.bits.files.dao.FileDao;
import com.ccut.bits.files.entity.FileVO;
import com.ccut.bits.page.Page;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.List;


/**
 * Created by Administrator on 2016/3/30.
 */
@Service("FileService")
@Transactional
public class FileServiceImpl implements FileService {

    @Autowired
    private FileDao fileDao;

    @Override
    public List<FileVO> getALLfile(Page page) {
        Integer size = page.getPageSize();
        Integer begin = (page.getPageNo() - 1) * page.getPageSize();
        List<FileVO> files = fileDao.getALLfile(begin, size);
        return fileDao.getALLfile(begin, size);
    }


    @Override
    public List<FileVO> getFileInfoList(FileVO fileVO) {
        return fileDao.getFileInfoList(fileVO);
    }

//    @Override
//    public List<FileVO> getFileNameById(int id) {
//        return fileDao.getFileNameById(id);
//    }

    //    @Override
//    public void addFileInfo(FileVO fileVO) {
//        checkFileId(fileVO);
//        fileDao.addFileInfo(fileVO);
//    }
    @Override
    public void checkFileId(FileVO fileVO) {
        if (fileDao.checkFileId(fileVO) > 0) {
            //   throw new ValidateErrorException("账号已存在");
        }
    }

    /**
     * Query Student Information
     */
//    @Override
//    public FileVO showFileInfo(String fileId) {
//        return fileDao.showFileInfo(fileId);
//    }

    /**
     * Delete Student Information
     */
    @Override
    public void deleteManyFileInfo(String[] FileIds) {

        fileDao.deleteManyFileInfo(FileIds);

        //fileDao.deleteManyFileCourseInfo(FileIds);
    }

    @Override
    public FileVO findFileById(int id) {
        return fileDao.findFileById(id);
    }

    @Override
    public List<FileVO> getPictureThemeId(FileVO fileVO) {
        return fileDao.getPictureThemeId(fileVO);
    }

    @Override
    public List<FileVO> getVideoThemeId(FileVO fileVO) {
        return fileDao.getVideoThemeId(fileVO);
    }


    @Override
    public List<FileVO> getMusicThemeId(FileVO fileVO) {
        return fileDao.getMusicThemeId(fileVO);
    }

    @Override
    public List<FileVO> getFileThemeId(FileVO fileVO) {
        return fileDao.getFileThemeId(fileVO);
    }

    @Override
    public List<FileVO> searchInterest(FileVO fileVO) {
        return fileDao.searchInterest(fileVO);
    }

    @Override
    public void editFileVO(FileVO fileVO) {
        fileDao.editFileVO(fileVO);
    }

    @Override
    public void addFileInterest(FileVO fileVO) {
        fileDao.addFileInterest(fileVO);
    }

    @Override
    public List<FileVO> search(String fsearch) {
        return fileDao.search(fsearch);
    }

    @Override
    public List<FileVO> search111(FileVO fileVO) {
        return fileDao.search111(fileVO);
    }

    @Override
    public List<FileVO> search110(FileVO fileVO) {
        return fileDao.search110(fileVO);
    }

    @Override
    public List<FileVO> search100(FileVO fileVO) {
        return fileDao.search100(fileVO);
    }

    @Override
    public List<FileVO> search101(FileVO fileVO) {
        return fileDao.search101(fileVO);
    }

    @Override
    public List<FileVO> search011(FileVO fileVO) {
        return fileDao.search011(fileVO);
    }

    @Override
    public List<FileVO> search010(FileVO fileVO) {
        return fileDao.search010(fileVO);
    }

    @Override
    public List<FileVO> search001(FileVO fileVO) {
        return fileDao.search001(fileVO);
    }

    @Override
    public List<FileVO> search000(FileVO fileVO) {
        return fileDao.search000(fileVO);
    }

    @Override
    public void downFile(String serverUrl, HttpServletRequest request, HttpServletResponse response) {
        try {
            String path = request.getContextPath();

            String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
            String imgUrl = basePath + serverUrl;
            response.reset();
            response.addHeader("Content-Disposition", "attachment;filename=" + new String(URLEncoder.encode(serverUrl.split("/")[1], "UTF-8").getBytes()));
            response.addHeader("Content-Type", "text/plain");
            URL url = new URL(imgUrl);
            URLConnection conn = url.openConnection();
            InputStream inStream = conn.getInputStream();
            OutputStream out = response.getOutputStream();
            int byteread = 0;
            byte[] buffer = new byte[1024];
            while ((byteread = inStream.read(buffer)) != -1) {
                out.write(buffer, 0, byteread);
            }
            out.flush();
            out.close();
            inStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public String previewFile(String serverUrl, HttpServletRequest request, HttpServletResponse response)throws IOException{

        String path = request.getSession().getServletContext().getRealPath("/");
        String  url = path + serverUrl;
        CloseableHttpClient httpclient = HttpClients.createDefault();
        String  paths;
        String fileurl;
        try {
            //HttpPost httppost = new HttpPost("http://view.webofficeapi.com/upload");
            HttpPost httppost = new HttpPost("http://api.idocv.com/doc/upload");


            File ff = new File(url);
            //StringBody apiKeyPart = new StringBody(apiKey, org.apache.http.entity.ContentType.TEXT_PLAIN);
            FileBody filePart = new FileBody(ff);

            StringBody testtoken = new StringBody("testtoken", org.apache.http.entity.ContentType.TEXT_PLAIN);
            StringBody public1 = new StringBody("public", org.apache.http.entity.ContentType.TEXT_PLAIN);

            HttpEntity reqEntity = MultipartEntityBuilder.create()
                    .addPart("token", testtoken)
                    .addPart("file",filePart)
                    .addPart("mode ",public1)
                    .build();

            httppost.setEntity(reqEntity);

            System.out.println("executing request " + httppost.getRequestLine());
            CloseableHttpResponse returnResponse = httpclient.execute(httppost);



            try {
                System.out.println("----------------------------------------");
                System.out.println(returnResponse.getStatusLine());
                HttpEntity resEntity = returnResponse.getEntity();
                if (resEntity != null) {
                    System.out.println("Response content length: " + resEntity.getContentLength());
                }
                paths=EntityUtils.toString(resEntity);

                JSONObject ob = JSON.parseObject(paths);
                String uuid = ob.getString("uuid");

                fileurl="http://api.idocv.com/view/"+uuid;

                System.out.println("++++++++++++++++++++++"+uuid);
            } finally {
                returnResponse.close();
            }
        } finally {
            httpclient.close();
        }
          return fileurl;


    }
}