package com.ccut.bits.literature.dao;

import com.ccut.bits.annotation.mybatisScan;
import com.ccut.bits.literature.model.Literature;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * Created by Demon on 2015/4/11.
 */
@mybatisScan
public interface LiteratureDao {
    List<Literature> getAllLiterature(@Param("begin") Integer begin, @Param("size") Integer size);

    int getTotalRecord();

    int getTotalRecordByThemeId(@Param("themeId") int themeId);

    List<Literature> getLiteratureByThemeId(@Param("themeId") int themeId, @Param("begin") int begin, @Param("size") int size);

    Literature getLiteratureById(@Param("id") int id);

    List<Literature> getLiteraturesByIds(@Param("ids") List<Integer> ids);

    int countLiteratureByExpertId(@Param("id") int id);

    List<Integer> getLiteratureIdsByExpertId(@Param("id") int id);

    List<Literature> getLiteratureListPage(Literature literature);

    void delLiterature(@Param("id") int id);

    void addLiterature(Literature literature);

    void editLiterature(Literature literature);
}
