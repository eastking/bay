package com.ccut.bits.literature.model;

import com.ccut.bits.page.PageBase;

/**
 * Created by Demon on 2015/4/11.
 */
public class Literature extends PageBase {
    private int id;
    private String abs;
    private String authorCn;
    private String unit;
    private String appDate;
    private String key;
    private String title;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAbs() {
        return abs;
    }

    public void setAbs(String abs) {
        this.abs = abs;
    }

    public String getAuthorCn() {
        return authorCn;
    }

    public void setAuthorCn(String authorCn) {
        this.authorCn = authorCn;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getAppDate() {
        return appDate;
    }

    public void setAppDate(String appDate) {
        this.appDate = appDate;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

}
