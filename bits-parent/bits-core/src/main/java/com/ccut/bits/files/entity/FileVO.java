package com.ccut.bits.files.entity;


import com.ccut.bits.page.PageBase;


/**
 * Created by Administrator on 2016/3/30.
 */
public class FileVO extends PageBase {

    private String fileSize;

    public String getFileSuum() {
        return fileSuum;
    }

    public void setFileSuum(String fileSuum) {
        this.fileSuum = fileSuum;
    }

    private String fileSuum;

    private String fileName;

    private String fileType;

    private String filePath;

    private  String userName2;

    private String fileId;

    private int userId;

    private int ThemeId;

    public void setThemeId(int themeId) {
        ThemeId = themeId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public Integer getThemeId() {
        return themeId;
    }

    public void setThemeId(Integer themeId) {
        this.themeId = themeId;
    }

    public String getFileUrl() {
        return fileUrl;
    }

    public void setFileUrl(String fileUrl) {
        this.fileUrl = fileUrl;
    }

    private String fileUrl;
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    private int id;

    public String getFileAbstrat() {
        return fileAbstrat;
    }

    public void setFileAbstrat(String fileAbstrat) {
        this.fileAbstrat = fileAbstrat;
    }

    private String fileAbstrat;

    public String getFileTime() {
        return fileTime;
    }

    public void setFileTime(String fileTime) {
        this.fileTime = fileTime;
    }

    private String fileTime;




    public String getFileKeyword() {
        return fileKeyword;
    }

    public void setFileKeyword(String fileKeyword) {
        this.fileKeyword = fileKeyword;
    }

    private String fileKeyword;




    public Integer getTheme() {
        return themeId;
    }

    public void setTheme(Integer themeId) {
        this.themeId = themeId;
    }

    private Integer themeId;




    public String getFileId() {
        return fileId;
    }

    public void setFileId(String fileId) {
        this.fileId = fileId;
    }



    public String getFileSize() {
        return fileSize;
    }

    public void setFileSize(String fileSize) {
        this.fileSize = fileSize;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }





    public String getUserName2() {
        return userName2;
    }

    public void setUserName2(String userName2) {
        this.userName2 = userName2;
    }








}
