package com.ccut.bits.userFile.model;

import com.ccut.bits.page.PageBase;

import java.sql.Time;

/**
 * Created by Administrator on 2016/6/6.
 */
public class UserFileVO extends PageBase {

    private String ufileId;

    private int ucommonId;

    private String ufileName;

    private String ufileKeyword;

    private String ufileAbstract;

    private String ufileType;

    private String ufileUrl;

    private String ufilePath;

    private String ufileTime;

    private String ufileChecktime;

    private String themeType;

    private String delTime;

    private int id;

    private int userId;

    private int delId;


    private int[] fileId1;

    private int fileId2;

    private String fileName;

    private String fileTime;


    public int getFileId() {
        return fileId;
    }

    public void setFileId(int fileId) {
        this.fileId = fileId;
    }

    private int fileId;

    public String getUfileId() {
        return ufileId;
    }

    public void setUfileId(String ufileId) {
        this.ufileId = ufileId;
    }

    private String userName;

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFileTime() {
        return fileTime;
    }

    public void setFileTime(String fileTime) {
        this.fileTime = fileTime;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public int getFileId2() {
        return fileId2;
    }

    public void setFileId2(int fileId2) {
        this.fileId2 = fileId2;
    }

    public int getDelId() {
        return delId;
    }

    public void setDelId(int delId) {
        this.delId = delId;
    }

    public String getDelTime() {
        return delTime;
    }

    public void setDelTime(String delTime) {
        this.delTime = delTime;
    }

    public String getThemeType() {
        return themeType;
    }

    public void setThemeType(String themeType) {
        this.themeType = themeType;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getUfileChecktime() {
        return ufileChecktime;
    }

    public void setUfileChecktime(String ufileChecktime) {
        this.ufileChecktime = ufileChecktime;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUfileAbstract() {
        return ufileAbstract;
    }

    public void setUfileAbstract(String ufileAbstract) {
        this.ufileAbstract = ufileAbstract;
    }

    public String getUfileName() {
        return ufileName;
    }

    public void setUfileName(String ufileName) {
        this.ufileName = ufileName;
    }

    public String getUfileKeyword() {
        return ufileKeyword;
    }

    public void setUfileKeyword(String ufileKeyword) {
        this.ufileKeyword = ufileKeyword;
    }

    public String getUfileType() {
        return ufileType;
    }

    public void setUfileType(String ufileType) {
        this.ufileType = ufileType;
    }

    public String getUfileUrl() {
        return ufileUrl;
    }

    public void setUfileUrl(String ufileUrl) {
        this.ufileUrl = ufileUrl;
    }

    public String getUfilePath() {
        return ufilePath;
    }

    public void setUfilePath(String ufilePath) {
        this.ufilePath = ufilePath;
    }

    public int getUcommonId() {
        return ucommonId;
    }

    public void setUcommonId(int ucommonId) {
        this.ucommonId = ucommonId;
    }

    public String  getUfileTime() {
        return ufileTime;
    }

    public void setUfileTime(String  ufileTime) {
        this.ufileTime = ufileTime;
    }
}
