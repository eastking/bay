package com.ccut.bits.search.service.Imp;

import com.ccut.bits.index.IndexService;
import com.ccut.bits.util.SpringTransactionalTestCase;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

@ContextConfiguration("/applicationContext-test.xml")
public class IndexServiceImpTest extends SpringTransactionalTestCase {
    @Autowired
    IndexService indexService;
    @Test
    public void testCreateLiteratureIndex() throws Exception {
        indexService.initLiteratureIndex();
    }
    @Test
    public void testCreateExpertIndex() throws Exception {
        indexService.initExpertIndex();
    }
    @Test
    public void testCreatePatentIndex() throws Exception {
        indexService.initPatentIndex();
    }
}